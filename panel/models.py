from django.db import models
from django.contrib.auth.models import User
from django.utils.timezone import now
from django.db.models.signals import post_save,pre_save
from django.dispatch import receiver
from .utils import send_email
import random
from installation.models import SiteModel
from .tokens import create_token
from django.utils.http import urlsafe_base64_encode
from django.utils.encoding import force_bytes
import jsonfield
from channels.layers import get_channel_layer
import json
from asgiref.sync import async_to_sync
from ckeditor_uploader.fields import RichTextUploadingField
from installation.models import SiteModel
from django.utils.crypto import get_random_string
from phonenumber_field.modelfields import PhoneNumberField
from django.db.models import Max
from django.utils.translation import gettext_lazy as _
import environ
from django.contrib.humanize.templatetags.humanize import intcomma,naturaltime
import timeago,datetime
env=environ.Env()
environ.Env.read_env()

     
#generate random
def generate_id():
    return get_random_string(6,'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKMNOPQRSTUVWXYZ0123456789')


#generate random
def generate_serial():
    return get_random_string(12,'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKMNOPQRSTUVWXYZ0123456789')


@receiver(post_save, sender=SiteModel)
def send_installation_email(sender, instance, created, **kwargs):
    if created:
        if instance.is_installed:
            #site is installed
            obj=SiteModel.objects.all()[0]
            subject='Congragulations:Site installed successfully.'
            email=instance.user.email
            message={
                        'user':instance.user,
                        'site_name':instance.site_name,
                        'site_url':instance.site_url,
                        'address':instance.address,
                        'location':instance.location,
                        'description':obj.description,
                        'phone':obj.phone
                     } 
            template='emails/installation.html'
            send_email(subject,email,message,template)





def bgcolor():
    hex_digits=['0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f']
    digit_array=[]
    for i in range(6):
        digits=hex_digits[random.randint(0,15)]
        digit_array.append(digits)
    joined_digits=''.join(digit_array)
    color='#'+joined_digits
    return color





options=[
            ('---Select gender---',
                    (
                        ('Male','Male'),
                        ('Female','Female'),
                        ('Other','Other'),
                    )
            ),
        ]
class ExtendedAuthUser(models.Model):
    user=models.OneToOneField(User,primary_key=True,on_delete=models.CASCADE)
    phone=PhoneNumberField(null=True,blank=True,verbose_name='phone',unique=True,max_length=13)
    initials=models.CharField(max_length=10,blank=True,null=True)
    category=models.CharField(max_length=100,blank=True,null=True)
    balance=models.FloatField(blank=True,null=True,default=0)
    reserved_money=models.FloatField(blank=True,null=True,default=0)
    available_for_withdraw=models.FloatField(blank=True,null=True,default=0)
    serial_no=models.CharField(max_length=100,default=get_random_string,null=True,blank=True)
    bgcolor=models.CharField(max_length=10,blank=True,null=True,default=bgcolor)
    company=models.CharField(max_length=100,null=True,blank=True,default=env('SITE_NAME'))
    zipcode=models.CharField(max_length=100,null=True,blank=True,default='416')
    city=models.CharField(max_length=100,null=True,blank=True,default='Nairobi')
    country=models.CharField(max_length=100,null=True,blank=True,default='Kenya')
    rating=models.FloatField(null=True,blank=True,default=0)
    rating_counter=models.FloatField(null=True,blank=True,default=0)
    native_language=models.TextField(null=True,blank=True)
    writer_cv=models.TextField(null=True,blank=True)
    academic_degree=models.TextField(null=True,blank=True)
    timezone=models.CharField(max_length=200,null=True,blank=True,default='Africa/Nairobi')
    degree=models.CharField(max_length=200,null=True,blank=True)
    university=models.CharField(max_length=200,null=True,blank=True)
    reference_id=models.CharField(max_length=200,null=True,blank=True)
    exam_complete=models.BooleanField(default=False,blank=True,null=True)
    profile_reg=models.BooleanField(default=False,blank=True,null=True)
    profile_pic=models.ImageField(upload_to='profiles/',null=True,blank=True,default="placeholder.jpg")
    role=models.CharField(choices=[('employee','Employee'),('admins','Admin'),],max_length=200,null=True,blank=True)
    citation_style=models.TextField(null=True,blank=True)
    is_deactivated=models.BooleanField(default=False,blank=True,null=True)
    essay_part=models.BooleanField(default=False,blank=True,null=True)
    test_passed=models.BooleanField(default=False,blank=True,null=True)
    test_reason=models.CharField(max_length=200,null=True,blank=True)
    completed_projects=models.IntegerField(null=True,blank=True,default=0)
    inprogress_projects=models.IntegerField(null=True,blank=True,default=0)
    is_experienced=models.BooleanField(default=False,blank=True,null=True)
    is_verified=models.BooleanField(default=False,blank=True,null=True)
    is_featured=models.BooleanField(default=False,blank=True,null=True)
    is_online=models.BooleanField(default=False,blank=True,null=True)
    path=models.CharField(max_length=200,null=True,blank=True)
    certificate=models.ImageField(null=True,blank=True,upload_to='certs/',)
    essay=models.FileField(null=True,blank=True,upload_to='docs/',)
    essay_ok=models.BooleanField(default=True,blank=True,null=True)
    pending_verification=models.BooleanField(default=False,blank=True,null=True)
    bio=models.TextField(null=True,blank=True,default='something about you...')
    nickname=models.CharField(max_length=100,null=True,blank=True,default='Your nickname')
    facebook=models.CharField(max_length=200,null=True,blank=True,default='https://facebook.com/username')
    twitter=models.CharField(max_length=200,null=True,blank=True,default='https://twitter.com/username')
    instagram=models.CharField(max_length=200,null=True,blank=True,default='https://instagram.com/username')
    linkedin=models.CharField(max_length=200,null=True,blank=True,default='https://linkedin.com/username')
    gender=models.CharField(choices=options,max_length=6,null=True,blank=True)
    birthday=models.DateField(null=True,blank=True)
    graduation_year=models.DateField(null=True,blank=True)
    withdraw_period=models.DateTimeField(null=True,blank=True)
    created_on=models.DateTimeField(default=now)
    class Meta:
        db_table='extended_auth_user'
        verbose_name_plural='extended_auth_users'
    def __str__(self)->str:
        return f'{self.user.username} extended auth profile'

    def delete(self, using=None,keep_parents=False):
        if self.essay or self.profile_pic or self.certificate:
            self.essay.storage.delete(self.essay.name)
            self.profile_pic.storage.delete(self.profile_pic.name)
            self.certificate.storage.delete(self.certificate.name)
        super().delete()


        
@receiver(post_save, sender=ExtendedAuthUser)
def send_success_email(sender, instance, created, **kwargs):
    if created and instance.pending_verification:
        #site is installed
        subject='Congragulations:Registration Successfully.'
        email=instance.user.email
        obj=SiteModel.objects.all()[0]
        message={
                    'user':instance.user,
                    'email':instance.user.email,
                    'address':obj.address,
                    'location':obj.location,
                    'phone':obj.phone,
                    'uid':urlsafe_base64_encode(force_bytes(instance.user.id)),
                    'token':create_token.make_token(instance.user),
                    'site_logo':obj.email_template_logo,
                    'site_name':obj.site_name,
                    'site_url':obj.site_url,
                    'description':obj.description,
                }
        template='emails/success.html'
        send_email(subject,email,message,template)



class ContactModel(models.Model):
    name=models.CharField(max_length=100,blank=True,null=True)
    phone=PhoneNumberField(null=True,blank=True,verbose_name='phone',max_length=13)
    subject=models.CharField(max_length=100,null=True,blank=True)
    email=models.CharField(max_length=100,blank=True,null=True)
    initials=models.CharField(max_length=10,blank=True,null=True)
    bgcolor=models.CharField(max_length=10,blank=True,null=True,default=bgcolor)
    is_read=models.BooleanField(default=False,blank=True,null=True)
    message=models.TextField(blank=True,null=True)
    reply=models.TextField(blank=True,null=True)
    created_on=models.DateTimeField(default=now)
    class Meta:
        db_table='contact_tbl'
        verbose_name_plural='contact_tbl'
    def __str__(self)->str:
        return f'{self.name} contact message'




@receiver(post_save, sender=ContactModel)
def send_contact_email(sender, instance, created, **kwargs):
    if created and instance.message:
        channel_layer=get_channel_layer()
        total=ContactModel.objects.filter(is_read=False).count()
        subject='Message received.'
        obj=SiteModel.objects.all()[0]
        email=instance.email
        # async_to_sync(channel_layer.group_send)(
        #     #roon name
        #     "notification_admin",
        #     {
        #         'type': 'send_contact',
        #         'messages_count':json.dumps({'inbox_counter':intcomma(total)})
        #     }
        # )
        message={
                    'user':instance.name,
                    'site_name':obj.site_name,
                    'site_url':obj.site_url,
                    'address':obj.address,
                    'description':obj.description,
                    'phone':obj.phone
                }
        template='emails/contact.html'
        send_email(subject,email,message,template)



class SubscribeModel(models.Model):
    email=models.CharField(max_length=100,blank=True,null=True)
    created_on=models.DateTimeField(default=now)
    class Meta:
        db_table='subscribe_tbl'
        verbose_name_plural='subscribe_tbl'
    def __str__(self)->str:
        return f'{self.name} =subscriber info'




@receiver(post_save, sender=SubscribeModel)
def send_subscribe_email(sender, instance, created, **kwargs):
    if created:
        subject='Subscribed successfully.'
        obj=SiteModel.objects.all()[0]
        email=instance.email
        message={
                    'user':instance.name,
                    'site_name':obj.site_name,
                    'site_url':obj.site_url,
                    'address':obj.address,
                    'phone':obj.phone,
                    'description':obj.description,
                }
        template='emails/subscribe.html'
        send_email(subject,email,message,template)

class SubscribersModel(models.Model):
    email=models.CharField(max_length=50,verbose_name='email address',unique=True,null=True)
    date=models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.name

@receiver(post_save, sender=SubscribersModel)
def send_notification_email(sender, instance, created, **kwargs):
    if created:
        obj=SiteModel.objects.all().first()
        subject='Thank you for subscribing.'
        email=instance.email
        message={
                    'site_name':obj.site_name,
                    'site_url':obj.site_url,
                    'name':email,
                    'address':obj.address,
                    'description':obj.description,
                    'phone':obj.phone
                }
        template='emails/subscription.html'
        send_email(subject,email,message,template)

class CmsModel(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    terms_and_conditions=RichTextUploadingField(null=True,blank=True)
    money_back_guarantee=RichTextUploadingField(null=True,blank=True)
    privacy_policy=RichTextUploadingField(null=True,blank=True)
    writers=RichTextUploadingField(null=True,blank=True)
    srudirre_help=RichTextUploadingField(null=True,blank=True)
    confediality_policy=RichTextUploadingField(null=True,blank=True)
    webmaster=RichTextUploadingField(null=True,blank=True)
    writing_help=RichTextUploadingField(null=True,blank=True)
    research_help=RichTextUploadingField(null=True,blank=True)
    thesis_help=RichTextUploadingField(null=True,blank=True)
    dissertation_help=RichTextUploadingField(null=True,blank=True)
    personal_statement=RichTextUploadingField(null=True,blank=True)
    resources=RichTextUploadingField(null=True,blank=True)
    samples=RichTextUploadingField(null=True,blank=True)
    one_hour=RichTextUploadingField(null=True,blank=True)
    created_on=models.DateTimeField(default=now)
    class Meta:
        db_table='cms_tbl'
        verbose_name_plural='cms_tbl'
    def __str__(self)->str:
        return f'{self.terms_and_conditions} cms'

class FaqModel(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    category=models.CharField(max_length=100,blank=True,null=True)
    subject=models.CharField(max_length=100,null=True,blank=True)
    text=models.TextField(blank=True,null=True)
    created_on=models.DateTimeField(default=now)
    class Meta:
        db_table='faq_tbl'
        verbose_name_plural='faq_tbl'
    def __str__(self)->str:
        return f'{self.name} FAQ'

class HowItWorksModel(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    category=models.CharField(max_length=100,blank=True,null=True)
    subject=models.CharField(max_length=100,null=True,blank=True)
    text=models.TextField(blank=True,null=True)
    image=models.ImageField(upload_to='howitworks/',null=True,blank=True)
    created_on=models.DateTimeField(default=now)
    class Meta:
        db_table='how_tbl'
        verbose_name_plural='how_tbl'
    def __str__(self)->str:
        return f'{self.name} how it works'

class ActivityModel(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    icon=models.TextField(blank=True,null=True)
    title=models.CharField(max_length=100,blank=True,null=True)
    name=models.CharField(max_length=100,blank=True,null=True)
    is_seen=models.BooleanField(default=False,blank=True,null=True)
    created_on=models.DateTimeField(default=now)
    class Meta:
        db_table='activity_tbl'
        verbose_name_plural='activity_tbl'
    def __str__(self)->str:
        return f'{self.user.username} activity info'


@receiver(post_save, sender=ActivityModel)
def create_activity_info(sender, instance, created, **kwargs):
    if created:
        channel_layer=get_channel_layer()
        total=ActivityModel.objects.filter(is_seen=False,user_id=instance.user.pk).count()
        time=timeago.format(instance.created_on, datetime.datetime.now())
        pass
        # async_to_sync(channel_layer.group_send)(
        #     #roon name
        #     "notification_%s" %instance.user.username,
        #     {
        #         'type': 'send_notification',
        #         'activity':json.dumps({'title':instance.title,'icon':instance.icon,'total':intcomma(total),'activity':instance.name,'time':time})
        #     }
        # )

class UpdatesModel(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    name=models.CharField(max_length=100,blank=True,null=True)
    created_on=models.DateTimeField(default=now)
    class Meta:
        db_table='updates_tbl'
        verbose_name_plural='updates_tbl'
    def __str__(self)->str:
        return f'{self.user.username} update info'

@receiver(post_save, sender=UpdatesModel)
def create_updates_info(sender, instance, created, **kwargs):
    if created:
        channel_layer=get_channel_layer()
        time=timeago.format(instance.created_on, datetime.datetime.now())
        pass
        # async_to_sync(channel_layer.group_send)(
        #         #roon name
        #         "updates_landing_page",
        #         {
        #             'type': 'send_updates',
        #             'activity':json.dumps({'title':instance.user.get_full_name(),'activity':instance.name,'time':time})
        #         }
        # )

class PaymentModel(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    name=models.CharField(max_length=100,blank=True,null=True)
    created_on=models.DateTimeField(default=now)
    class Meta:
        db_table='payment_tbl'
        verbose_name_plural='payment_tbl'
    def __str__(self)->str:
        return f'{self.name} payment method'

class ServiceModel(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    name=models.CharField(max_length=100,blank=True,null=True)
    created_on=models.DateTimeField(default=now)
    class Meta:
        db_table='service_tbl'
        verbose_name_plural='service_tbl'
    def __str__(self)->str:
        return f'{self.name} service type'

class AcademicModel(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    name=models.CharField(max_length=100,blank=True,null=True)
    created_on=models.DateTimeField(default=now)
    class Meta:
        db_table='academic_tbl'
        verbose_name_plural='academic_tbl'
    def __str__(self)->str:
        return f'{self.name} academic degree'


class DisplineModel(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    name=models.CharField(max_length=100,blank=True,null=True)
    created_on=models.DateTimeField(default=now)
    class Meta:
        db_table='displine_tbl'
        verbose_name_plural='displine_tbl'
    def __str__(self)->str:
        return f'{self.name} displine'

class CitationModel(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    name=models.CharField(max_length=100,blank=True,null=True)
    created_on=models.DateTimeField(default=now)
    class Meta:
        db_table='citation_tbl'
        verbose_name_plural='citation_tbl'
    def __str__(self)->str:
        return f'{self.name} citation style'


class LanguageModel(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    name=models.CharField(max_length=100,blank=True,null=True)
    created_on=models.DateTimeField(default=now)
    class Meta:
        db_table='language_tbl'
        verbose_name_plural='language_tbl'
    def __str__(self)->str:
        return f'{self.name} language type'

class PaperModel(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    name=models.CharField(max_length=100,blank=True,null=True)
    created_on=models.DateTimeField(default=now)
    class Meta:
        db_table='paper_tbl'
        verbose_name_plural='paper_tbl'
    def __str__(self)->str:
        return f'{self.name} paper type'


class GrammarModel(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    question=models.TextField(blank=True,null=True)
    answer1=models.TextField(blank=True,null=True)
    answer2=models.TextField(blank=True,null=True)
    answer3=models.TextField(blank=True,null=True)
    answer4=models.TextField(blank=True,null=True)
    correct_answer=models.CharField(max_length=100,blank=True,null=True)
    status=models.BooleanField(default=False,blank=True,null=True)
    created_on=models.DateTimeField(default=now)
    class Meta:
        db_table='grammer_tbl'
        verbose_name_plural='grammer_tbl'

    def __str__(self)->str:
        return f'{self.question} grammer'

class AnswerModel(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    question_id=models.IntegerField(blank=True,null=True,default=0)
    passmark=models.IntegerField(blank=True,null=True,default=0)
    question=models.TextField(blank=True,null=True)
    answer=models.TextField(blank=True,null=True)
    correct_answer=models.TextField(blank=True,null=True)
    status=models.BooleanField(default=False,blank=True,null=True)
    created_on=models.DateTimeField(default=now)
    class Meta:
        db_table='answer_tbl'
        verbose_name_plural='answer_tbl'
    def __str__(self)->str:
        return f'{self.answer} answer'

class InfotipsModel(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    service_rate_tag=models.CharField(max_length=100,blank=True,null=True)
    bid_rate_tag=models.CharField(max_length=100,null=True,blank=True)
    created_on=models.DateTimeField(default=now)
    class Meta:
        db_table='infotip_tbl'
        verbose_name_plural='infotip_tbl'
    def __str__(self)->str:
        return f'{self.name} infotips'


class OrderModel(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    inviter_id=models.IntegerField(blank=True,null=True)
    paper_type=models.CharField(max_length=100,blank=True,null=True)
    topic=models.CharField(max_length=100,blank=True,null=True)
    order_id=models.CharField(max_length=100,blank=True,null=True)
    price=models.IntegerField(blank=True,null=True,default=0)
    pages=models.IntegerField(blank=True,null=True,default=0)
    words_per_page=models.IntegerField(blank=True,null=True)
    completer_id=models.IntegerField(blank=True,null=True,default=0)
    bids=models.IntegerField(blank=True,null=True,default=0)
    messages=models.IntegerField(blank=True,null=True,default=0)
    budget=models.IntegerField(blank=True,null=True,default=0)
    amount_paid=models.IntegerField(blank=True,null=True,default=0)
    deadline=models.DateField(blank=True,null=True)
    verdict=models.CharField(max_length=200,blank=True,null=True)
    category=models.CharField(max_length=100,blank=True,null=True)
    rating=models.FloatField(null=True,blank=True)
    status=models.CharField(max_length=100,blank=True,null=True,default='Pending acceptance')
    comment=models.TextField(blank=True,null=True)
    revision=models.BooleanField(default=False,blank=True,null=True)
    is_completed=models.BooleanField(default=False,blank=True,null=True)
    is_answered=models.BooleanField(default=False,blank=True,null=True)
    is_cancelled=models.BooleanField(default=False,blank=True,null=True)
    inprogress=models.BooleanField(default=False,blank=True,null=True)
    is_accepted=models.BooleanField(default=False,blank=True,null=True)
    is_public=models.BooleanField(default=True,blank=True,null=True)
    citation_style=models.CharField(max_length=100,blank=True,null=True)
    paper_instructions=models.TextField(blank=True,null=True)
    is_disabled=models.BooleanField(default=False,blank=True,null=True)
    inviter=models.CharField(max_length=100,blank=True,null=True)
    created_on=models.DateTimeField(default=now)
    class Meta:
        db_table='order_tbl'
        verbose_name_plural='order_tbl'
    def __str__(self)->str:
        return f'{self.user.username} order info'

@receiver(post_save, sender=OrderModel)
def create_updates_info(sender, instance, created, **kwargs):
    if created:
        channel_layer=get_channel_layer()
        time=timeago.format(instance.created_on, datetime.datetime.now())
        pass
        # async_to_sync(channel_layer.group_send)(
        #         #roon name
        #         "notification_%s" %instance.user.username,
        #         {
        #             'type': 'send_notification',
        #             'activity':json.dumps({'title':'New order update','icon':'<i class="ti-shopping-cart"></i>','activity':f'New order #{instance.order_id} posted!','time':time})
        #         }
        # )



class BidModel(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    parent=models.ForeignKey(OrderModel,on_delete=models.CASCADE)
    order_id=models.CharField(max_length=100,blank=True,null=True)
    bid_id=models.CharField(max_length=100,blank=True,null=True)
    price=models.IntegerField(blank=True,null=True,default=0)
    service_charge=models.FloatField(blank=True,null=True,default=0)
    payable=models.FloatField(blank=True,null=True,default=0)
    rating=models.FloatField(blank=True,null=True,default=0)
    client_id=models.IntegerField(blank=True,null=True,default=0)
    is_completed=models.BooleanField(default=False,blank=True,null=True)
    is_accepted=models.BooleanField(default=False,blank=True,null=True)
    has_bid=models.BooleanField(default=False,blank=True,null=True)
    comment=models.TextField(blank=True,null=True)
    status=models.CharField(max_length=100,default='Pending Approval',blank=True,null=True)
    created_on=models.DateTimeField(default=now)
    class Meta:
        db_table='bid_tbl'
        verbose_name_plural='bid_tbl'
    def __str__(self)->str:
        return f'{self.user.username} bid info'
    def save(self,*args,**kwargs):
        if self.payable:
            channel_layer=get_channel_layer()
            amount_to_load=int(self.price)+(int(self.price)-int(self.payable))
            # async_to_sync(channel_layer.group_send)(
            #     #roon name
            #     "notification_%s" %self.parent.user.username,
            #     {
            #         'type': 'notify_client',
            #         'update':json.dumps({'bid_amount':intcomma(self.price),'amount_to_load':amount_to_load,'service_charge':int(self.price)-int(self.payable)})
            #     },
            # )
        super(BidModel,self).save(*args,**kwargs)




class WithdrawRequestModel(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    amount=models.IntegerField(blank=True,null=True)
    email=models.CharField(max_length=100,blank=True,null=True)
    mode=models.CharField(max_length=100,blank=True,null=True)
    name=models.CharField(max_length=100,blank=True,null=True)
    balance=models.IntegerField(blank=True,null=True)
    is_accepted=models.BooleanField(default=False,blank=True,null=True)
    is_declined=models.BooleanField(default=False,blank=True,null=True)
    status=models.CharField(max_length=100,blank=True,null=True,default='Pending Approval')
    created_on=models.DateTimeField(default=now)
    class Meta:
        db_table='withdraw_tbl'
        verbose_name_plural='withdraw_tbl'
    def __str__(self)->str:
        return f'{self.name} withdraw request info'

class ReviewModel(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    name=models.CharField(max_length=100,blank=True,null=True)
    rating=models.CharField(max_length=100,blank=True,null=True)
    category=models.CharField(max_length=100,blank=True,null=True)
    title=models.CharField(max_length=100,blank=True,null=True)
    message=models.TextField(blank=True,null=True)
    image=models.ImageField(upload_to='howitworks/',null=True,blank=True,default="profiles/placeholder.jpg")
    created_on=models.DateTimeField(default=now)
    class Meta:
        db_table='review_tbl'
        verbose_name_plural='review_tbl'
    def __str__(self)->str:
        return f'{self.user.username} review info'


class CategoryModel(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    name=models.CharField(max_length=100,blank=True,null=True)
    created_on=models.DateTimeField(default=now)
    class Meta:
        db_table='category_tbl'
        verbose_name_plural='category_tbl'
    def __str__(self)->str:
        return f'{self.name} blog category'


class SamplesModel(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    topic=models.CharField(max_length=100,blank=True,null=True)
    content=RichTextUploadingField(blank=True,null=True)
    comment_count=models.IntegerField(null=True,blank=True,default=0)
    sample=models.FileField(upload_to='samples/',null=True,blank=True)
    created_on=models.DateTimeField(default=now)
    class Meta:
        db_table='samples_tbl'
        verbose_name_plural='samples_tbl'
    def __str__(self)->str:
        return f'{self.name} samples info'

class CommentModel(models.Model):
    comment=models.ForeignKey(SamplesModel,on_delete=models.CASCADE)
    blog_id=models.IntegerField(blank=True,null=True)
    name=models.CharField(max_length=100,blank=True,null=True)
    email=models.CharField(max_length=100,blank=True,null=True)
    comment=models.TextField(blank=True,null=True)
    created_on=models.DateTimeField(default=now)
    class Meta:
        db_table='comment_tbl'
        verbose_name_plural='comment_tbl'
    def __str__(self)->str:
        return f'{self.name} comment info'

class TransactionModel(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    transaction_id=models.CharField(max_length=100,blank=True,null=True,default=generate_serial)
    order_id=models.CharField(max_length=100,blank=True,null=True)
    debit=models.IntegerField(blank=True,null=True,default=0)
    credit=models.IntegerField(blank=True,null=True,default=0)
    balance=models.IntegerField(blank=True,null=True,default=0)
    reserved_money=models.IntegerField(blank=True,null=True,default=0)
    status=models.CharField(max_length=100,blank=True,null=True,default='Pending')
    particulars=models.CharField(max_length=100,blank=True,null=True)
    mode=models.CharField(max_length=100,blank=True,null=True,default='Wallet')
    created_on=models.DateTimeField(default=now)
    class Meta:
        db_table='transaction_tbl'
        verbose_name_plural='transaction_tbl'
    def __str__(self)->str:
        return f'{self.user.username} transaction info'

class WriterTransactionModel(models.Model):
        user=models.ForeignKey(User,on_delete=models.CASCADE)
        parent=models.ForeignKey(OrderModel,on_delete=models.CASCADE)
        transaction_id=models.CharField(max_length=100,blank=True,null=True,default=generate_serial)
        order_id=models.CharField(max_length=100,blank=True,null=True)
        debit=models.IntegerField(blank=True,null=True,default=0)
        credit=models.IntegerField(blank=True,null=True,default=0)
        balance=models.IntegerField(blank=True,null=True,default=0)
        status=models.CharField(max_length=100,blank=True,null=True,default='Pending')
        particulars=models.CharField(max_length=100,blank=True,null=True)
        mode=models.CharField(max_length=100,blank=True,null=True,default='Wallet')
        created_on=models.DateTimeField(default=now)
        class Meta:
            db_table='writer_transaction_tbl'
            verbose_name_plural='writer_transaction_tbl'
        def __str__(self)->str:
            return f'{self.user.username} writer transaction info'

class ExamModel(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    bid_id=models.IntegerField(blank=True,null=True)
    topic=models.CharField(max_length=100,blank=True,null=True)
    order_id=models.CharField(max_length=100,blank=True,null=True)
    no_of_mcq_quiz=models.IntegerField(blank=True,null=True,default=0)
    no_of_essay_quiz=models.IntegerField(blank=True,null=True,default=0)
    bids=models.IntegerField(blank=True,null=True,default=0)
    budget=models.IntegerField(blank=True,null=True,default=0)
    amount_paid=models.IntegerField(blank=True,null=True,default=0)
    deadline=models.DateField(blank=True,null=True)
    country=models.CharField(max_length=100,blank=True,null=True)
    time=models.TimeField(blank=True,null=True)
    time_required_to_finish=models.CharField(max_length=100,blank=True,null=True)
    whatsapp_phone=PhoneNumberField(null=True,blank=True,verbose_name='phone',max_length=13)
    status=models.CharField(max_length=100,blank=True,null=True,default='Pending Confirmation')
    bid_status=models.CharField(max_length=100,blank=True,null=True,default='Pending Approval')
    is_completed=models.BooleanField(default=False,blank=True,null=True)
    is_cancelled=models.BooleanField(default=False,blank=True,null=True)
    is_rejected=models.BooleanField(default=False,blank=True,null=True)
    is_accepted=models.BooleanField(default=False,blank=True,null=True)
    additional_materials=models.FileField(upload_to='materials/',null=True,blank=True)
    created_on=models.DateTimeField(default=now)
    class Meta:
        db_table='exam_tbl'
        verbose_name_plural='exam_tbl'
    def __str__(self)->str:
        return f'{self.user.username} exam info'

    def delete(self, using=None,keep_parents=False):
        if self.additional_materials:
            self.additional_materials.storage.delete(self.additional_materials.name)
        super().delete()

class OrderMediaModel(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    parent=models.ForeignKey(OrderModel,on_delete=models.CASCADE)
    additional_materials=models.FileField(upload_to='materials/',null=True,blank=True)
    created_on=models.DateTimeField(default=now)
    class Meta:
        db_table='media_tbl'
        verbose_name_plural='media_tbl'
    def __str__(self)->str:
        return f'{self.user.username} media info'
    def delete(self, using=None,keep_parents=False):
        if self.additional_materials:
            self.additional_materials.storage.delete(self.additional_materials.name)
        super().delete()

class ExamMediaModel(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    parent=models.ForeignKey(ExamModel,on_delete=models.CASCADE)
    additional_materials=models.FileField(upload_to='materials/',null=True,blank=True)
    created_on=models.DateTimeField(default=now)
    class Meta:
        db_table='exam_media_tbl'
        verbose_name_plural='exam_media_tbl'
    def __str__(self)->str:
        return f'{self.user.username} media info'

    def delete(self, using=None,keep_parents=False):
        if self.additional_materials:
            self.additional_materials.storage.delete(self.additional_materials.name)
        super().delete()

class SubmittedAnswerModel(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    parent=models.ForeignKey(OrderModel,on_delete=models.CASCADE)
    additional_materials=models.FileField(upload_to='answers/',null=True,blank=True)
    created_on=models.DateTimeField(default=now)
    class Meta:
        db_table='submitted_answer_tbl'
        verbose_name_plural='submitted_answer_tbl'
    def __str__(self)->str:
        return f'{self.user.username} answer info'

    def delete(self, using=None,keep_parents=False):
        if self.additional_materials:
            self.additional_materials.storage.delete(self.additional_materials.name)
        super().delete()


class ChatModel(models.Model):
    sender=models.ForeignKey(User,on_delete=models.CASCADE)
    parent=models.ForeignKey(OrderModel,on_delete=models.CASCADE)
    receiver=models.IntegerField(blank=True,null=True)
    bid_id=models.IntegerField(blank=True,null=True)
    message=models.TextField(blank=True,null=True)
    is_read=models.BooleanField(default=False,blank=True,null=True)
    created_on=models.DateTimeField(default=now)
    class Meta:
        db_table='chat_tbl'
        verbose_name_plural='chat_tbl'
    def __str__(self)->str:
        return f'{self.sender.get_full_name()} chat info'

@receiver(post_save, sender=ChatModel)
def create_updates_info(sender, instance, created, **kwargs):
    if created:
        channel_layer=get_channel_layer()
        time=timeago.format(instance.created_on, datetime.datetime.now())
        user=User.objects.get(id__exact=instance.receiver)
        if user.extendedauthuser.category == 'writer':
            id=instance.parent.id
        else:
           id=instance.bid_id
        pass
        # async_to_sync(channel_layer.group_send)(
        #         #roon name
        #         "notification_%s" %user.username,
        #         {
        #             'type': 'send_notification',
        #             'activity':json.dumps({'category':user.extendedauthuser.category,'name':instance.sender.get_full_name(),'profile':instance.sender.extendedauthuser.profile_pic.url,'message':True,'id':id,'title':'New message','icon':'<i class="ti-comments"></i>','activity':instance.message,'time':time})
        #         }
        # )

class BroadcastModel(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    message=models.TextField(blank=True,null=True)
    to=models.CharField(max_length=100,blank=True,null=True)
    is_read=models.BooleanField(default=False,blank=True,null=True)
    group=models.BooleanField(default=True,blank=True,null=True)
    created_on=models.DateTimeField(default=now)
    class Meta:
        db_table='broadcast_tbl'
        verbose_name_plural='broadcast_tbl'
    def __str__(self)->str:
        return f'{self.user.get_full_name()} broadcast info'

    def save(self,*args,**kwargs):
        if self.to:
            channel_layer=get_channel_layer()
            messages_count=BroadcastModel.objects.filter(to=self.to,is_read=False).count()
            # async_to_sync(channel_layer.group_send)(
            #     #roon name
            #     "notification_%s" %self.to,
            #     {
            #         'type': 'notify_users',
            #         'broadcast_count':json.dumps({'broadcast_count':intcomma(messages_count)})
            #     },
            # )
        super(BroadcastModel,self).save(*args,**kwargs)


class IndividualModel(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    parent=models.ForeignKey(BroadcastModel,on_delete=models.CASCADE)
    message=models.TextField(blank=True,null=True)
    is_read=models.BooleanField(default=False,blank=True,null=True)
    created_on=models.DateTimeField(default=now)
    class Meta:
        db_table='individual_tbl'
        verbose_name_plural='individual_tbl'
    def __str__(self)->str:
        return f'{self.user.get_full_name()} individual message info'


@receiver(post_save, sender=User)
def create_cms_email(sender, instance, created, **kwargs):
    if created and instance.is_superuser:
        CmsModel.objects.create(user_id=instance.pk)
        PaymentModel.objects.bulk_create([
            PaymentModel(user_id=instance.pk,name='Mpesa'),
            PaymentModel(user_id=instance.pk,name='Payoneer'),
            PaymentModel(user_id=instance.pk,name='Paypal'),
            ])
        ServiceModel.objects.bulk_create([
            ServiceModel(user_id=instance.pk,name='Service 1'),
            ServiceModel(user_id=instance.pk,name='Service 2'),
            ServiceModel(user_id=instance.pk,name='Service 3'),
            ])

        LanguageModel.objects.bulk_create([
            LanguageModel(user_id=instance.pk,name='English'),
            LanguageModel(user_id=instance.pk,name='French'),
            LanguageModel(user_id=instance.pk,name='Germany'),
            ])
        CitationModel.objects.bulk_create([
            CitationModel(user_id=instance.pk,name='APA'),
            CitationModel(user_id=instance.pk,name='MLA'),
            CitationModel(user_id=instance.pk,name='Russian'),
            ])
        AcademicModel.objects.bulk_create([
            AcademicModel(user_id=instance.pk,name='Primary level'),
            AcademicModel(user_id=instance.pk,name='High school level'),
            AcademicModel(user_id=instance.pk,name='University level'),
            ])
        DisplineModel.objects.bulk_create([
            DisplineModel(user_id=instance.pk,name='Displine 1'),
            DisplineModel(user_id=instance.pk,name='Displine 2'),
            DisplineModel(user_id=instance.pk,name='Displine 3'),
            ])
        PaperModel.objects.bulk_create([
            PaperModel(user_id=instance.pk,name='Paper 1'),
            PaperModel(user_id=instance.pk,name='Paper 2'),
            PaperModel(user_id=instance.pk,name='Paper 3'),
            ])
        InfotipsModel.objects.bulk_create([
            InfotipsModel(user_id=instance.pk,service_rate_tag='20%',bid_rate_tag="20%"),
            ])
        GrammarModel.objects.bulk_create([
            GrammarModel(user_id=instance.pk,question='Who was the first president of kenya?',answer1='Kenyatta',answer2="Moi",answer3="Kibaki",answer4="Ruto",correct_answer="Kenyatta",status=True),
            GrammarModel(user_id=instance.pk,question='Whats your fav meal?',answer1='Mango',answer2="Banana",answer3="Onion",answer4="Orange",correct_answer="Banana",status=True),
            GrammarModel(user_id=instance.pk,question='Whats your fav name?',answer1='Kevin',answer2="Legit",answer3="Kibebe",answer4="User",correct_answer="Legit",status=True),
            ])