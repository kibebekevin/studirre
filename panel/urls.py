from django.urls import path
from django import views
from . import views
from .views import *
from .forms import UserResetPassword
from django.contrib.auth import views as auth_views


urlpatterns=[
    path('',Home.as_view(),name='home'),
    path('dashboard',views.dashboard,name='dashboard'),
    path('accounts/logout',views.user_logout,name='logout'),
    path('accounts/reset/password',auth_views.PasswordResetView.as_view(form_class=UserResetPassword,extra_context={'title':'Password Reset'},template_name='panel/password_reset.html'),{'title':'Password Reset'},name='password_reset'),
    path('accounts/reset/password/done',auth_views.PasswordResetDoneView.as_view(template_name='panel/password_reset_done.html'),name='password_reset_done'),
    path('accounts/reset/<uidb64>/<token>',auth_views.PasswordResetConfirmView.as_view(template_name='panel/password_reset_confirm.html'),name='password_reset_confirm'),
    path('reset/password/success',auth_views.PasswordResetCompleteView.as_view(template_name='panel/password_reset_complete.html'),name='password_reset_complete'),
    
    path('proceed/<int:id>',Proceed.as_view(),name="proceed"),

    #contact us
    path('contact/us',Contact.as_view(),name='contact'),
    path('users/contact/us',usersContact.as_view(),name='users contact'),
    #path('sign/up',Register.as_view(),name='register'), 
    path('about/us',views.about,name='about'),
    path('FAQs',views.faqs,name='faqs'),
    path('all/writers',views.allWriters,name='all writers'),
    path('how/it/works',views.work,name='how it works'),
    path('terms/and/conditions',views.terms,name='terms and conditions'),


    path('subscription',views.subscribe,name='subscribe'),
    path('site/writers',views.writers,name='writers'),
    path('money/back/guarantee',views.moneyback,name='moneyback'),
    path('privacy/policy',views.privacyPolicy,name='privacy policy'),
    path('site/help',views.siteHelp,name='site help'),
    path('confediality/policy',views.confediality_policy,name='confediality policy'),
    path('webmaster/affliate/program',views.webmasterAffliate,name='webmaster program'),
    path('paper/writing/help',views.paperWriting,name='paper writing'),
    path('research/help',views.researchHelp,name='research help'),
    path('thesis/help',views.thesisHelp,name='thesis help'),
    path('dissertation/help',views.dissertationHelp,name='dissertation help'),
    path('personal/statement/help',views.personalHelp,name='personal help'),
    path('resources',views.siteResources,name='resources name'),
    path('one/hour/topup/writer',views.oneHour,name='one name'),
    path('customer/sign/up',views.customer_signup,name='customer signup'),

    path('accounts/login',Login.as_view(),name='login'),
    path('accounts/activate/<str:category>',views.account_activate,name='account activate'),
    path('accounts/activate/<uidb64>/<token>',views.AccountActivator,name='activate'),
    path('profile/setup/complete',views.loginnow,name='loginnow'),
    path('<str:username>',ProfileView.as_view(),name='profile'),
    path('site/settings',EditSite.as_view(),name='site settings'),
    path('site/working/days',views.siteWorking,name='site working days'),
    path('site/contact',views.siteContact,name='site contact'),
    path('site/social/links/settings',views.siteSocial,name='site social links'),
    path('site/localization/settings',views.localization,name='localization'),
    path('site/grammer/settings',views.grammer,name='grammer'),
    path('site/wallet/settings',views.wallet,name='wallet'),
    path('site/commision/settings',views.commision,name='commision'),

    #Payment method
    path('manage/payment/method',views.payment,name='payment'),
    path('add/payment/method',AddPayment.as_view(),name='add payment'),
    path('edit/payment/method/<int:id>',EditPayment.as_view(),name='edit payment'),
    path('delete/payment/method/<int:id>',views.deletePayment,name='delete payment'),

    #service type
    path('manage/service/type',views.service,name='service'),
    path('add/service/type',AddService.as_view(),name='add service'),
    path('edit/service/type/<int:id>',EditService.as_view(),name='edit service'),
    path('delete/service/type/<int:id>',views.deleteService,name='delete service'),


    #displine
    path('manage/displines',views.displine,name='displine'),
    path('add/displine',AddDispline.as_view(),name='add displine'),
    path('edit/displine/<int:id>',EditDispline.as_view(),name='edit displine'),
    path('delete/displine/<int:id>',views.deleteDispline,name='delete displine'),


    #citation styles
    path('manage/citation/styles',views.citation,name='citation'),
    path('add/citation/style',AddCitation.as_view(),name='add citation'),
    path('edit/citation/style/<int:id>',EditCitation.as_view(),name='edit citation'),
    path('delete/citation/style/<int:id>',views.deleteCitation,name='delete citation'),

    # academic
    path('manage/academic/degree',views.academicDegree,name='academic'),
    path('add/academic/degree',AddAcademic.as_view(),name='add academic'),
    path('edit/academic/degree/<int:id>',EditAcademic.as_view(),name='edit academic'),
    path('delete/academic/degree/<int:id>',views.deleteAcademic,name='delete academic'),

    #language
    path('manage/languages',views.language,name='language'),
    path('add/language',AddLanguage.as_view(),name='add language'),
    path('edit/language/<int:id>',EditLanguage.as_view(),name='edit language'),
    path('delete/language/<int:id>',views.deleteLanguage,name='delete language'),

    #paper
    path('manage/paper/types',views.paperType,name='paper'),
    path('add/paper/types',AddPaper.as_view(),name='add paper'),
    path('edit/paper/types/<int:id>',EditPaper.as_view(),name='edit paper'),
    path('delete/paper/types/<int:id>',views.deletePaper,name='delete paper'),

    #questions
    path('manage/questions',views.questions,name='questions'),
    path('add/questions',AddQuestion.as_view(),name='add questions'),
    path('edit/questions/<int:id>',EditQuestion.as_view(),name='edit questions'),
    path('delete/questions/<int:id>',views.deleteQuestion,name='delete questions'),

    #answers
    path('manage/answers/<id>',views.answers,name='answers'),

    #reject essay
    path('reject/essay/<id>',views.rejectEssay,name='reject essay'),

    #verify user
    path('verify/user/<id>',views.verifyUser,name='verify user'),
    #users
    path('manage/users',views.users,name='users'),
    path('manage/user/details/<id>',UserDetails.as_view(),name='user details'),
    path('add/writer',addWriter.as_view(),name='add writer'),
    path('add/customer',addCustomer.as_view(),name='add customer'),
    path('edit/writer/<int:id>',editWriter.as_view(),name='edit writer'),
    path('edit/customer/<int:id>',editCustomer.as_view(),name='edit customer'),
    path('delete/user/<int:id>',views.deleteUser,name='delete user'), 
    
    #manage faqs
    path('faqs/settings',views.manageFaqs,name='manage faqs'), 
    path('add/faq',AddFaq.as_view(),name='add faq'),
    path('edit/faq/<int:id>',EditFaq.as_view(),name='edit faq'),
    path('delete/faq/<int:id>',views.deleteFaq,name='delete faq'),

    #howitworks
    path('manage/howitworks/page',views.howitworks,name='works'),
    path('add/item',AddWork.as_view(),name='add work'),
    path('edit/item/<int:id>',EditWork.as_view(),name='edit work'),
    path('delete/item/<int:id>',views.deleteWork,name='delete work'),

    #infotips
    path('manage/infotips',views.infotips,name='infotips'),
    path('add/infotip',AddInfotip.as_view(),name='add infotip'),
    path('edit/infotip/<int:id>',EditInfotip.as_view(),name='edit infotip'),
    path('delete/infotip/<int:id>',views.deleteInfotip,name='delete infotip'),

    #cmc
    path('manage/cms',views.Cms.as_view(),name='manage cms'),

    path('all/orders',views.allOrders,name='orders'),
    path('all/reviews',views.allReviews,name='all reviews'),

    path('lock/screen/<str:username>',views.screenLock,name='lock screen'),
    path('unlock/screen/<str:username>',views.screenUnlock,name='unlock screen'),
    path('change/profile/pic',views.profilePic,name='profile pic'),
    path('deactivate/account',views.action,name='action'),
    path('edit/social/links',views.edit_social_link,name='edit social link'),
    path('password/change',views.passwordChange,name='password change'),

    path('app/writers',views.app_writers,name='studirre writers'),
    path('app/money',views.money,name='money'),
    path('app/privacy',views.privacy,name='privacy'),
    path('app/help',views.help,name='help'),
    path('app/confediality',views.confediality,name='confediality'),
    path('app/webmaster',views.webmaster,name='webmaster'),
    path('app/manage/paper/types',views.cmsPaper,name='cms paper'),
    path('app/research',views.research,name='research'),
    path('app/thesis',views.thesis,name='thesis'),
    path('app/dissertation',views.dissertation,name='dissertation'),
    path('app/personal',views.personal,name='personal'),
    path('app/resources',views.resources,name='resources'),
    path('app/add/sample/paper',SamplePaper.as_view(),name='add sample paper'),
    path('app/samples',views.manageSamples,name='app samples'),
    path('manage/cms/app/samples',views.CMSsamples,name='cms samples'),
    path('delete/app/samples/<int:id>',views.deleteSamples,name='delete sample'),
    path('app/one',views.one,name='one'),

    path('manage/writer/reviews',manageReviews.as_view(),name='manage reviews'),
    path('manage/customer/reviews',manageCustomerReviews.as_view(),name='manage customer reviews'),

    #customer review
    path('add/customer/review',views.customerReview,name='customer review'),
    path('edit/writer/review/<int:id>',EditWriterReview.as_view(),name='edit writer review'),
    path('edit/customer/review/<int:id>',EditCustomerReview.as_view(),name='edit customer review'),
    path('delete/writer/review/<int:id>',views.deleteWriterReview,name='delete writer review'),
    path('delete/customer/review/<int:id>',views.deleteCustomerReview,name='delete customer review'),
    #messages
    path('inbox/messages',views.messages,name='messages'),
    path('delete/message/<int:id>',views.deleteMessage,name='delete message'),

    #Grammer
    path('grammer/test/<id>',GrammerTest.as_view(),name='grammer test'),
    path('time/elapsed/<id>',views.elapsed,name='elapsed'),
    #write essay
    path('write/essay/<id>',Essay.as_view(),name='essay'),

    #gotodashboard
    path('go/to/dashboard/<id>',views.gotodashboard,name='gotodashboard'),


    #browse orders
    path('browse/orders',views.browseOrders,name='browse orders'),
    path('my/orders',views.myOrders,name='my orders'),
    path('my/orders/writer',views.myOrdersWriter,name='my orders writer'),

    path('writer/earnings',Earnings.as_view(),name='writer earnings'),
    path('inbound/messages',views.InboxMessages,name='inbox messages'),
    path('inbound/messages/<int:id>',views.messageSingle,name='message single'),
    path('my/messages',views.myMessages,name='my messages'),

    #users
    path('manage/admins',views.admins,name='admins'),
    path('add/admin',addAdmin.as_view(),name='add admin'),
    path('edit/admin/<int:id>',editAdmin.as_view(),name='edit admin'),
    path('delete/admin/<int:id>',views.deleteAdmin,name='delete admin'), 
    path('manage/admin/details/<id>',AdminDetails.as_view(),name='admin details'),

    #account activities
    path('manage/account/activities',account_activities,name='account activities'),
    #delete activity
    path('delete/activity/<int:id>',views.deleteActivity,name='delete activity'),
    path('delete/update/<int:id>',views.deleteUpdate,name='delete update'),

    #delete all
    path('delete/all/activities',views.deleteAllActivity,name='delete all'),
    path('delete/all/updates',views.deleteAllUpdates,name='delete updates all'),

    path('account/topup',Topup.as_view(),name='topup'),

    #new order
    path('place/new/order',newOrder.as_view(),name='new order'),
    path('order/preview/<int:id>',orderPreview.as_view(),name='order preview'),
    path('confirm/order/<int:id>',views.confirmOrder,name='confirm order'),
    path('bids/<int:id>',views.bids,name='bids'),
    path('all/orders',views.allOrders,name='orders'),
    path('publish/order/for/writers/<int:id>',publishOrder.as_view(),name='admin edit order'),

    #edit order
    path('edit/order/<int:id>',editOrder.as_view(),name='edit order'),
    #cancel order
    path('cancel/order/<int:id>',views.cancelOrder,name='cancel order'),
    path('uncancel/order/<int:id>',views.uncancelOrder,name='uncancel order'),
    path('delete/order/<int:id>',views.deleteOrder,name='delete order'),
    path('delete/bid/<int:id>',views.deleteBid,name='delete bid'),
    #make public
    path('make/order/public/<int:id>',views.publicOrder,name='make public'),
    path('place/bid/<int:id>',placeBid.as_view(),name='place bid'),
    path('preview/order/<int:id>',views.orderPrev,name='see order'),
    path('preview/exam/order/<int:id>',views.examOrderPrev,name='see exam order'),

    #bid details
    path('bid/details/<int:id>',BidDetails.as_view(),name='bid details'),
    path('user/<int:id>',views.writer,name='writer'),

    #accept bid
    path('accept/bid/<int:id>',views.acceptBid,name='accept bid'),

    path('suspended/account',views.suspendedAccount,name='suspended account'),

    #readall messages
    path('readall/<int:id>',views.readall,name='readall'),

    #passform
    path('change/user/account/password<int:id>',views.passform,name='passform'),

    #online writers
    path('online/users',views.onlineUsers,name='online users'),

    #exam
    path('exam/order',views.examOrder,name='exam'),
    path('delete/exam/<int:id>',views.deleteExam,name='delete exam'),
    path('edit/exam/order/<int:id>',editExamOrder.as_view(),name='edit exam order'),
    path('edit/question/order/<int:id>',views.editorexamOrder,name='edit question order'),
    path('bid/preview/<int:id>',bidPreview.as_view(),name='bid preview'),

    #additional materials
    path('upload/additional/materials/<int:id>',views.additionalMaterials,name='additional materials'),
    #answer materials
    path('upload/answer/materials/<int:id>',views.answerMaterials,name='answer materials'),
    #delete media
    path('delete/media/<int:id>',views.deleteMedia,name='delete media'),
    path('delete/media/exam/<int:id>',views.deleteMediaExam,name='delete media exam'),
    path('order/summary',views.orderSummary,name='order summary'),
    path('writers/order/summary',views.writerOrderSummary,name='writer order summary'),
    path('receive/payments',views.moneyReceived,name='receive payments'),

    #verdict
    path('verdict/<int:id>',views.verdict,name='verdict'),
    path('ratings/and/reviews',views.manageRatings,name='ratings and reviews'),
    #dit customer rating
    path('edit/customer/rating/<int:id>',editCustomerRating.as_view(),name='edit customer rating'),
    path('edit/writer/rating/<int:id>',editWriterRating.as_view(),name='edit writer rating'),
    path('accounts/approve/payments/<int:id>',views.approvePayments,name='approve payment'),
    path('accounts/decline/payments/<int:id>',views.declinePayments,name='decline payments'),

    path('chat/<str:order_id>/<int:user_id>',views.chat,name='chat'),
    path('all/writer/reviews',views.allWriterReviews,name='all writer reviews'),
    path('writer/reviews/<int:id>',views.writerReviews,name='writer reviews'),
    path('client/reviews/<int:id>',views.clientReviews,name='client reviews'),
    path('order/conversation/<int:id>',orderConversation.as_view(),name='order conversation'),
    path('disable/conversation/<int:id>',views.disableChat,name='disable chat'),

    #broadcasts
    path('broadcasts/messages',views.broadcasts,name='broadcasts'),
    path('add/broadcasts/message',AddBroadcast.as_view(),name='add broadcast'),
    path('add/single/broadcasts/message',AddSingleBroadcast.as_view(),name='add single'),
    path('edit/broadcasts/message/<int:id>',EditBroadcast.as_view(),name='edit broadcast'),
    path('edit/single/broadcast/message/<int:id>',EditSingleBroadcast.as_view(),name='edit single'),
    path('delete/broadcasts/message/<int:id>',views.deleteBroadcast,name='delete broadcast'),
    path('individial/broadcast/message',views.individialMessage,name='single message'),
    #mark is read
    path('mark/message/is/read/<int:id>',views.markMessageIsRead,name='mark is read'),
    path('mark/inbox/message/is/read/<int:id>',views.markInboxMessageIsRead,name='mark inbox is read'),

    #mark chat read
    path('mark/chat/message/is/read',views.markChatIsRead,name='mark chat read'),

    #delete read message
    path('delete/read/message/<int:id>',views.deleteReadMessage,name='delete read message'),
    path('samples/papers',views.samples,name='samples'),
    path('sample/<int:id>',sampleSingle.as_view(),name='sample single'),
    #edit sample
    path('edit/sample/<int:id>',editSample.as_view(),name='edit sample'),
    path('edit/comment/<int:id>',editComment.as_view(),name='edit comment'),
    path('delete/comment/<int:id>',views.deleteComment,name='delete comment'),
    path('continue/profile/setup/<str:username>',profileSetup.as_view(),name='profile setup'),
]

