from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from .models import *
from .forms import *
from django import forms
import datetime
from django.contrib.auth.forms import PasswordResetForm, UserCreationForm,UserChangeForm,PasswordChangeForm
from django.contrib.auth.forms import User
from phonenumber_field.formfields import PhoneNumberField
from phonenumber_field.widgets import PhoneNumberPrefixWidget
from django.contrib.auth.hashers import check_password
from django.core.validators import FileExtensionValidator,URLValidator
from installation.forms import SiteModel
from django.contrib.auth import authenticate
import re
from urllib.parse import urlparse
from ckeditor.fields import RichTextFormField
from ckeditor_uploader.fields import RichTextUploadingFormField


options=[
            ('---Select gender---',
                    (
                        ('Male','Male'),
                        ('Female','Female'),
                        ('Other','Other'),
                    )
            ),
        ]
class UserResetPassword(PasswordResetForm):
    email=forms.EmailField(widget=forms.EmailInput(attrs={'class':'form-control','placeholder':'Enter email address'}),error_messages={'required':'Email address is required'})

    def clean_email(self):
        email=self.cleaned_data['email']
        if  not User.objects.filter(email=email).exists():
            raise forms.ValidationError('Email address does not exist')
        try:
            validate_email(email)
        except ValidationError:
            raise forms.ValidationError('Invalid email address')
        return email

#contact form
class ContactForm(forms.ModelForm):
    name=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'name','class':'form-control','placeholder':'Enter name'}),error_messages={'required':'Name is required'})
    email=forms.EmailField(widget=forms.EmailInput(attrs={'aria-label':'email','class':'form-control','placeholder':'Enter email address'}),error_messages={'required':'Email address is required'})
    subject=forms.CharField(min_length=5,widget=forms.TextInput(attrs={'aria-label':'subject','class':'form-control','placeholder':'Enter subject'}),error_messages={'required':'Subject is required','min_length':'enter atleast 5 characters long subject'})
    message=forms.CharField(min_length=10,widget=forms.Textarea(attrs={'aria-label':'message','class':'form-control','placeholder':'Enter message','rows':6}),error_messages={'required':'Message is required','min_lenghth':'enter atleast 10 characters long message'})
    class Meta:
        model=ContactModel
        fields=['name','email','subject','message']
        
    def clean_name(self):
        name=self.cleaned_data['name']
        try:
             re.match('^[a-zA-Z]+$',name)
        except ValidationError as e:
            raise forms.ValidationError('only characters are allowed')
        return name
    
    def clean_email(self):
        email=self.cleaned_data['email']
        try:
            validate_email(email)
        except ValidationError as e:
            raise forms.ValidationError('invalid email address')
        return email


    def clean_message(self):
        subject=self.cleaned_data['subject']
        message=self.cleaned_data['message']
        if ContactModel.objects.filter(subject=subject,message=message).exists():
            raise forms.ValidationError('message found please say something else')
        return message

#subscriber form
class SubscriberForm(forms.ModelForm):
    email=forms.EmailField(widget=forms.EmailInput(attrs={'aria-label':'email','class':'form-control','placeholder':'Enter email address'}),error_messages={'required':'Email address is required'})
    class Meta:
        model=SubscribersModel
        fields=['email']
        
    def clean_email(self):
        email=self.cleaned_data['email']
        if SubscribersModel.objects.filter(email=email).exists():
            raise forms.ValidationError('you have already been subscribed')
        try:
            validate_email(email)
        except ValidationError as e:
            raise forms.ValidationError('invalid email address')
        return email

class UserLoginForm(forms.Form):
    username=forms.CharField(widget=forms.TextInput(attrs={'aria-required':'true','class':'form-control','placeholder':'Username ','aria-label':'username'}),error_messages={'required':'Username  is required'})
    password=forms.CharField(widget=forms.PasswordInput(attrs={'aria-required':'true','class':'form-control login-password','placeholder':'Password','aria-label':'password','autocomplete':True}),error_messages={'required':'Password is required'})

    class Meta:
        model=User
        fields=['username','password',]

    def clean_username(self):
        username=self.cleaned_data['username']
        if User.objects.filter(username=username).exists():
            return username
        else:
            raise forms.ValidationError('invalid username')

#user register form
class UserReg(UserCreationForm):
    first_name=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'first_name','class':'form-control fg-theme','placeholder':'Enter first name'}),error_messages={'required':'First name is required'})
    last_name=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'last_name','class':'form-control','placeholder':'Enter last name'}),error_messages={'required':'Last name is required'})
    email=forms.EmailField(widget=forms.EmailInput(attrs={'aria-label':'email','class':'form-control','placeholder':'Enter email address'}),error_messages={'required':'Email address is required'})
    username=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'username','class':'form-control','placeholder':'Enter username'}),error_messages={'required':'Username is required'})
    password1=forms.CharField(min_length=6,widget=forms.PasswordInput(attrs={'aria-label':'password1','class':'form-control','placeholder':'Enter password'}),error_messages={'required':'Password is required','min_length':'enter atleast 6 characters long'})
    is_active=forms.BooleanField(widget=forms.CheckboxInput(attrs={'aria-label':'is_active','id':'checkbox1'}),required=False)
    password2=forms.CharField(widget=forms.PasswordInput(attrs={'aria-label':'password2','class':'form-control','placeholder':'Enter confirm password'}),error_messages={'required':'Confirm password is required'})
    class Meta:
        model=User
        fields=['first_name','last_name','username','email','password1','password2','is_active',]

    def clean_first_name(self):
        first_name=self.cleaned_data['first_name']
        if not str(first_name).isalpha():
            raise forms.ValidationError('only characters are required')
        return first_name
    def clean_last_name(self):
        last_name=self.cleaned_data['last_name']
        if not str(last_name).isalpha():
            raise forms.ValidationError('only characters are required')
        return last_name

    def clean_email(self):
        email=self.cleaned_data['email']
        if self.instance.email:
            if email != self.instance.email:
                if User.objects.filter(email=email).exists():
                    raise forms.ValidationError('A user with this email already exists.')
                try:
                    validate_email(email)
                except ValidationError as e:
                    raise forms.ValidationError('Invalid email address.')
                return email
            else:
               return email
        else:
            if User.objects.filter(email=email).exists():
                raise forms.ValidationError('A user with that email already exist')
            try:
                validate_email(email)
            except ValidationError as e:
                raise forms.ValidationError('Invalid email address')
            return email

class UserChanger(UserChangeForm):
    first_name=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'first_name','class':'form-control fg-theme','placeholder':'Enter first name'}),error_messages={'required':'First name is required'})
    last_name=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'last_name','class':'form-control','placeholder':'Enter last name'}),error_messages={'required':'Last name is required'})
    email=forms.EmailField(widget=forms.EmailInput(attrs={'aria-label':'email','class':'form-control','placeholder':'Enter email address'}),error_messages={'required':'Email address is required'})
    username=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'username','class':'form-control','placeholder':'Enter username'}),error_messages={'required':'Username is required'})
    password1=forms.CharField(min_length=6,widget=forms.PasswordInput(attrs={'aria-label':'password1','class':'form-control','placeholder':'Enter password'}),error_messages={'required':'Password is required','min_length':'enter atleast 6 characters long'})
    is_active=forms.BooleanField(widget=forms.CheckboxInput(attrs={'aria-label':'is_active','id':'checkbox1'}),required=False)
    password2=forms.CharField(widget=forms.PasswordInput(attrs={'aria-label':'password2','class':'form-control','placeholder':'Enter confirm password'}),error_messages={'required':'Confirm password is required'})
    class Meta:
        model=User
        fields=['first_name','last_name','username','email','password1','password2','is_active',]

    def clean_first_name(self):
        first_name=self.cleaned_data['first_name']
        if not str(first_name).isalpha():
            raise forms.ValidationError('only characters are required')
        return first_name
    def clean_last_name(self):
        last_name=self.cleaned_data['last_name']
        if not str(last_name).isalpha():
            raise forms.ValidationError('only characters are required')
        return last_name

    def clean_email(self):
        email=self.cleaned_data['email']
        if self.instance.email:
            if email != self.instance.email:
                if User.objects.filter(email=email).exists():
                    raise forms.ValidationError('A user with this email already exists.')
                try:
                    validate_email(email)
                except ValidationError as e:
                    raise forms.ValidationError('Invalid email address.')
                return email
            else:
               return email
        else:
            if User.objects.filter(email=email).exists():
                raise forms.ValidationError('A user with that email already exist')
            try:
                validate_email(email)
            except ValidationError as e:
                raise forms.ValidationError('Invalid email address')
            return email

class ExtendedRegisterForm(forms.ModelForm):
    phone=PhoneNumberField(widget=PhoneNumberPrefixWidget(attrs={'class':'form-control input-rounded','type':'tel','aria-label':'phone','placeholder':'Phone'}),required=False)
    city=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control input-rounded','aria-label':'city'}))
    country=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control input-rounded','aria-label':'country'}))
    zipcode=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control input-rounded','aria-label':'zipcode'}))
    university=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control input-rounded','aria-label':'university'}))
    degree=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control input-rounded','aria-label':'degree'}))
    balance=forms.CharField(widget=forms.NumberInput(attrs={'class':'form-control','aria-label':'balance'}),initial=0,required=False)
    rating=forms.ChoiceField(choices=[('0','select rating'),('5','5'),('4','4'),('3','3'),('2','2'),('1','1')],widget=forms.Select(attrs={'aria-label':'rating','class':'form-control'}),required=False)
    rating_counter=forms.CharField(widget=forms.NumberInput(attrs={'class':'form-control','aria-label':'rating_counter'}),initial=0,required=False)
    completed_projects=forms.CharField(widget=forms.NumberInput(attrs={'class':'form-control','aria-label':'completed_projects'}),initial=0,required=False)
    timezone=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control input-rounded','aria-label':'timezone'}),required=False)
    is_verified=forms.BooleanField(widget=forms.CheckboxInput(attrs={'aria-label':'is_verified','id':'checkbox1'}),required=False)
    is_deactivated=forms.BooleanField(widget=forms.CheckboxInput(attrs={'aria-label':'is_deactivated','id':'checkbox2'}),required=False)
    graduation_year=forms.DateField(widget=forms.DateInput(attrs={'class':'form-control','aria-label':'graduation_year','type':'date'}))
    withdraw_period=forms.DateTimeField(widget=forms.DateTimeInput(attrs={'class':'form-control','aria-label':'withdraw_period','type':'datetime-local'}),required=False)
    gender=forms.ChoiceField(choices=options, error_messages={'required':'Gender is required','aria-label':'gender'},widget=forms.Select(attrs={'class':'form-control show-tick ms select2','placeholder':'Gender'}))
    is_active=forms.BooleanField(widget=forms.CheckboxInput(attrs={'aria-label':'is_active','id':'checkbox3'}),required=False)
    test_passed=forms.BooleanField(widget=forms.CheckboxInput(attrs={'aria-label':'test_passed','id':'checkbox4'}),required=False)
    certificate=forms.ImageField(
                                widget=forms.FileInput(attrs={'aria-label':'certificate','class':'custom-file-input','accept':'image/*',}),
                                required=False,
                                validators=[FileExtensionValidator(['jpg','jpeg','png','gif'],message="Invalid image extension",code="invalid_extension")]
                                )
    profile_pic=forms.ImageField(
                                widget=forms.FileInput(attrs={'class':'profile','accept':'image/*','hidden':True}),
                                required=False,
                                validators=[FileExtensionValidator(['jpg','jpeg','png','gif'],message="Invalid image extension",code="invalid_extension")]
                                )
    class Meta:
        model=ExtendedAuthUser
        fields=['test_passed','is_active','withdraw_period','rating','rating_counter','completed_projects','is_deactivated','balance','profile_pic','is_verified','timezone','zipcode','graduation_year','certificate','phone','city','country','gender','citation_style','native_language','academic_degree','university','degree','writer_cv']

    
    def clean_phone(self):
        phone=self.cleaned_data['phone']
        if phone:
            if self.instance.phone:
                if phone != self.instance.phone:
                    if ExtendedAuthUser.objects.filter(phone=phone).exists():
                        raise forms.ValidationError('A user with this phone number already exists.')
                    else:
                        return phone   
                else:
                   return phone
            else:
                if ExtendedAuthUser.objects.filter(phone=phone).exists():
                    raise forms.ValidationError('A user with this phone number already exists.')
                else:
                    return phone
                  

class ExtendedCustomerRegisterForm(forms.ModelForm):
    phone=PhoneNumberField(widget=PhoneNumberPrefixWidget(attrs={'class':'form-control input-rounded','type':'tel','aria-label':'phone','placeholder':'Phone'}),required=False)
    city=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control input-rounded','aria-label':'city'}),required=False)
    country=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control input-rounded','aria-label':'country'}),required=False)
    balance=forms.CharField(widget=forms.NumberInput(attrs={'class':'form-control','aria-label':'balance'}),initial=0,required=False)
    reserved_money=forms.CharField(widget=forms.NumberInput(attrs={'class':'form-control','aria-label':'reserved_money'}),initial=0,required=False)
    completed_projects=forms.CharField(widget=forms.NumberInput(attrs={'class':'form-control','aria-label':'completed_projects'}),initial=0,required=False)
    rating=forms.ChoiceField(choices=[('0','select rating'),('5','5'),('4','4'),('3','3'),('2','2'),('1','1')],widget=forms.Select(attrs={'aria-label':'rating','class':'form-control'}),required=False)
    rating_counter=forms.CharField(widget=forms.NumberInput(attrs={'class':'form-control','aria-label':'rating_counter'}),initial=0,required=False)
    gender=forms.ChoiceField(choices=options, error_messages={'required':'Gender is required','aria-label':'gender'},widget=forms.Select(attrs={'class':'form-control show-tick ms select2','placeholder':'Gender'}),required=False)
    timezone=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control input-rounded','aria-label':'timezone'}),required=False)
    is_verified=forms.BooleanField(widget=forms.CheckboxInput(attrs={'aria-label':'is_verified','id':'checkbox1'}),required=False)
    is_active=forms.BooleanField(widget=forms.CheckboxInput(attrs={'aria-label':'is_active','id':'checkbox1'}),required=False)
    is_deactivated=forms.BooleanField(widget=forms.CheckboxInput(attrs={'aria-label':'is_deactivated','id':'checkbox2'}),required=False)
    class Meta:
        model=ExtendedAuthUser
        fields=['is_deactivated','is_active','rating_counter','completed_projects','rating','is_verified','phone','timezone','city','country','balance','gender','reserved_money',]

    
    def clean_phone(self):
        phone=self.cleaned_data['phone']
        if phone:
            if self.instance.phone:
                if phone != self.instance.phone:
                    if ExtendedAuthUser.objects.filter(phone=phone).exists():
                        raise forms.ValidationError('A user with this phone number already exists.')
                    else:
                        return phone   
                else:
                   return phone
            else:
                if ExtendedAuthUser.objects.filter(phone=phone).exists():
                    raise forms.ValidationError('A user with this phone number already exists.')
                else:
                    return phone


class RegForm(forms.ModelForm):
    email=forms.EmailField(widget=forms.EmailInput(attrs={'class':'form-control input-rounded','placeholder':'Email address','aria-label':'email'}),error_messages={'required':'Email address is required'})
    username=forms.CharField(widget=forms.TextInput(attrs={'id':'reg_username','class':'form-control input-rounded','placeholder':'Username ','aria-label':'username'}),error_messages={'required':'Username is required'})

    class Meta:
        model=User
        fields=['email','username',]

    def clean_email(self):
        email=self.cleaned_data['email']
        if User.objects.filter(email=email).exists():
            user=User.objects.get(email=email)
            if user.is_active:
                if user.extendedauthuser.category =='Admin':
                    raise forms.ValidationError('Already done with this section. <a href="/accounts/login">Login Now</a>')
                elif user.extendedauthuser.category =='customer':
                    if user.extendedauthuser.profile_reg:
                        raise forms.ValidationError('Already done with this section. <a href="/profile/setup/complete">proceed</a>')
                    else:
                        raise forms.ValidationError(f'Already done with this section. <a href="/proceed/{user.id}">proceed</a>')
                else:
                    #writer
                    if user.extendedauthuser.profile_reg and not user.extendedauthuser.exam_complete:
                        raise forms.ValidationError(f'Already done with this section. <a href="/grammer/test/{user.id}">proceed</a>')
                    elif user.extendedauthuser.exam_complete and not user.extendedauthuser.essay_part:
                        raise forms.ValidationError(f'Already done with this section. <a href="/write/essay/{user.id}">proceed</a>')
                    elif user.extendedauthuser.essay_part:
                        raise forms.ValidationError('Already done with this section. <a href="/accounts/login?type=writer">Login Now</a>')
                    else:
                        raise forms.ValidationError(f'Already done with this section. <a href="/proceed/{user.id}">proceed</a>')
            else:
                raise forms.ValidationError('You have already registered this email.Kindly <a target="_blank" href="https://mail.google.com">verify</a> it.')
        try:
            validate_email(email)
        except ValidationError as e:
            raise forms.ValidationError('invalid email address')
        return email
    
    def clean_username(self):
        username=self.cleaned_data['username']
        if User.objects.filter(username=username).exists():
            raise forms.ValidationError('A user with this username already exists')
        return username
class ExtEmailReg(forms.ModelForm):
    category=forms.CharField(widget=forms.EmailInput(attrs={'hidden':True,'class':'form-control input-rounded','placeholder':'Email address','aria-label':'category'}),error_messages={'required':'Category is required'})
    class Meta:
        model=ExtendedAuthUser
        fields=['category',]

#profileForm
class CurrentStaffLoggedInUserProfileChangeForm(UserChangeForm):
    first_name=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control input-rounded'}),required=False)
    last_name=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control input-rounded','aria-label':'last_name'}),error_messages={'required':'Last name is required'})
    username=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control input-rounded','placeholder':'Username ','aria-label':'username'}),error_messages={'required':'Username is required'})
    email=forms.EmailField(widget=forms.EmailInput(attrs={'class':'form-control input-rounded','aria-label':'email'}),error_messages={'required':'Email address is required'})
    is_active=forms.BooleanField(widget=forms.CheckboxInput(attrs={'aria-label':'is_active','id':'checkbox1'}),required=False)
    class Meta:
        model=User
        fields=['first_name','last_name','email','is_active','username',]


    def clean_first_name(self):
        first_name=self.cleaned_data['first_name']
        if not str(first_name).isalpha():
                raise forms.ValidationError('only characters are allowed.')
        return first_name
    
    def clean_last_name(self):
        last_name=self.cleaned_data['last_name']
        if not str(last_name).isalpha():
                raise forms.ValidationError('only characters are allowed.')
        return last_name

    def clean_email(self):
        email=self.cleaned_data['email']
        if email != self.instance.email:
            if User.objects.filter(email=email).exists():
                raise forms.ValidationError('A user with this email already exists.')
            try:
                validate_email(email)
            except ValidationError as e:
                raise forms.ValidationError('Invalid email address.')
            return email
        else:
           return email

    def clean_username(self):
        username=self.cleaned_data['username']
        if username != self.instance.username:
            if User.objects.filter(username=username).exists():
                raise forms.ValidationError('A user with this username already exists')
            return username
        return username


#profileForm
class CurrentLoggedInUserProfileChangeForm(UserChangeForm):
    first_name=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control input-rounded'}),required=False)
    last_name=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control input-rounded','aria-label':'last_name'}),error_messages={'required':'Last name is required'})
    username=forms.CharField(widget=forms.TextInput(attrs={'readonly':True,'class':'form-control input-rounded','placeholder':'Username ','aria-label':'username'}),error_messages={'required':'Username is required'})
    email=forms.EmailField(widget=forms.EmailInput(attrs={'readonly':True,'class':'form-control input-rounded','aria-label':'email'}),error_messages={'required':'Email address is required'})
    is_active=forms.BooleanField(widget=forms.CheckboxInput(attrs={'aria-label':'is_active','id':'checkbox1'}),required=False)
    class Meta:
        model=User
        fields=['first_name','last_name','email','is_active','username',]


    def clean_first_name(self):
        first_name=self.cleaned_data['first_name']
        if not str(first_name).isalpha():
                raise forms.ValidationError('only characters are allowed.')
        return first_name
    
    def clean_last_name(self):
        last_name=self.cleaned_data['last_name']
        if not str(last_name).isalpha():
                raise forms.ValidationError('only characters are allowed.')
        return last_name

    def clean_email(self):
        email=self.cleaned_data['email']
        if email != self.instance.email:
            if User.objects.filter(email=email).exists():
                raise forms.ValidationError('A user with this email already exists.')
            try:
                validate_email(email)
            except ValidationError as e:
                raise forms.ValidationError('Invalid email address.')
            return email
        else:
           return email

    def clean_username(self):
        username=self.cleaned_data['username']
        if username != self.instance.username:
            if User.objects.filter(username=username).exists():
                raise forms.ValidationError('A user with this username already exists')
            return username
        return username

# admin profileForm
class CurrentAdminExtUserProfileChangeForm(forms.ModelForm):
    phone=PhoneNumberField(widget=PhoneNumberPrefixWidget(attrs={'class':'form-control input-rounded','type':'tel','aria-label':'phone','placeholder':'Phone example +25479626...'}),required=False)
    bio=forms.CharField(widget=forms.Textarea(attrs={'class':'form-control','aria-label':'email'}),required=False)
    nickname=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control input-rounded','aria-label':'nickname'}),error_messages={'required':'Nickname is required'})
    company=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control input-rounded','aria-label':'company'}),required=False)
    timezone=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control input-rounded','aria-label':'timezone'}))
    zipcode=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control input-rounded','aria-label':'zipcode'}))
    city=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control input-rounded','aria-label':'city'}),required=False)
    country=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control input-rounded','aria-label':'country'}),required=False)
    gender=forms.ChoiceField(choices=options, error_messages={'required':'Gender is required','aria-label':'gender'},widget=forms.Select(attrs={'class':'form-control show-tick ms select2','placeholder':'Gender'}))
    birthday=forms.DateField(widget=forms.DateInput(attrs={'class':'form-control','aria-label':'birthday','type':'date'}),required=False)   
    profile_pic=forms.ImageField(
                                widget=forms.FileInput(attrs={'class':'profile','accept':'image/*','hidden':True}),
                                required=False,
                                validators=[FileExtensionValidator(['jpg','jpeg','png','gif'],message="Invalid image extension",code="invalid_extension")]
                                )
    class Meta:
        model=ExtendedAuthUser
        fields=['zipcode','timezone','phone','profile_pic','bio','nickname','birthday','gender','company','country','city',]

    
    def clean_phone(self):
        phone=self.cleaned_data['phone']
        if phone != self.instance.phone:
            if ExtendedAuthUser.objects.filter(phone=phone).exists():
                raise forms.ValidationError('A user with this phone number already exists.')
            else:
                return phone
        else:
           return phone 

class UserPasswordChangeForm(UserCreationForm):
    oldpassword=forms.CharField(widget=forms.PasswordInput(attrs={'aria-required':'true','class':'form-control input-rounded','placeholder':'Old password','aria-label':'oldpassword'}),error_messages={'required':'Old password is required','min_length':'enter atleast 6 characters long'})
    password1=forms.CharField(widget=forms.PasswordInput(attrs={'aria-required':'true','class':'form-control input-rounded','placeholder':'New password Eg Example12','aria-label':'password1'}),error_messages={'required':'New password is required','min_length':'enter atleast 6 characters long'})
    password2=forms.CharField(widget=forms.PasswordInput(attrs={'aria-required':'true','class':'form-control input-rounded','placeholder':'Confirm new password','aria-label':'password2'}),error_messages={'required':'Confirm new password is required'})

    class Meta:
        model=User
        fields=['password1','password2']
    
    def clean_oldpassword(self):
        oldpassword=self.cleaned_data['oldpassword']
        if not self.instance.check_password(oldpassword):
            raise forms.ValidationError('Wrong old password.')
        else:
           return oldpassword


class ProfilePicForm(forms.ModelForm):
    profile_pic=forms.ImageField(
                                widget=forms.FileInput(attrs={'class':'profile','accept':'image/*','hidden':True}),
                                required=False,
                                validators=[FileExtensionValidator(['jpg','jpeg','png','gif'],message="Invalid image extension",code="invalid_extension")]
                                )
    class Meta:
        model=ExtendedAuthUser
        fields=['profile_pic',]


#social form
class UserSocialForm(forms.ModelForm):
    facebook=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'facebook','class':'form-control input-rounded','placeholder':'Facebook Link'}),required=False)    
    twitter=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'twitter','class':'form-control input-rounded','placeholder':'Twitter Link'}),required=False)    
    instagram=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'instagram','class':'form-control input-rounded','placeholder':'Instagram Link'}),required=False)    
    linkedin=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'linkedin','class':'form-control input-rounded','placeholder':'Linkedin Link'}),required=False)   
    class Meta:
        model=ExtendedAuthUser
        fields=['facebook','twitter','linkedin','instagram',]

    def clean_facebook(self):
        facebook=self.cleaned_data['facebook']
        if URLValidator(facebook):
                output=urlparse(facebook)
                username=output.path.strip('/')
                if not username:
                    raise forms.ValidationError('Username parameter missing')
                else:
                    return [facebook,username]
        else:
            raise forms.ValidationError('Invalid url')
    
    def clean_twitter(self):
        twitter=self.cleaned_data['twitter']
        if URLValidator(twitter):
                output=urlparse(twitter)
                username=output.path.strip('/')
                if not username:
                    raise forms.ValidationError('Username parameter missing')
                else:
                    return [twitter,username]
        else:
            raise forms.ValidationError('Invalid url')
    

    def clean_instagram(self):
        instagram=self.cleaned_data['instagram']
        if URLValidator(instagram):
                output=urlparse(instagram)
                username=output.path.strip('/')
                if not username:
                    raise forms.ValidationError('Username parameter missing')
                else:
                    return [instagram,username]
        else:
            raise forms.ValidationError('Invalid url')
    
    def clean_linkedin(self):
        linkedin=self.cleaned_data['linkedin']
        if URLValidator(linkedin):
                output=urlparse(linkedin)
                username=output.path.strip('/')
                if not username:
                    raise forms.ValidationError('Username parameter missing')
                else:
                    return [linkedin,username]
        else:
            raise forms.ValidationError('Invalid url')
    
    def clean_youtube(self):
        youtube=self.cleaned_data['youtube']
        if URLValidator(youtube):
                output=urlparse(youtube)
                username=output.path.strip('/')
                if not username:
                    raise forms.ValidationError('Channel id parameter missing')
                else:
                    return [youtube,username]
        else:
            raise forms.ValidationError('Invalid url')
    def clean_whatsapp(self):
        whatsapp=self.cleaned_data['whatsapp']
        if URLValidator(whatsapp):
            output=urlparse(whatsapp)
            username=output.path.strip('/')
            if not username:
                raise forms.ValidationError('username parameter missing')
            else:
                return [whatsapp,username]
        else:
            raise forms.ValidationError('Invalid url')


class SiteForm(forms.ModelForm):
    site_name=forms.CharField(widget=forms.EmailInput(attrs={'aria-label':'site_name','class':'form-control input-rounded','placeholder':'Site name'}),error_messages={'required':'Site Name is required'})
    description=forms.CharField(widget=forms.Textarea(attrs={'aria-label':'description','class':'form-control','placeholder':'Site Description'}),error_messages={'required':'Site Description is required'})
    theme_color=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'theme_color','class':'form-control gradient-colorpicker input-rounded','placeholder':'Site Theme Color eg #ff0000'}),required=False)
    key_words=forms.CharField(widget=forms.TextInput(attrs={'data-role':'tagsinput','aria-label':'key_words','class':'form-control input-rounded ','placeholder':'Site Keywords'}),required=False)
    site_url=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'site_url','class':'form-control input-rounded','placeholder':'Site URL'}),error_messages={'required':'Site URL is required'})
    favicon=forms.ImageField(
                                widget=forms.FileInput(attrs={'aria-label':'favicon','class':'custom-file-input','id':'customFileInput','accept':'image/*','hidden':True}),
                                required=False,
                                validators=[FileExtensionValidator(['jpg','jpeg','png','ico'],message="Invalid image extension",code="invalid_extension")]
                                )
    website_logo=forms.ImageField(
                                widget=forms.FileInput(attrs={'aria-label':'website_logo','class':'custom-file-input','id':'customFileInput1','accept':'image/*','hidden':True}),
                                required=False,
                                validators=[FileExtensionValidator(['jpg','jpeg','png','ico'],message="Invalid image extension",code="invalid_extension")]
                                ) 
    login_logo=forms.ImageField(
                                widget=forms.FileInput(attrs={'aria-label':'login_logo','class':'custom-file-input','id':'customFileInput2','accept':'image/*','hidden':True}),
                                required=False,
                                validators=[FileExtensionValidator(['jpg','jpeg','png','ico'],message="Invalid image extension",code="invalid_extension")]
                                )
    footer_logo=forms.ImageField(
                                widget=forms.FileInput(attrs={'aria-label':'footer_logo','class':'custom-file-input','id':'customFileInput3','accept':'image/*','hidden':True}),
                                required=False,
                                validators=[FileExtensionValidator(['jpg','jpeg','png','ico'],message="Invalid image extension",code="invalid_extension")]
                                )
    email_template_logo=forms.ImageField(
                                widget=forms.FileInput(attrs={'aria-label':'email_template_logo','class':'custom-file-input','id':'customFileInput4','accept':'image/*','hidden':True}),
                                required=False,
                                validators=[FileExtensionValidator(['jpg','jpeg','png','ico'],message="Invalid image extension",code="invalid_extension")]
                                )
    class Meta:
        model=SiteModel
        fields=['site_name','theme_color','site_url','description','key_words','favicon','website_logo','login_logo','footer_logo','email_template_logo',]
    
    def clean_theme_color(self):
        theme_color=self.cleaned_data['theme_color']
        match=re.search(r'^#(?:[0-9a-fA-F]{1,2}){3}$',theme_color)
        if not match:
            raise forms.ValidationError('Invalid color code given')
        else:
            return theme_color
            
    def clean_site_url(self):
        site_url=self.cleaned_data['site_url']
        if URLValidator(site_url):
            return site_url
        else:
            raise forms.ValidationError('Invalid url')


#AddressConfigForm
class AddressConfigForm(forms.ModelForm):
    site_email=forms.EmailField(widget=forms.EmailInput(attrs={'aria-label':'site_email','class':'form-control input-rounded','placeholder':'Site Email Address'}),error_messages={'required':'Address is required'})
    site_email2=forms.EmailField(widget=forms.EmailInput(attrs={'aria-label':'site_email2','class':'form-control input-rounded','placeholder':'Site Additional Email Address'}),required=False)
    address=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'address','class':'form-control input-rounded'}),error_messages={'required':'Address is required'})
    location=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'location','class':'form-control input-rounded'}),error_messages={'required':'Location is required'})
    phone=PhoneNumberField(widget=PhoneNumberPrefixWidget(attrs={'aria-label':'phone','class':'form-control input-rounded'},initial='KE'),required=False)
    class Meta:
        model=SiteModel
        fields=['address','location','phone','site_email','site_email2']
    
    def clean_site_email(self):
        email=self.cleaned_data['site_email']
        if self.instance.site_email:
            if email != self.instance.site_email:
                if User.objects.filter(email=email).exists():
                    raise forms.ValidationError('A user with this email already exists.')
                try:
                    validate_email(email)
                except ValidationError as e:
                    raise forms.ValidationError('Invalid email address.')
                return email
            else:
               return email
        else:
            if User.objects.filter(email=email).exists():
                raise forms.ValidationError('A user with that email already exist')
            try:
                validate_email(email)
            except ValidationError as e:
                raise forms.ValidationError('Invalid email address')
            return email
    

    def clean_site_email2(self):
        email=self.cleaned_data['site_email2']
        if self.instance.site_email2:
            if email != self.instance.site_email2:
                if User.objects.filter(email=email).exists():
                    raise forms.ValidationError('A user with this email already exists.')
                try:
                    validate_email(email)
                except ValidationError as e:
                    raise forms.ValidationError('Invalid email address.')
                return email
            else:
               return email
        else:
            if User.objects.filter(email=email).exists():
                raise forms.ValidationError('A user with that email already exist')
            try:
                validate_email(email)
            except ValidationError as e:
                raise forms.ValidationError('Invalid email address')
            return email


#social form
class SiteSocialForm(forms.ModelForm):
    facebook=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'facebook','class':'form-control input-rounded','placeholder':'Facebook Link'}),required=False)    
    twitter=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'twitter','class':'form-control input-rounded','placeholder':'Twitter Link'}),required=False)    
    instagram=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'instagram','class':'form-control input-rounded','placeholder':'Instagram Link'}),required=False)    
    linkedin=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'linkedin','class':'form-control input-rounded','placeholder':'Linkedin Link'}),required=False)   
    youtube=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'youtube','class':'form-control input-rounded','placeholder':'Youtube Link'}),required=False)    
    whatsapp=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'whatsapp','class':'form-control input-rounded','placeholder':'Whats App'}),required=False)
    class Meta:
        model=SiteModel
        fields=['facebook','twitter','linkedin','instagram','youtube','whatsapp',]

    def clean_facebook(self):
        facebook=self.cleaned_data['facebook']
        if URLValidator(facebook):
                output=urlparse(facebook)
                username=output.path.strip('/')
                if not username:
                    raise forms.ValidationError('Username parameter missing')
                else:
                    return [facebook,username]
        else:
            raise forms.ValidationError('Invalid url')
    
    def clean_twitter(self):
        twitter=self.cleaned_data['twitter']
        if URLValidator(twitter):
                output=urlparse(twitter)
                username=output.path.strip('/')
                if not username:
                    raise forms.ValidationError('Username parameter missing')
                else:
                    return [twitter,username]
        else:
            raise forms.ValidationError('Invalid url')
    

    def clean_instagram(self):
        instagram=self.cleaned_data['instagram']
        if URLValidator(instagram):
                output=urlparse(instagram)
                username=output.path.strip('/')
                if not username:
                    raise forms.ValidationError('Username parameter missing')
                else:
                    return [instagram,username]
        else:
            raise forms.ValidationError('Invalid url')
    
    def clean_linkedin(self):
        linkedin=self.cleaned_data['linkedin']
        if URLValidator(linkedin):
                output=urlparse(linkedin)
                username=output.path.strip('/')
                if not username:
                    raise forms.ValidationError('Username parameter missing')
                else:
                    return [linkedin,username]
        else:
            raise forms.ValidationError('Invalid url')
    
    def clean_youtube(self):
        youtube=self.cleaned_data['youtube']
        if URLValidator(youtube):
                output=urlparse(youtube)
                username=output.path.strip('/')
                if not username:
                    raise forms.ValidationError('Channel id parameter missing')
                else:
                    return [youtube,username]
        else:
            raise forms.ValidationError('Invalid url')
    def clean_whatsapp(self):
        whatsapp=self.cleaned_data['whatsapp']
        if URLValidator(whatsapp):
            output=urlparse(whatsapp)
            username=output.path.strip('/')
            if not username:
                raise forms.ValidationError('username parameter missing')
            else:
                return [whatsapp,username]
        else:
            raise forms.ValidationError('Invalid url')

#WorkingConfigForm
class WorkingConfigForm(forms.ModelForm):
    working_days=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'working_days','class':'form-control input-rounded'}),error_messages={'required':'Working days is required'})
    working_hours=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'working_hours','class':'form-control input-rounded'}),error_messages={'required':'Working hours is required'})

    class Meta:
        model=SiteModel
        fields=['working_days','working_hours',]

class LocalizationForm(forms.ModelForm):
    number_of_words_per_page=forms.CharField(widget=forms.NumberInput(attrs={'aria-label':'number_of_words_per_page','class':'form-control input-rounded','placeholder':'Number of words per page'}),initial=0)
    essay_topic=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'essay_topic','class':'form-control input-rounded','placeholder':'Essay topic'}))
    video_link=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'video_link','class':'form-control input-rounded','placeholder':'Video link'}))
    currency_code=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'currency_code','class':'form-control input-rounded','placeholder':'Currency code'}))
    currency_symbol=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'currency_symbol','class':'form-control input-rounded','placeholder':'Currency symbol'}))
  
    class Meta:
        model=SiteModel
        fields=['number_of_words_per_page','essay_topic','video_link','currency_code','currency_symbol',]

class GrammerForm(forms.ModelForm):
    quiz_time=forms.CharField(widget=forms.NumberInput(attrs={'aria-label':'quiz_time','class':'form-control input-rounded'}),initial=0)
    no_of_quiz_to_display=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'no_of_quiz_to_display','class':'form-control input-rounded'}))
    no_of_correct_ans=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'no_of_correct_ans','class':'form-control input-rounded'}))
  
    class Meta:
        model=SiteModel
        fields=['quiz_time','no_of_quiz_to_display','no_of_correct_ans',]

class WalletForm(forms.ModelForm):
    paypal_client_id=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'paypal_client_id','class':'form-control input-rounded'}))
    tidio_script=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'tidio_script','class':'form-control input-rounded'}),required=False)
    wallet_limit=forms.CharField(widget=forms.NumberInput(attrs={'aria-label':'wallet_limit','class':'form-control input-rounded'}),initial=0)
    min_withdraw=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'min_withdraw','class':'form-control input-rounded'}))
  
    class Meta:
        model=SiteModel
        fields=['wallet_limit','min_withdraw','paypal_client_id','tidio_script',]

class CommissionForm(forms.ModelForm):
    commision_percent=forms.CharField(widget=forms.NumberInput(attrs={'aria-label':'commision_percent','class':'form-control input-rounded'}),initial=0)
    service_charge=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'service_charge','class':'form-control input-rounded'}))
    order_budget=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'order_budget','class':'form-control input-rounded'}))
  
    class Meta:
        model=SiteModel
        fields=['commision_percent','service_charge','order_budget']


class PaymentForm(forms.ModelForm):
    name=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'name','class':'form-control input-rounded'}))
    class Meta:
        model=PaymentModel
        fields=['name',]
    def clean_name(self):
        name=self.cleaned_data['name']
        if self.instance.name:
            if name != self.instance.name:
                if PaymentModel.objects.filter(name=name).exists():
                    raise forms.ValidationError('Payment method already exists')
                else:
                    return name
            else:
                return name
        else:
            if PaymentModel.objects.filter(name=name).exists():
                raise forms.ValidationError('Payment method already exists')
            else:
                return name

class ServiceForm(forms.ModelForm):
    name=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'name','class':'form-control input-rounded'}))
    class Meta:
        model=ServiceModel
        fields=['name',]
    def clean_name(self):
        name=self.cleaned_data['name']
        if self.instance.name:
            if name != self.instance.name:
                if ServiceModel.objects.filter(name=name).exists():
                    raise forms.ValidationError('Data already exists')
                else:
                    return name
            else:
                return name
        else:
            if ServiceModel.objects.filter(name=name).exists():
                raise forms.ValidationError('Data already exists')
            else:
                return name

class AcademicForm(forms.ModelForm):
    name=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'name','class':'form-control input-rounded'}))
    class Meta:
        model=AcademicModel
        fields=['name',]
    def clean_name(self):
        name=self.cleaned_data['name']
        if self.instance.name:
            if name != self.instance.name:
                if ServiceModel.objects.filter(name=name).exists():
                    raise forms.ValidationError('Data already exists')
                else:
                    return name
            else:
                return name
        else:
            if AcademicModel.objects.filter(name=name).exists():
                raise forms.ValidationError('Data already exists')
            else:
  
                return name
class DisplineForm(forms.ModelForm):
    name=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'name','class':'form-control input-rounded'}))
    class Meta:
        model=DisplineModel
        fields=['name',]
    def clean_name(self):
        name=self.cleaned_data['name']
        if self.instance.name:
            if name != self.instance.name:
                if DisplineModel.objects.filter(name=name).exists():
                    raise forms.ValidationError('Data already exists')
                else:
                    return name
            else:
                return name
        else:
            if DisplineModel.objects.filter(name=name).exists():
                raise forms.ValidationError('Data already exists')
            else:
                return name

#Broadcast
class BroadcastForm(forms.ModelForm):
    to=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'to','class':'form-control','placeholder':'To(username)...','list':'userslist'}),required=False)
    message=forms.CharField(widget=forms.Textarea(attrs={'aria-label':'message','class':'form-control input-rounded'}))
    class Meta:
        model=BroadcastModel
        fields=['message','to',]

#IndividualForm
class IndividualForm(forms.ModelForm):
    to=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'to','class':'form-control','placeholder':'To...','list':'userslist'}),required=False)
    message=forms.CharField(widget=forms.Textarea(attrs={'aria-label':'message','class':'form-control input-rounded'}))
    class Meta:
        model=IndividualModel
        fields=['message','to',]


class CitationForm(forms.ModelForm):
    name=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'name','class':'form-control input-rounded'}))
    class Meta:
        model=CitationModel
        fields=['name',]
    def clean_name(self):
        name=self.cleaned_data['name']
        if self.instance.name:
            if name != self.instance.name:
                if CitationModel.objects.filter(name=name).exists():
                    raise forms.ValidationError('Data already exists')
                else:
                    return name
            else:
                return name
        else:
            if CitationModel.objects.filter(name=name).exists():
                raise forms.ValidationError('Data already exists')
            else:
                return name


class LanguageForm(forms.ModelForm):
    name=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'name','class':'form-control input-rounded'}))
    class Meta:
        model=LanguageModel
        fields=['name',]
    def clean_name(self):
        name=self.cleaned_data['name']
        if self.instance.name:
            if name != self.instance.name:
                if LanguageModel.objects.filter(name=name).exists():
                    raise forms.ValidationError('Data already exists')
                else:
                    return name
            else:
                return name
        else:
            if LanguageModel.objects.filter(name=name).exists():
                raise forms.ValidationError('Data already exists')
            else:
                return name

class PaperForm(forms.ModelForm):
    name=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'name','class':'form-control input-rounded'}))
    class Meta:
        model=PaperModel
        fields=['name',]
    def clean_name(self):
        name=self.cleaned_data['name']
        if self.instance.name:
            if name != self.instance.name:
                if PaperModel.objects.filter(name=name).exists():
                    raise forms.ValidationError('Data already exists')
                else:
                    return name
            else:
                return name
        else:
            if PaperModel.objects.filter(name=name).exists():
                raise forms.ValidationError('Data already exists')
            else:
                return name


class GrammarForm(forms.ModelForm):
    question=forms.CharField(widget=forms.Textarea(attrs={'aria-label':'question','class':'form-control input-rounded'}))
    status=forms.BooleanField(widget=forms.CheckboxInput(attrs={'aria-label':'status','id':'checkbox1'}),required=False)
    class Meta:
        model=GrammarModel
        fields=['question','status',]
    def clean_question(self):
        question=self.cleaned_data['question']
        if self.instance.question:
            if question != self.instance.question:
                if GrammarModel.objects.filter(question__icontains=question).exists():
                    raise forms.ValidationError('Question already exists')
                else:
                    return question
            else:
                return question
        else:
            if GrammarModel.objects.filter(question__icontains=question).exists():
                raise forms.ValidationError('Question already exists')
            else:
                return question

class AnswerForm(forms.ModelForm):
    answer1=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'answer1','class':'form-control input-rounded'}))
    answer2=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'answer2','class':'form-control input-rounded'}))
    answer3=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'answer3','class':'form-control input-rounded'}))
    answer4=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'answer4','class':'form-control input-rounded'}))
    correct_answer=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'correct_answer','class':'is-valid form-control input-rounded'}))
    class Meta:
        model=GrammarModel
        fields=['answer1','answer2','answer3','answer4','correct_answer',]


#faq form
class FaqForm(forms.ModelForm):
    category=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'category','class':'form-control'}),error_messages={'required':'Category is required'})
    subject=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'subject','class':'form-control'}),error_messages={'required':'Subject is required'})
    text=forms.CharField(widget=forms.Textarea(attrs={'aria-label':'text','class':'form-control'}),error_messages={'required':'Text is required'})
    class Meta:
        model=FaqModel
        fields=['category','text','subject',]
        

    def clean_text(self):
        text=self.cleaned_data['text']
        if self.instance.text:
            if text != self.instance.text:
                if FaqModel.objects.filter(text__icontains=text).exists():
                    raise forms.ValidationError('Category already exists')
                else:
                    return text
            else:
                return text
        else:
            if FaqModel.objects.filter(text__icontains=text).exists():
                raise forms.ValidationError('Category already exists')
            else:
                return text

class HowItWorksForm(forms.ModelForm):
    category=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'category','class':'form-control'}))
    subject=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'subject','class':'form-control'}))
    text=forms.CharField(widget=forms.Textarea(attrs={'aria-label':'text','class':'form-control'}))
    image=forms.ImageField(
                                widget=forms.FileInput(attrs={'aria-label':'image','class':'custom-file-input','id':'customFileInput','accept':'image/*'}),
                                required=False,
                                validators=[FileExtensionValidator(['jpg','jpeg','png','ico'],message="Invalid image extension",code="invalid_extension")]
                                )
    class Meta:
        model=HowItWorksModel
        fields=['category','text','subject','image',]

class InfotipsForm(forms.ModelForm):
    service_rate_tag=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'service_rate_tag','class':'form-control'}))
    bid_rate_tag=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'bid_rate_tag','class':'form-control'}))
   
    class Meta:
        model=InfotipsModel
        fields=['service_rate_tag','bid_rate_tag',]

    def clean(self):
        cleaned_data=super(InfotipsForm,self).clean()
        if InfotipsModel.objects.count() > 0:
            self.add_error('service_rate_tag','Data more than one is not allowed')
            self.add_error('bid_rate_tag','Data more than one is not allowed')
        return cleaned_data

class CmsForm(forms.ModelForm):
    class Meta:
        model=CmsModel
        fields=['terms_and_conditions',]

class WitersForm(forms.ModelForm):
    class Meta:
        model=CmsModel
        fields=['writers',]

class MoneyForm(forms.ModelForm):
    class Meta:
        model=CmsModel
        fields=['money_back_guarantee',]

class PrivacyForm(forms.ModelForm):
    class Meta:
        model=CmsModel
        fields=['privacy_policy',]

class StudirreForm(forms.ModelForm):
    class Meta:
        model=CmsModel
        fields=['srudirre_help',]

class ConfedialityForm(forms.ModelForm):
    class Meta:
        model=CmsModel
        fields=['confediality_policy',]

class WebmasterForm(forms.ModelForm):
    class Meta:
        model=CmsModel
        fields=['webmaster',]

class WritingForm(forms.ModelForm):
    class Meta:
        model=CmsModel
        fields=['writing_help',]

class ResearchForm(forms.ModelForm):
    class Meta:
        model=CmsModel
        fields=['research_help',]

class ThesisForm(forms.ModelForm):
    class Meta:
        model=CmsModel
        fields=['thesis_help',]

class DissertationForm(forms.ModelForm):
    class Meta:
        model=CmsModel
        fields=['dissertation_help',]

class PersonalForm(forms.ModelForm):
    class Meta:
        model=CmsModel
        fields=['personal_statement',]

class ResourcesForm(forms.ModelForm):
    class Meta:
        model=CmsModel
        fields=['resources',]

class CMSSampleForm(forms.ModelForm):
    class Meta:
        model=CmsModel
        fields=['samples',]

class OneHourForm(forms.ModelForm):
    class Meta:
        model=CmsModel
        fields=['one_hour',]


class OrderForm(forms.ModelForm):
    topic=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'topic','class':'form-control'}))
    pages=forms.CharField(widget=forms.NumberInput(attrs={'aria-label':'pages','class':'form-control'}),initial=0,required=False)
    words_per_page=forms.CharField(widget=forms.NumberInput(attrs={'aria-label':'words_per_page','class':'form-control'}),required=False)
    category=forms.ChoiceField(choices=[('Double Spaced','Double Spaced'),('Single Spaced','Single Spaced')],widget=forms.Select(attrs={'aria-label':'category','class':'form-control'}))
    budget=forms.CharField(widget=forms.NumberInput(attrs={'aria-label':'budget','class':'form-control','readonly':True}),initial=0)
    balance=forms.CharField(widget=forms.NumberInput(attrs={'aria-label':'balance','class':'form-control'}),initial=0,required=False)
    deadline=forms.DateField(widget=forms.DateInput(attrs={'type':'date','aria-label':'deadline','class':'form-control'}))
    citation_style=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'citation_style','class':'form-control'}))
    paper_instructions=forms.CharField(widget=forms.Textarea(attrs={'aria-label':'paper_instructions','class':'form-control'}),required=False)
    tos=forms.BooleanField(widget=forms.CheckboxInput(attrs={'aria-label':'tos','id':'checkbox1'}))
    inviter=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'inviter','class':'form-control','list':'customerlist'}),required=False)
    additional_materials=forms.FileField(
                                widget=forms.FileInput(attrs={'multiple':True,'aria-label':'additional_materials','class':'custom-file-input','id':'customFileInput'}),
                                required=False,
                                )   
    class Meta:
        model=OrderModel
        fields=['category','tos','additional_materials','inviter','words_per_page','topic','pages','budget','deadline','citation_style','paper_instructions']

    def clean_deadline(self):
        deadline=self.cleaned_data.get('deadline',None)
        if deadline < datetime.date.today():
            raise forms.ValidationError("The date cannot be in the past.")
        return deadline


    def clean(self):
        obj=SiteModel.objects.all()[0]
        cleaned_data=super(OrderForm,self).clean()
        budget=int(cleaned_data.get('budget',None))
        balance=float(cleaned_data.get('balance',None))
        if budget < 5:
            self.add_error("budget",f"Minimum budget is {obj.currency_symbol} 5")
        elif balance - budget < 0:
            self.add_error('budget',f'Insufficient balance.Apparent balance is {obj.currency_symbol} {balance}.Please <a href="/account/topup?amount={str(float(balance-budget)).replace("-","")}">top up</a> {obj.currency_symbol}:{str(float(balance-budget)).replace("-","")}')
        return cleaned_data

    def clean_inviter(self):
        inviter=self.cleaned_data['inviter']
        if inviter:
            if User.objects.filter(username=inviter).exists():
                return inviter
            else:
                raise forms.ValidationError('Inviter does not exists.')

class BidForm(forms.ModelForm):
    budget=forms.CharField(widget=forms.NumberInput(attrs={'aria-label':'budget','class':'form-control'}),initial=0,)
    service_charge=forms.CharField(widget=forms.NumberInput(attrs={'aria-label':'service_charge','class':'form-control'}),initial=0,)
    payable=forms.CharField(widget=forms.NumberInput(attrs={'aria-label':'payable','class':'form-control'}),initial=0,required=False)
    tos=forms.BooleanField(widget=forms.CheckboxInput(attrs={'aria-label':'tos','id':'checkbox1'}))
    class Meta:
        model=BidModel
        fields=['price','payable','service_charge',]



class ReviewForm(forms.ModelForm):
    name=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'name','class':'form-control'}))
    rating=forms.ChoiceField(choices=[('0','select rating'),('5','5'),('4','4'),('3','3'),('2','2'),('1','1')],widget=forms.Select(attrs={'aria-label':'rating','class':'form-control'}),required=False)
    title=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'title','class':'form-control'}))
    category=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'category','class':'form-control'}))
    message=forms.CharField(widget=forms.Textarea(attrs={'aria-label':'message','class':'form-control'}))
    image=forms.ImageField(
                                widget=forms.FileInput(attrs={'aria-label':'image','class':'custom-file-input','id':'customFileInput','accept':'image/*'}),
                                required=False,
                                validators=[FileExtensionValidator(['jpg','jpeg','png','ico'],message="Invalid image extension",code="invalid_extension")]
                                ) 
    class Meta:
       model=ReviewModel
       fields=['category','name','rating','title','message','image',]


#SampleForm
class SampleForm(forms.ModelForm):
    topic=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'topic','class':'form-control'}))
    sample=forms.FileField(
                                widget=forms.FileInput(attrs={'aria-label':'sample','class':'custom-file-input','id':'customFileInput','accept':'application/pdf'}),
                                validators=[FileExtensionValidator(['pdf'],message="Invalid file extension",code="invalid_extension")],
                                error_messages={'required':'file name is required'}
                                ) 
    class Meta:
       model=SamplesModel
       fields=['topic','sample',]



class CategoryForm(forms.ModelForm):
    name=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'name','class':'form-control'}))
    class Meta:
        model=CategoryModel
        fields=['name',]

    def clean_name(self):
        name=self.cleaned_data['name']
        if self.instance.name:
            if name != self.instance.name:
                if CategoryModel.objects.filter(name__icontains=name).exists():
                    raise forms.ValidationError('Blog category name already exists')
                else:
                    return name
            else:
                return name
        else:
            if CategoryModel.objects.filter(name__icontains=name).exists():
                raise forms.ValidationError('Blog category name already exists')
            else:
                return name



class WithdrawForm(forms.ModelForm):
    amount=forms.CharField(widget=forms.NumberInput(attrs={'aria-label':'amount','class':'form-control','placeholder':'Enter amount to withdraw'}),initial=0,error_messages={'required':'Amount to withdraw is required'})
    mode=forms.ChoiceField(choices=[('Mpesa','Mpesa'),('Payoneer','Payoneer'),('PayPal','PayPal')],widget=forms.Select(attrs={'aria-label':'mode','class':'form-control'}),required=False)
    class Meta:
        model=WithdrawRequestModel
        fields=['amount','mode',]

    def clean_amount(self):
        amount=int(self.cleaned_data['amount'])
        balance=float(self.instance.balance)
        withdraw_period=self.instance.withdraw_period
        today=datetime.datetime.now()
        results=withdraw_period-today
        obj=SiteModel.objects.all()[0]
        min_withdraw=int(obj.min_withdraw)
        if amount < min_withdraw:
            raise forms.ValidationError(f'Minimum amount to withdraw is {obj.currency_symbol} {min_withdraw}')
        elif balance - amount < 0:
            raise forms.ValidationError(f'Account balance too low.Apparent balance is {obj.currency_symbol} {balance}')
        elif results.days > 0:
                raise forms.ValidationError(f'Kindly wait for {results.days}  before requesting for funds withdrawal.')
        else:
            return amount


class UserPasswordCreateForm(UserCreationForm):
    password1=forms.CharField(widget=forms.PasswordInput(attrs={'aria-required':'true','class':'form-control input-rounded','placeholder':'New password Eg Example12','aria-label':'password1'}),error_messages={'required':'New password is required','min_length':'enter atleast 6 characters long'})
    password2=forms.CharField(widget=forms.PasswordInput(attrs={'aria-required':'true','class':'form-control input-rounded','placeholder':'Confirm new password','aria-label':'password2'}),error_messages={'required':'Confirm new password is required'})

    class Meta:
        model=User
        fields=['password1','password2']


class UserProfile(UserChangeForm):
    first_name=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'first_name','class':'form-control fg-theme','placeholder':'Enter first name'}),error_messages={'required':'First name is required'})
    last_name=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'last_name','class':'form-control','placeholder':'Enter last name'}),error_messages={'required':'Last name is required'})
    email=forms.EmailField(widget=forms.EmailInput(attrs={'aria-label':'email','class':'form-control','placeholder':'Enter email address'}),error_messages={'required':'Email address is required'})
    is_active=forms.BooleanField(widget=forms.CheckboxInput(attrs={'aria-label':'is_active','id':'checkbox1'}),required=False)
    username=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'username','class':'form-control','placeholder':'Enter username'}),error_messages={'required':'Username is required'})
    class Meta:
        model=User
        fields=['is_active','first_name','last_name','username','email',]

    def clean_first_name(self):
        first_name=self.cleaned_data['first_name']
        if not str(first_name).isalpha():
            raise forms.ValidationError('only characters are required')
        return first_name
    def clean_last_name(self):
        last_name=self.cleaned_data['last_name']
        if not str(last_name).isalpha():
            raise forms.ValidationError('only characters are required')
        return last_name

    def clean_email(self):
        email=self.cleaned_data['email']
        if self.instance.email:
            if email != self.instance.email:
                if User.objects.filter(email=email).exists():
                    raise forms.ValidationError('A user with this email already exists.')
                try:
                    validate_email(email)
                except ValidationError as e:
                    raise forms.ValidationError('Invalid email address.')
                return email
            else:
               return email
        else:
            if User.objects.filter(email=email).exists():
                raise forms.ValidationError('A user with that email already exist')
            try:
                validate_email(email)
            except ValidationError as e:
                raise forms.ValidationError('Invalid email address')
            return email

class CurrentWriterExtUserProfileChangeForm(forms.ModelForm):
    phone=PhoneNumberField(widget=PhoneNumberPrefixWidget(attrs={'class':'form-control input-rounded','type':'tel','aria-label':'phone','placeholder':'Phone example +25479626...'}),required=False)
    bio=forms.CharField(widget=forms.Textarea(attrs={'class':'form-control','aria-label':'email'}),required=False)
    nickname=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control input-rounded','aria-label':'nickname'}),error_messages={'required':'Nickname is required'})
    zipcode=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control input-rounded','aria-label':'zipcode'}))
    university=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control input-rounded','aria-label':'university'}))
    degree=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control input-rounded','aria-label':'degree'}))
    timezone=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control input-rounded','aria-label':'timezone'}))
    balance=forms.CharField(widget=forms.NumberInput(attrs={'class':'form-control','aria-label':'balance'}),initial=0,required=False)
    graduation_year=forms.DateField(widget=forms.DateInput(attrs={'class':'form-control','aria-label':'graduation_year','type':'date'}))
    company=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control input-rounded','aria-label':'company'}),required=False)
    city=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control input-rounded','aria-label':'city'}),required=False)
    country=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control input-rounded','aria-label':'country'}),required=False)
    gender=forms.ChoiceField(choices=options, error_messages={'required':'Gender is required','aria-label':'gender'},widget=forms.Select(attrs={'class':'form-control show-tick ms select2','placeholder':'Gender'}))
    birthday=forms.DateField(widget=forms.DateInput(attrs={'class':'form-control','aria-label':'birthday','type':'date'}),required=False)   
    is_active=forms.BooleanField(widget=forms.CheckboxInput(attrs={'aria-label':'is_active','id':'checkbox1'}),required=False)
    bio=forms.CharField(widget=forms.Textarea(attrs={'class':'form-control','aria-label':'email'}),required=False)
    profile_pic=forms.ImageField(
                                widget=forms.FileInput(attrs={'class':'profile','accept':'image/*','hidden':True}),
                                required=False,
                                validators=[FileExtensionValidator(['jpg','jpeg','png','gif'],message="Invalid image extension",code="invalid_extension")]
                                )
    certificate=forms.ImageField(
                            widget=forms.FileInput(attrs={'aria-label':'certificate','class':'custom-file-input','accept':'image/*',}),
                            required=False,
                            validators=[FileExtensionValidator(['jpg','jpeg','png','gif'],message="Invalid image extension",code="invalid_extension")]
                            )
    class Meta:
        model=ExtendedAuthUser
        fields=['balance','native_language','writer_cv','academic_degree','certificate','timezone','graduation_year','degree','university','zipcode','phone','profile_pic','bio','nickname','birthday','gender','is_active','company','country','city',]


class CurrentCustomerExtUserProfileChangeForm(forms.ModelForm):
    phone=PhoneNumberField(widget=PhoneNumberPrefixWidget(attrs={'class':'form-control input-rounded','type':'tel','aria-label':'phone','placeholder':'Phone example +25479626...'}),required=False)
    bio=forms.CharField(widget=forms.Textarea(attrs={'class':'form-control','aria-label':'email'}),required=False)
    nickname=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control input-rounded','aria-label':'nickname'}),error_messages={'required':'Nickname is required'})
    company=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control input-rounded','aria-label':'company'}),required=False)
    timezone=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control input-rounded','aria-label':'timezone'}))
    balance=forms.CharField(widget=forms.NumberInput(attrs={'class':'form-control','aria-label':'balance'}),initial=0,required=False)
    zipcode=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control input-rounded','aria-label':'zipcode'}))
    city=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control input-rounded','aria-label':'city'}),required=False)
    country=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control input-rounded','aria-label':'country'}),required=False)
    gender=forms.ChoiceField(choices=options, error_messages={'required':'Gender is required','aria-label':'gender'},widget=forms.Select(attrs={'class':'form-control show-tick ms select2','placeholder':'Gender'}))
    birthday=forms.DateField(widget=forms.DateInput(attrs={'class':'form-control','aria-label':'birthday','type':'date'}),required=False)   
    is_active=forms.BooleanField(widget=forms.CheckboxInput(attrs={'aria-label':'is_active','id':'checkbox1'}),required=False)
    profile_pic=forms.FileField(
                                widget=forms.FileInput(attrs={'class':'profile','accept':'image/*','hidden':True}),
                                required=False,
                                validators=[FileExtensionValidator(['jpg','jpeg','png','gif'],message="Invalid image extension",code="invalid_extension")]
                                )
    class Meta:
        model=ExtendedAuthUser
        fields=['balance','zipcode','timezone','phone','profile_pic','bio','nickname','birthday','gender','is_active','company','country','city',]

    
    def clean_phone(self):
        phone=self.cleaned_data['phone']
        if phone != self.instance.phone:
            if ExtendedAuthUser.objects.filter(phone=phone).exists():
                raise forms.ValidationError('A user with this phone number already exists.')
            else:
                return phone
        else:
           return phone

#essay
class EssayForm(forms.ModelForm):
    essay=forms.FileField(
                                widget=forms.FileInput(attrs={'aria-label':'essay','class':'custom-file-input','accept':'application/*',}),
                                required=False,
                                validators=[FileExtensionValidator(['txt','pdf','docx',],message="Invalid file extension",code="invalid_extension")]
                                )
    class Meta:
        model=ExtendedAuthUser
        fields=['essay',]


class ExamForm(forms.ModelForm):
    topic=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'topic','class':'form-control'}))
    no_of_mcq_quiz=forms.CharField(widget=forms.NumberInput(attrs={'aria-label':'no_of_mcq_quiz','class':'form-control'}),initial=0)
    no_of_essay_quiz=forms.CharField(widget=forms.NumberInput(attrs={'aria-label':'no_of_essay_quiz','class':'form-control'}),initial=0)
    country=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'country','class':'form-control'}))
    deadline=forms.DateField(widget=forms.DateInput(attrs={'type':'date','aria-label':'deadline','class':'form-control'}))
    time=forms.TimeField(widget=forms.TimeInput(attrs={'type':'time','aria-label':'time','class':'form-control'}))
    time_required_to_finish=forms.CharField(widget=forms.TextInput(attrs={'placeholder':'eg 1hr 30mins','aria-label':'time_required_to_finish','class':'form-control'}))
    tos=forms.BooleanField(widget=forms.CheckboxInput(attrs={'aria-label':'tos','id':'checkbox1'}))
    whatsapp_phone=PhoneNumberField(widget=PhoneNumberPrefixWidget(attrs={'class':'form-control input-rounded','type':'tel','aria-label':'whatsapp_phone','placeholder':'Phone'}),error_messages={'required':'Phone number is required'})
    additional_materials=forms.FileField(
                                widget=forms.FileInput(attrs={'multiple':True,'aria-label':'additional_materials','class':'custom-file-input','id':'customFileInput'}),
                                required=False,
                                )   
    class Meta:
        model=ExamModel
        fields=['whatsapp_phone','additional_materials','time_required_to_finish','time','country','no_of_essay_quiz','no_of_mcq_quiz','deadline','topic',]

    def clean_deadline(self):
        deadline=self.cleaned_data.get('deadline',None)
        if deadline < datetime.date.today():
            raise forms.ValidationError("The date cannot be in the past.")
        return deadline

class BiddingForm(forms.ModelForm):
    budget=forms.CharField(widget=forms.NumberInput(attrs={'aria-label':'budget','class':'form-control'}),initial=0,)
    amount_to_load=forms.CharField(widget=forms.NumberInput(attrs={'aria-label':'amount_to_load','class':'form-control'}),initial=0,)
    class Meta:
        model=OrderModel
        fields=['budget',]

verdict_options=[
                    ('---Select status---',
                            (
                                ('completed','Mark Order Completed'),
                                ('revision','Ask Revision'),
                                ('dispute','Dispute This Order'),
                            )
                    ),
                ]
class FinalForm(forms.ModelForm):
    comment=forms.CharField(widget=forms.Textarea(attrs={'rows':5,'aria-label':'comment','class':'form-control','placeholder':'Leave a comment...'}),required=False)
    rating=forms.ChoiceField(choices=[('0','select rating'),('5','5'),('4','4'),('3','3'),('2','2'),('1','1')],widget=forms.Select(attrs={'aria-label':'rating','class':'form-control'}),required=False)
    verdict=forms.ChoiceField(choices=verdict_options, error_messages={'class':'verdic','required':'Verdict is required','aria-label':'verdict'},widget=forms.Select(attrs={'class':'form-control show-tick ms select2','placeholder':'verdict'}))
    class Meta:
        model=OrderModel
        fields=['verdict','rating','comment',]
    def clean_rating(self):
        rating=int(self.cleaned_data.get('rating',None))
        if rating < 0 or rating > 5:
            raise forms.ValidationError("Rating should be between 0 and 5")
        return rating

class WriterFinalForm(forms.ModelForm):
    comment=forms.CharField(widget=forms.Textarea(attrs={'rows':5,'aria-label':'comment','class':'form-control','placeholder':'Leave a comment...'}),required=False)
    rating=forms.ChoiceField(choices=[('0','select rating'),('5','5'),('4','4'),('3','3'),('2','2'),('1','1')],widget=forms.Select(attrs={'aria-label':'rating','class':'form-control'}),required=False)
    verdict=forms.ChoiceField(choices=verdict_options, error_messages={'class':'verdic','required':'Verdict is required','aria-label':'verdict'},widget=forms.Select(attrs={'class':'form-control show-tick ms select2','placeholder':'verdict'}))
    class Meta:
        model=BidModel
        fields=['verdict','rating','comment',]
    def clean_rating(self):
        rating=int(self.cleaned_data.get('rating',None))
        if rating < 0 or rating > 5:
            raise forms.ValidationError("Rating should be between 0 and 5")
        return rating

class OrderAnswerPad(forms.ModelForm):
    additional_materials=forms.FileField(widget=forms.FileInput(attrs={'multiple':True,'aria-label':'additional_materials','class':'custom-file-input','id':'form_additional_materials'}),error_messages={'required':'Please select file(s) to upload.'})   
    class Meta:
        model=OrderMediaModel
        fields=['additional_materials',]

class AnswerPad(forms.ModelForm):
    additional_materials=forms.FileField(widget=forms.FileInput(attrs={'multiple':True,'aria-label':'additional_materials','class':'custom-file-input','id':'id_additional_materials'}),error_messages={'required':'Please select file(s) to upload.'})   
    class Meta:
        model=SubmittedAnswerModel
        fields=['additional_materials',]

class ChatForm(forms.ModelForm):
    message=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'message','class':'form-control','placeholder':'Enter message','style':'padding-right:40px;'}),error_messages={'required':'Message is required','min_lenghth':'enter atleast 10 characters long message'})
    class Meta:
        model=ChatModel
        fields=['message',]

class CommentForm(forms.ModelForm):
    name=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'name','class':'form-control'}))
    email=forms.EmailField(widget=forms.EmailInput(attrs={'aria-label':'email','class':'form-control'}))
    comment=forms.CharField(widget=forms.Textarea(attrs={'rows':4,'aria-label':'comment','class':'form-control no-resize'}))
    class Meta:
        model=CommentModel
        fields=['name','email','comment',]

class ContentForm(forms.ModelForm):
    topic=forms.CharField(widget=forms.TextInput(attrs={'aria-label':'topic','class':'form-control'}))
    class Meta:
        model=SamplesModel
        fields=['content','topic']