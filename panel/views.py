from django.shortcuts import render
from django.http import JsonResponse,HttpResponse,HttpResponseBadRequest
from django.shortcuts import render,get_object_or_404,redirect
from asgiref.sync import sync_to_async
from installation.models import SiteModel
import time,asyncio
from django.core.paginator import Paginator
from .decorators import unauthenticated_user,allowed_users
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.views.generic import View
from .forms import *
from .search import searchUser
from.utils import get_current_writers
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth import get_user_model
from django.utils.http import urlsafe_base64_decode
import json
from django.views.decorators.cache import cache_page
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.conf import settings
from rest_framework.decorators import api_view
import re
from channels.layers import get_channel_layer
from django.core.files.storage import FileSystemStorage
from django.contrib.auth.hashers import make_password
from django.db.models import Avg
import timeago,datetime
from  django.contrib.auth.models import Group
from datetime import timedelta
import PyPDF2
import os
from datetime import date
CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)
todays_date=date.today()
# Create your views here.

def generate_id():
    return get_random_string(5,'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKMNOPQRSTUVWXYZ0123456789')

def notification(request):
    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.group_send)(
        #roon name
        "notification_admin",
        {
            'type': 'send_notification',
            'message':'notification received'
        }
    )
    return HttpResponse("Done")
    
def check_data():
  if SiteModel.objects.count() > 0:
    obj=SiteModel.objects.all()[0]
    return obj


#Home
class Home(View):
    def get(self,request):
        if not check_data():
            return redirect('/installation/')
        obj=check_data()
        qs=get_current_writers()
        data=User.objects.filter(extendedauthuser__category='writer',extendedauthuser__is_deactivated=False,extendedauthuser__is_verified=True).order_by("-id")
        paginator=Paginator(data,30)
        page_num=request.GET.get('page')
        writers=paginator.get_page(page_num)
        writer_reviews=ReviewModel.objects.filter(category__icontains='writer').order_by("-id")[:4]
        customer_reviews=ReviewModel.objects.filter(category__icontains='customer').order_by("-id")[:4]
        main_writers=User.objects.filter(extendedauthuser__category='writer',extendedauthuser__is_deactivated=False,extendedauthuser__is_verified=True).order_by("-id")[:4]
        papers=PaperModel.objects.all().order_by('-id')[:4]
        active_orders=OrderModel.objects.filter(is_cancelled=False).order_by("-id")[:10]
        updates=UpdatesModel.objects.all().order_by("-id")[:6]
        form=RegForm()
        form1=ExtEmailReg()
        data={
            'title':f'Welcome to {obj.site_name}',
            'obj':obj,
            'data':request.user,
            'online_writers':qs.count(),
            'writers':writers,
            'papers':papers,
            'form':form,
            'updates':updates,
            'form1':form1,
            'active_orders':active_orders,
            'main_writers':main_writers,
            'writer_reviews':writer_reviews,
            'customer_reviews':customer_reviews,
            'room_name':'landing_page',
        }
        return render(request,'manager/index.html',context=data)
    def post(self,request,*args ,**kwargs):
        form=RegForm(request.POST or None)
        form1=ExtEmailReg(request.POST or None)
        if form.is_valid() and form1.is_valid():
            userme=form.save(commit=False)
            userme.is_active=False
            userme.save()
            if not Group.objects.filter(name=form1.cleaned_data.get('category',None)).exists():
                group=Group.objects.create(name=form1.cleaned_data.get('category',None))
                group.user_set.add(userme)
                group.save()
            else:
                group=Group.objects.get(name__icontains=form1.cleaned_data.get('category',None))
                group.user_set.add(userme)
                group.save()
            extended=form1.save(commit=False)
            extended.user=userme
            extended.pending_verification=True
            extended.save()
            return JsonResponse({'category':form1.cleaned_data.get('category',None),'valid':True,'message':'Registered successfuly.','register':True},content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors,'eform_errors':form1.errors},content_type='application/json')

@api_view(['GET',])
def home(request):
    if not check_data():
        return redirect('/installation/')
    obj=check_data()
    qs=get_current_writers()
    data=User.objects.filter(extendedauthuser__category='writer',extendedauthuser__is_deactivated=False,extendedauthuser__is_verified=True).order_by("-id")
    paginator=Paginator(data,30)
    page_num=request.GET.get('page')
    writers=paginator.get_page(page_num)
    writer_reviews=ReviewModel.objects.filter(category__icontains='writer').order_by("-id")[:4]
    customer_reviews=ReviewModel.objects.filter(category__icontains='customer').order_by("-id")[:4]
    main_writers=User.objects.filter(extendedauthuser__category='writer',extendedauthuser__is_deactivated=False,extendedauthuser__is_verified=True).order_by("-id")[:4]
    papers=PaperModel.objects.all().order_by('-id')[:4]
    active_orders=OrderModel.objects.filter(is_cancelled=False).order_by("-id")[:10]
    data={
        'title':f'Welcome to {obj.site_name}',
        'obj':obj,
        'data':request.user,
        'online_writers':qs.count(),
        'writers':writers,
        'papers':papers,
        'active_orders':active_orders,
        'main_writers':main_writers,
        'writer_reviews':writer_reviews,
        'customer_reviews':customer_reviews,
    }
    return render(request,'manager/index.html',context=data)



#Contact
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['writer','customer']),name='dispatch')
class Contact(View):
    def get(self,request):
        form=ContactForm()
        obj=check_data()
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        messages=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).order_by("-id")
        messages_count=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).count()
        inner_qs=IndividualModel.objects.values_list('parent_id',flat=True).distinct()
        qs2=BidModel.objects.filter(user_id=request.user.pk).values_list('user_id',flat=True)
        if inner_qs:
            count1=BroadcastModel.objects.filter(group=True).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
            count2=BroadcastModel.objects.filter(to__icontains=request.user.username).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
        else:
            count1=BroadcastModel.objects.filter(group=True).count()
            count2=BroadcastModel.objects.filter(to__icontains=request.user.username).count()
        broadcasts_count=int(count1+count2)
        data={
                'title':'Contact us',
                'obj':obj,
                'form':form,
                'data':request.user,
                'messages':messages,
                'messages_count':messages_count,
                'broadcasts_count':broadcasts_count,
                'activities':activities,
                'contact_messages':contact_messages,
                'contact_count':contact_count,

            }
        return render(request,'panel/contact.html',context=data)
    def post(self,request,*args ,**kwargs):
        form=ContactForm(request.POST,request.FILES or None)
        if form.is_valid():
            usr=form.save(commit=False)
            usr.initials=form.cleaned_data.get('name')[0].upper()
            usr.phone=request.user.extendedauthuser.phone
            usr.save()
            return JsonResponse({'valid':True,'message':'Request successfuly submitted.'},content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors},content_type='application/json')

#usersContact

class usersContact(View):
    def get(self,request):
        obj=check_data()
        form=ContactForm()
        data={
                'title':'Contact us',
                'obj':obj,
                'form':form,
                'data':request.user,
            }
        return render(request,'manager/contact.html',context=data)
    def post(self,request,*args ,**kwargs):
        form=ContactForm(request.POST,request.FILES or None)
        if form.is_valid():
            usr=form.save(commit=False)
            usr.initials=form.cleaned_data.get('name')[0].upper()
            usr.save()
            return JsonResponse({'valid':True,'message':'Request successfuly submitted.'},content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors},content_type='application/json')

#about
@api_view(['GET',])
def about(request):
    obj=check_data()
    obj=SiteModel.objects.all()[0]
    data=User.objects.filter(extendedauthuser__category='writer',extendedauthuser__is_deactivated=False,extendedauthuser__is_verified=True).order_by("-id")
    paginator=Paginator(data,30)
    page_num=request.GET.get('page')
    writers=paginator.get_page(page_num)
    data={
            'title':'About us',
            'obj':obj,
            'writers':writers,
            'data':request.user,
        }
    return render(request,'manager/about.html',context=data)

#faqs
@api_view(['GET',])
def faqs(request):
    obj=check_data()
    obj=SiteModel.objects.all()[0]
    customers=FaqModel.objects.filter(category__icontains='customer').order_by("-id")
    writers=FaqModel.objects.filter(category__icontains='writer').order_by("-id")
    data={
            'title':'FAQs',
            'obj':obj,
            'customers':customers,
            'writers':writers,
            'data':request.user,
        }
    return render(request,'manager/faqs.html',context=data)

#suspendedAccount
@api_view(['GET',])
def suspendedAccount(request):
    obj=check_data()
    obj=SiteModel.objects.all()[0]
    data={
            'title':'Suspended account',
            'obj':obj,
            'data':request.user,
        }
    return render(request,'manager/suspended.html',context=data)

#allWriters
@api_view(['GET',])
def allWriters(request):
    obj=check_data()
    native_language=request.GET.get('native_language',None)
    citation_style=request.GET.get('citation_style',None)
    if native_language:
        data=User.objects.filter(extendedauthuser__native_language__isnull=False).order_by("-id")
        paginator=Paginator(data,30)
        styles=CitationModel.objects.all().order_by("-id")
        languages=LanguageModel.objects.all().order_by("-id")
        page_num=request.GET.get('page')
        users=paginator.get_page(page_num)
    elif citation_style:
        data=User.objects.filter(extendedauthuser__citation_style__isnull=False).order_by("-id")
        paginator=Paginator(data,30)
        styles=CitationModel.objects.all().order_by("-id")
        languages=LanguageModel.objects.all().order_by("-id")
        page_num=request.GET.get('page')
        users=paginator.get_page(page_num) 
    else:
        data=User.objects.filter(extendedauthuser__category__icontains='writer').order_by("-id")
        paginator=Paginator(data,30)
        styles=CitationModel.objects.all().order_by("-id")
        languages=LanguageModel.objects.all().order_by("-id")
        page_num=request.GET.get('page')
        users=paginator.get_page(page_num)
    data={
            'title':'View All Writers',
            'obj':obj,
            'users':users,
            'styles':styles,
            'languages':languages,
            'count':paginator.count,
            'data':request.user,
        }
    return render(request,'panel/all_writers.html',context=data)

#work
@api_view(['GET',])
def work(request):
    obj=check_data()
    customers=HowItWorksModel.objects.filter(category__icontains='customer').order_by("-id")[:8]
    writers=HowItWorksModel.objects.filter(category__icontains='writer').order_by("-id")[:8]
    data={
            'title':'How it works',
            'obj':obj,
            'customers':customers,
            'writers':writers,
            'data':request.user,
        }
    return render(request,'manager/howitworks.html',context=data)

#terms
@api_view(['GET',])
def terms(request):
    obj=check_data()
    obj=SiteModel.objects.all()[0]
    cms=CmsModel.objects.all().last()
    data={
            'title':'Terms And Conditions',
            'obj':obj,
            'cms':cms,
            'data':request.user,
        }
    return render(request,'manager/terms.html',context=data)


#subscription
@api_view(['POST',])
def subscribe(request,*args,**kwargs):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest' and request.method == 'POST':
        form=SubscriberForm(request.POST or None)
        if form.is_valid():
            form.save()
            return JsonResponse({'valid':True,'message':'success:subscribed successfully'},content_type='application/json')
        return JsonResponse({'valid':False,'uform_errors':form.errors},content_type='application/json')

#writers
@api_view(['GET',])
def writers(request):
    obj=check_data()
    obj=SiteModel.objects.all()[0]
    cms=CmsModel.objects.all().last()
    data={
            'title':f'{obj.site_name} writers',
            'obj':obj,
            'cms':cms,
            'data':request.user,
        }
    return render(request,'manager/studirre_writers.html',context=data)

#moneyback
@api_view(['GET',])
def moneyback(request):
    obj=check_data() 
    cms=CmsModel.objects.all().last()
    data={
            'title':'Money Back Guarantee',
            'obj':obj,
            'cms':cms,
            'data':request.user,
        }
    return render(request,'manager/moneyback.html',context=data)

#privacy
@api_view(['GET',])
def privacyPolicy(request):
    obj=check_data()
    cms=CmsModel.objects.all().last()
    data={
            'title':'Privacy Policy',
            'obj':obj,
            'cms':cms,
            'data':request.user,
        }
    return render(request,'manager/privacy.html',context=data)

#help
@api_view(['GET',])
def siteHelp(request):
    obj=check_data()
    cms=CmsModel.objects.all().last()
    data={
            'title':f'{obj.site_name} Help',
            'obj':obj,
            'cms':cms,
            'data':request.user,
        }
    return render(request,'manager/help.html',context=data)

#confediality_policy
@api_view(['GET',])
def confediality_policy(request):
    obj=check_data()  
    cms=CmsModel.objects.all().last()
    data={
            'title':'Confediality Policy',
            'obj':obj,
            'cms':cms,
            'data':request.user,
        }
    return render(request,'manager/confediality_policy.html',context=data)

#webmaster
@api_view(['GET',])
def webmasterAffliate(request):
    obj=check_data()
    cms=CmsModel.objects.all().last()
    data={
            'title':'webmaster Affliate Program',
            'obj':obj,
            'cms':cms,
            'data':request.user,
        }
    return render(request,'manager/webmaster.html',context=data)

#paper
@api_view(['GET',])
def paperWriting(request):
    obj=check_data()
    cms=CmsModel.objects.all().last()
    data={
            'title':'Paper Writing Help',
            'obj':obj,
            'cms':cms,
            'data':request.user,
        }
    return render(request,'manager/paper.html',context=data)

#research
@api_view(['GET',])
def researchHelp(request):
    obj=check_data()
    cms=CmsModel.objects.all().last()
    data={
            'title':'Research Help',
            'obj':obj,
            'cms':cms,
            'data':request.user,
        }
    return render(request,'manager/research.html',context=data)

#thesis
@api_view(['GET',])
def thesisHelp(request):
    obj=check_data()
    cms=CmsModel.objects.all().last()
    
    data={
            'title':'Thesis Help',
            'obj':obj,
            'cms':cms,
           
            'data':request.user,
        }
    return render(request,'manager/thesis.html',context=data)

#dissertation
@api_view(['GET',])
def dissertationHelp(request):
    obj=check_data()
    cms=CmsModel.objects.all().last()
    
    data={
            'title':'Dissertation Help',
            'obj':obj,
            'cms':cms,
            'data':request.user,
        }
    return render(request,'manager/dissertation.html',context=data)

#personal
@api_view(['GET',])
def personalHelp(request):
    obj=check_data()
    cms=CmsModel.objects.all().last()
    
    data={
            'title':'Personal Statement Help',
            'obj':obj,
            'cms':cms,
            'data':request.user,
        }
    return render(request,'manager/personal.html',context=data)

#resourc
@api_view(['GET',])
def siteResources(request):
    obj=check_data()
    cms=CmsModel.objects.all().last()
    data={
            'title':'Resources',
            'obj':obj,
            'cms':cms,
            'data':request.user,
        }
    return render(request,'manager/resources.html',context=data)

#samples
@api_view(['GET',])
def siteSamples(request):
    obj=check_data()
    cms=CmsModel.objects.all().last()
    data={
            'title':'Samples',
            'obj':obj,
            'cms':cms,
            'data':request.user,
        }
    return render(request,'manager/samples.html',context=data)

#one
@api_view(['GET',])
def oneHour(request):
    obj=check_data()
    cms=CmsModel.objects.all().last()
    data={
            'title':'One Hour Topup Writer',
            'obj':obj,
            'cms':cms, 
            'data':request.user,
        }
    return render(request,'manager/one.html',context=data)

@api_view(['POST',])
def customer_signup(request):
    if request.method == 'POST' and request.headers.get('x-requested-with') == 'XMLHttpRequest':
        form=EmailReg(request.POST or None)
        form1=ExtEmailReg(request.POST or None)
        if form.is_valid() and form1.is_valid():
            userme=form.save(commit=False)
            userme.save()
            extended=form1.save(commit=False)
            extended.user=userme
            extended.category=request.POST['category']
            extended.pending_verification=True
            extended.save()
            updates=UpdatesModel.objects.create(user_id=User.objects.latest('id').id,name=f'User:{form.cleaned_data.get("first_name",None)} {form.cleaned_data.get("last_name",None)} registered.')
            updates.save()
            return JsonResponse({'valid':True,'message':'Registered successfuly.','register':True,'customer':True},content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors,},content_type='application/json')


@method_decorator(unauthenticated_user,name='dispatch')
class Login(View):
    def get(self,request):
        obj=check_data()
        form=UserLoginForm()
        regform=RegForm()
        data={
            'title':'Login',
            'obj':obj,
            'data':request.user,
            'form':form,
            'regform':regform,
            'login':True,
            'room_name':request.user.username,
        }
        return render(request,'panel/login.html',context=data)
    def post(self,request):
        if request.headers.get('x-requested-with') == 'XMLHttpRequest':
            key=request.POST['username']
            password=request.POST['password']
            if key:
                if password:
                    regex=re.compile(r'([A-Za-z0-9+[.-_]])*[A-Za-z0-9]+@[A-Za-z0-9-]+(\.[A-Z/a-z]{2,})+')
                    if re.fullmatch(regex,key):
                        #email address
                        if User.objects.filter(email=key).exists():
                            data=User.objects.get(email=key)
                            exam=data.extendedauthuser.test_passed
                            category=data.extendedauthuser.category
                            if category == 'writer' and exam:
                                activity=ActivityModel.objects.create(icon='<i class="fa fa-user"></i>',title='Login update',user_id=data.pk,name=f'{key} Logged into the account') 
                                activity.save()
                                updates=UpdatesModel.objects.create(user_id=data.pk,name=f'{data.username} Logged into the account')
                                updates.save()
                                user=authenticate(username=data.username,password=password)
                            elif category == 'customer' or data.is_staff:
                                activity=ActivityModel.objects.create(icon='<i class="fa fa-user"></i>',title='Login update',user_id=data.pk,name=f'{key} Logged into the account') 
                                activity.save()
                                updates=UpdatesModel.objects.create(user_id=data.pk,name=f'{data.username} Logged into the account')
                                updates.save()
                                user=authenticate(username=data.username,password=password)
                            else:
                                uform_errors={"username": ["You failed exam you cant login."]}
                                return JsonResponse({'valid':False,'uform_errors':uform_errors},content_type="application/json")
                        else:
                            uform_errors={"username": ["Invalid email address."]}
                            return JsonResponse({'valid':False,'uform_errors':uform_errors},content_type="application/json")
                    else:
                        #username
                        if User.objects.filter(username=key).exists():
                            data=User.objects.get(username=key)
                            exam=data.extendedauthuser.test_passed
                            category=data.extendedauthuser.category
                            if category == 'writer' and exam:
                                activity=ActivityModel.objects.create(icon='<i class="fa fa-user"></i>',title='Login update',user_id=data.pk,name=f'{key} Logged into the account') 
                                activity.save()
                                updates=UpdatesModel.objects.create(user_id=data.pk,name=f'{key} Logged into the account')
                                updates.save()
                                user=authenticate(username=key,password=password)
                            elif category == 'customer' or data.is_staff:
                                activity=ActivityModel.objects.create(icon='<i class="fa fa-user"></i>',title='Login update',user_id=data.pk,name=f'{key} Logged into the account') 
                                activity.save()
                                updates=UpdatesModel.objects.create(user_id=data.pk,name=f'{key} Logged into the account')
                                updates.save()
                                user=authenticate(username=key,password=password)
                            else:
                                uform_errors={"username": ["You failed exam you cant login."]}
                                return JsonResponse({'valid':False,'uform_errors':uform_errors},content_type="application/json")
                        else:
                            uform_errors={"username": ["Invalid username."]}
                            return JsonResponse({'valid':False,'uform_errors':uform_errors},content_type="application/json")
                        
                    if user is not None:
                        if 'rememberme' in request.POST:
                           request.session.set_expiry(1209600) #two weeeks
                        else:
                           request.session.set_expiry(0)
                        login(request,user)
                        return JsonResponse({'valid':True,'message':'success:Login Successfully.','login':True},content_type="application/json")
                    if User.objects.filter(username=key).exists():
                        g=User.objects.get(username=key)
                    else:
                        g=User.objects.get(email=key)

                    uform_errors={"password": ["Password is incorrect."]}
                    return JsonResponse({'status':g.extendedauthuser.is_deactivated,'valid':False,'uform_errors':uform_errors},content_type="application/json")
                else:
                    uform_errors={"password": ["Password is required."]}
                    return JsonResponse({'valid':False,'uform_errors':uform_errors},content_type="application/json")
            else:
                uform_errors={"username": ["Username/Email Address is required."]}
                return JsonResponse({'valid':False,'uform_errors':uform_errors},content_type="application/json")

#logout
@api_view(['GET',])
def user_logout(request):
    activity=ActivityModel.objects.create(icon='<i class="ti-key"></i>',title='Logout update', user_id=request.user.pk,name=f'{request.user.get_full_name()} Logged out of  the account') 
    activity.save()
    logout(request)
    return redirect('/accounts/login')


#activate account view
@api_view(['GET',])
def account_activate(request,category):
    obj=check_data()
    data={'category':category,'title':'Account activate link sent!','obj':obj,'room_name':'',}
    return render(request,'panel/sent_activation_done.html',context=data)


#activate user
def AccountActivator(request,uidb64,token):
    form=get_user_model()
    obj=check_data()
    obj=SiteModel.objects.all()[0]
    try:
        uid=urlsafe_base64_decode(uidb64)
        user=form.objects.get(pk=uid)
    except(TypeError,OverflowError,ValueError,User.DoesNotExist):
        user = None
    if user is not None and create_token.check_token(user, token):
        usr=User.objects.get(id=user.pk)
        usr.is_active = True
        usr.save()
        extended=ExtendedAuthUser.objects.get(user_id=user.pk)
        extended.pending_verification=False
        extended.path="/continue/profile/setup/%s" %usr.username
        extended.save()
        activity=ActivityModel.objects.create(icon='<i class="fa fa-lock"></i>',title='Account Activation',user_id=user.pk,name='You activated account') 
        activity.save()
        updates=UpdatesModel.objects.create(user_id=usr.pk,name=f'User:{usr.get_full_name()} activated account.')
        updates.save()
        message="Thank you for your email confirmation."
        data={
                'valid':True,
                'user':usr,
                'message':message,
                'title':'Account Activation Done',
                'obj':obj,
                'room_name':user.username,
            }
        return render(request,'panel/thank_you.html',context=data)
    else:
        message="Activation link is invalid !"
        data={
                'valid':False,
                'message':message,
                'title':'Account / Register',
                'obj':obj
            }
        return render(request,'panel/thank_you.html',context=data)


#profileSetup
class profileSetup(View):
    def get(self,request,username):
        user=get_object_or_404(User,username=username)
        form=UserChanger(instance=user)
        obj=check_data()
        if user.extendedauthuser.category == 'writer':
            styles=CitationModel.objects.all().order_by("-id")
            languages=LanguageModel.objects.all().order_by("-id")
            degrees=AcademicModel.objects.all().order_by("-id")
            form2=ExtendedRegisterForm(instance=user.extendedauthuser)
        else:
            styles=''
            degrees=''
            languages=''
            form2=ExtendedCustomerRegisterForm(instance=user.extendedauthuser)
        data={
                'valid':True,
                'user':user,
                'title':'Profile Infomation',
                'form2':form2,
                'form':form,
                'obj':obj,
                'styles':styles,
                'languages':languages,
                'degrees':degrees,
                'room_name':user.username,
            }
        return render(request,'panel/register2.html',context=data)

    def post(self,request,username):
        user=get_object_or_404(User,username=username)
        form=UserReg(request.POST or None,instance=user)
        if user.extendedauthuser.category =='customer':
            form1=ExtendedCustomerRegisterForm(request.POST or None,instance=user.extendedauthuser)
        else:
            form1=ExtendedRegisterForm(request.POST or None,instance=user.extendedauthuser)
        if form.is_valid() and form1.is_valid():
            form.save()
            styles=json.dumps(request.POST.getlist('citation_style'))
            language=json.dumps(request.POST.getlist('native_language'))
            degree=json.dumps(request.POST.getlist('academic_degree'))
            save_obj=form1.save(commit=False)
            save_obj.profile_reg=True
            save_obj.initials=form.cleaned_data.get('first_name')[0].upper()+form.cleaned_data.get('last_name')[0].upper()
            if user.extendedauthuser.category =='writer':
                save_obj.citation_style=styles
                save_obj.path="/grammer/test/%s" %str(user.pk)
                save_obj.native_language=language
                save_obj.academic_degree=degree
            else:
                save_obj.path="/accounts/login?type=customer"
            save_obj.save()
            updates=UpdatesModel.objects.create(user_id=user.id,name=f'User:{form.cleaned_data.get("first_name",None)} {form.cleaned_data.get("last_name",None)} joined the community.')
            updates.save()
            return JsonResponse({'profile':True,'valid':True,'message':'Profile saved successfuly.','category':user.extendedauthuser.category,'id':user.id},content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors,'eform_errors':form1.errors},content_type='application/json')

#deleteMedia
@login_required(login_url='/accounts/login')
@api_view(['GET',])
@allowed_users(allowed_roles=['admins','customer'])
def deleteMedia(request,id):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        media=get_object_or_404(OrderMediaModel,id=id)
        media.delete()
        activity=ActivityModel.objects.create(icon='<i class="fa fa-image"></i>',title='Media delete',user_id=request.user.pk,name=f'Media {media.additional_materials} deleted successfuly') 
        activity.save()
        return JsonResponse({'deleted':True,'message':'Media deleted successfuly.'},content_type='application/json')


#deleteMediaExam
@login_required(login_url='/accounts/login')
@api_view(['GET',])
@allowed_users(allowed_roles=['admins','customer'])
def deleteMediaExam(request,id):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        media=get_object_or_404(ExamMediaModel,id=id)
        media.delete()
        activity=ActivityModel.objects.create(icon='<i class="fa fa-image"></i>',title='Media delete',user_id=request.user.pk,name=f'Media {media.additional_materials} deleted successfuly') 
        activity.save()
        return JsonResponse({'deleted':True,'message':'Media deleted successfuly.'},content_type='application/json')


#verdict
@login_required(login_url='/accounts/login')
@api_view(['POST',])
@allowed_users(allowed_roles=['customer','admins'])
def verdict(request,id):
    if request.method == 'POST' and request.headers.get('x-requested-with') == 'XMLHttpRequest':
        obj=check_data()
        #writer details
        order=OrderModel.objects.select_related('user').filter(id__exact=id).last()
        form=FinalForm(request.POST or None,instance=order)
        if form.is_valid():
            usr=form.save(commit=False)
            bid=BidModel.objects.get(order_id=order.order_id)
            customer=ExtendedAuthUser.objects.get(user_id=order.user_id)
            writer=ExtendedAuthUser.objects.get(user_id=bid.user_id)
            verdict=form.cleaned_data.get('verdict',None)
            payable=bid.payable
            time=timeago.format(bid.created_on, datetime.datetime.now())
            creserved=customer.reserved_money
            if 'dispute' in verdict:
                bid.status='Disputed Order'
                usr.status='Disputed Order'
                usr.completer_id=bid.user_id
                activity=ActivityModel.objects.create(icon='<i class="fa fa-money"></i>',title='Order Dispute',user_id=order.user_id,name=f'Dispute arose on order #{order.order_id}') 
                activity.save()
                updates=UpdatesModel.objects.create(user_id=order.user_id,name=f'Dispute on order #{order.order_id} arose.')
                updates.save()
                usr.save()
                completed=False
                subject=f'Order #{order.order_id} status'
                email=writer.user.email
                message={
                            'user':writer.user,
                            'site_name':obj.site_name,
                            'site_url':obj.site_url,
                            'address':obj.address,
                            'location':obj.location,
                            'description':obj.description,
                            'phone':obj.phone,
                            'order_id':order.order_id,
                            'status':verdict,
                            'date':time,
                         } 
                template='emails/order.html'
                send_email(subject,email,message,template)
                verdict_message='Order disputed contact admin'
            elif 'completed' in verdict:
                usr.is_completed=True
                usr.status='Order Completed'
                usr.completer_id=bid.user_id
                bid.status='Order Completed'
                bid.is_completed=True
                bid.client_id=order.user_id
                usr.status='Order Completed'
                usr.inprogress=False
                usr.save()
                bid.save()
                reserved_money=int(creserved)-int(payable)
                customer.reserved_money=reserved_money
                balance=writer.balance
                writer.balance=float(balance) + float(payable)
                writer.withdraw_period=datetime.datetime.today() + timedelta(days=14)
                completed_orders=OrderModel.objects.filter(user_id=order.user_id,is_completed=True).count()
                wcompleted_orders=BidModel.objects.filter(user_id=bid.user_id,is_completed=True).count()
                writer.completed_projects=wcompleted_orders
                customer.completed_projects=completed_orders
                writer.save()
                customer.save()
                activity=ActivityModel.objects.create(icon='<i class="fa fa-money"></i>',title='Order Completed',user_id=order.user_id,name=f'Completed order #{order.order_id}') 
                activity.save()
                updates=UpdatesModel.objects.create(user_id=order.user_id,name=f'Order #{order.order_id} marked completed')
                updates.save()
                transaction=TransactionModel.objects.create(particulars=f'Funds paid for order id #{order.order_id}',status='Completed',user_id=order.user_id,order_id=order.order_id,debit=payable,balance=customer.balance,reserved_money=customer.reserved_money,mode='Wallet')
                transaction.save()
                payments=WriterTransactionModel.objects.create(parent_id=id,particulars=f'Funds received for order id #{order.order_id}',status='Completed',user_id=bid.user_id,order_id=order.order_id,credit=payable,balance=float(balance) + float(payable),mode='Wallet')
                payments.save()
                completed=True
                verdict_message='Order completed successfuly'
            else:
                completed=False
                usr.status='Requires Revision.'
                bid.status='Requires Revision.'
                usr.save()
                bid.save()
                verdict_message='Order asked for revision'

            bid.rating=form.cleaned_data.get('rating',None)
            bid.save()
            avg_rating=BidModel.objects.filter(user_id=bid.user_id).aggregate(Avg('rating'))
            rating_count=BidModel.objects.filter(user_id=bid.user_id,rating__isnull=False).count()
            writer.rating=round(avg_rating['rating__avg'])
            writer.rating_counter=rating_count
            writer.save()
            subject=f'Order #{order.order_id} status'
            email=writer.user.email
            message={
                        'user':writer.user,
                        'site_name':obj.site_name,
                        'site_url':obj.site_url,
                        'address':obj.address,
                        'location':obj.location,
                        'description':obj.description,
                        'phone':obj.phone,
                        'order_id':order.order_id,
                        'status':verdict,
                        'date':time,
                     } 
            template='emails/order.html'
            send_email(subject,email,message,template)
            return JsonResponse({'verdict':True,'completed':completed,'valid':True,'message':verdict_message},content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors},content_type='application/json')


#additionalMaterials
@login_required(login_url='/accounts/login')
@api_view(['POST',])
@allowed_users(allowed_roles=['customer'])
def additionalMaterials(request,id):
    if request.method == 'POST' and request.headers.get('x-requested-with') == 'XMLHttpRequest':
        files=request.FILES.getlist('additional_materials')
        form=OrderAnswerPad(request.POST,request.FILES or None)
        if form.is_valid():
            files=request.FILES.getlist('additional_materials')
            for file in files:
                OrderMediaModel.objects.create(user_id=request.user.pk,parent_id=id,additional_materials=file)
            activity=ActivityModel.objects.create(icon='<i class="fa fa-image"></i>',title='Additional materials',user_id=request.user.pk,name='Added additional materials') 
            activity.save()
            return JsonResponse({'valid':True,'message':'Additional materials uploaded successfuly.'},content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors},content_type='application/json')



#answerMaterials
@login_required(login_url='/accounts/login')
@api_view(['POST',])
@allowed_users(allowed_roles=['writer'])
def answerMaterials(request,id):
    if request.method == 'POST' and request.headers.get('x-requested-with') == 'XMLHttpRequest':
        order=get_object_or_404(OrderModel,id__exact=id)
        site=check_data()
        form=AnswerPad(request.POST,request.FILES or None)
        if form.is_valid():
            files=request.FILES.getlist('additional_materials')
            for index,file in enumerate(files):
                SubmittedAnswerModel.objects.create(user_id=request.user.pk,parent_id=id,additional_materials=file)
            order.is_answered=True
            order.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-image"></i>',title='Answer materials',user_id=request.user.pk,name=f'Uploaded answer materials for order #{order.order_id}') 
            activity.save()
            subject=f'Answer(s) submitted on order #{order.order_id}'
            email=order.user.email
            time=timeago.format(order.created_on, datetime.datetime.now())
            message={
                        'user':request.user,
                        'site_name':site.site_name,
                        'site_url':site.site_url,
                        'address':site.address,
                        'location':site.location,
                        'description':site.description,
                        'phone':site.phone,
                        'order_id':order.order_id,
                        'date':time,
                        'customer':order.user.get_full_name(),
                     } 
            template='emails/answer.html'
            send_email(subject,email,message,template)
            return JsonResponse({'valid':True,'message':'Answer materials uploaded successfuly.'},content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors},content_type='application/json')

class Proceed(View):
    def get(self,request,id):
        user=get_object_or_404(User,pk=id)
        obj=check_data()
        form=UserReg()
        styles=CitationModel.objects.all().order_by("-id")
        languages=LanguageModel.objects.all().order_by("-id")
        degrees=AcademicModel.objects.all().order_by("-id")
        if user.extendedauthuser.category =='customer':
            form2=ExtendedCustomerRegisterForm()
        else:
            form2=ExtendedRegisterForm()
        data={
                'valid':True,
                'user':user,
                'title':'Profile Infomation',
                'form2':form2,
                'form':form,
                'obj':obj,
                'styles':styles,
                'languages':languages,
                'degrees':degrees
            }
        return render(request,'panel/register2.html',context=data)

    def post(self,request,id,*args ,**kwargs):
        user=get_object_or_404(User,pk=id)
        form=UserReg(request.POST or None,instance=user)
        if user.extendedauthuser.category =='customer':
            form1=ExtendedCustomerRegisterForm(request.POST or None,instance=user.extendedauthuser)
        else:
            form1=ExtendedRegisterForm(request.POST or None,instance=user.extendedauthuser)
        if form.is_valid() and form1.is_valid():
            form.save()
            styles=json.dumps(request.POST.getlist('citation_style'))
            language=json.dumps(request.POST.getlist('native_language'))
            degree=json.dumps(request.POST.getlist('academic_degree'))
            save_obj=form1.save(commit=False)
            save_obj.profile_reg=True
            save_obj.citation_style=styles
            save_obj.native_language=language
            save_obj.academic_degree=degree
            save_obj.save()
            updates=UpdatesModel.objects.create(user_id=user.id,name=f'User:{form.cleaned_data.get("first_name",None)} {form.cleaned_data.get("last_name",None)} joined the community.')
            updates.save()
            return JsonResponse({'profile':True,'valid':True,'message':'Profile saved successfuly.','category':user.extendedauthuser.category,'id':user.id},content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors,'eform_errors':form1.errors},content_type='application/json')

#GrammerTest
class GrammerTest(View):
    def get(self,request,id):
        obj=check_data()
        questions=GrammarModel.objects.filter(status=True)
        count=GrammarModel.objects.filter(status=True).count()
        data=get_object_or_404(ExtendedAuthUser,user_id=id)
        data={
                'title':'Grammer Test',
                'obj':obj,
                'id':id,
                'data':data,
                'questions':questions,
                'count':count,
                'room_name':data.user.username,
            }
        return render(request,'panel/grammer.html',context=data)

    def post(self,request,*args ,**kwargs):
        question_id=request.POST['question_id']
        user_id=request.POST['user_id']
        passmark=request.POST['passmark']
        question=GrammarModel.objects.get(id__exact=question_id)
        given_anwer=''
        if 'lastpage' in request.POST:
            result=AnswerModel.objects.filter(user_id=user_id).last()
            count=AnswerModel.objects.filter(user_id=user_id,status=True).count()
            usr=ExtendedAuthUser.objects.get(user_id__exact=user_id)
            if count >= result.passmark:
                usr.test_passed=True
                usr.exam_complete=True
            else:
                usr.exam_complete=True
                usr.test_passed=False
                usr.user.is_active=False
            usr.path="/write/essay/%s" %str(usr.user_id)
            usr.save()
        if 'answer1' in request.POST:
            given_anwer=question.answer1
        elif 'answer2' in request.POST:
            given_anwer=question.answer2
        elif 'answer3' in request.POST:
            given_anwer=question.answer3
        elif 'answer4' in request.POST:
            given_anwer=question.answer4    
        if given_anwer in  question.correct_answer:
            if AnswerModel.objects.filter(question_id=question_id).exists():
                answer=AnswerModel.objects.get(question_id__exact=question_id)
                answer.status=True
                answer.question=question.question
                answer.correct_answer=question.correct_answer
                answer.answer=given_anwer
                answer.passmark=passmark
                answer.user_id=user_id
                answer.save()
                return JsonResponse({'id':user_id,'grammar':True,'answered':True,'message':'Answer submitted'},status=200,content_type='application/json')
            else:
                obj=AnswerModel.objects.create(passmark=passmark,question_id=question_id,status=True,question=question.question,correct_answer=question.correct_answer,answer=given_anwer,user_id=user_id)
                obj.save()
                return JsonResponse({'id':user_id,'grammar':True,'answered':True,'message':'Answer submitted'},status=200,content_type='application/json')
        else:
            if AnswerModel.objects.filter(question_id=question_id).exists():
                answer=AnswerModel.objects.get(question_id__exact=question_id)
                answer.status=False
                answer.question=question.question
                answer.correct_answer=question.correct_answer
                answer.answer=given_anwer
                answer.passmark=passmark
                answer.user_id=user_id
                answer.save()
                return JsonResponse({'id':user_id,'grammar':True,'answered':True,'message':'Answer submitted'},status=200,content_type='application/json')
            else:
                obj=AnswerModel.objects.create(passmark=passmark,question_id=question_id,status=False,question=question.question,correct_answer=question.correct_answer,answer=given_anwer,user_id=user_id)
                obj.save()
                return JsonResponse({'id':user_id,'grammar':True,'answered':True,'message':'Answer submitted'},status=200,content_type='application/json')

#elapsed
@api_view(['GET',])
def elapsed(request,id):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        user=get_object_or_404(User,id__exact=id)
        user.is_active=False
        user.extendedauthuser.test_passed=False
        user.extendedauthuser.test_reason='Time elapsed before finish'
        user.save()
        return JsonResponse({'valid':False,'message':'We are sorry but you have failed the test.','failed':True},status=200,content_type='application/json')



#essay
class Essay(View):
    def get(self,request,id):
        obj=check_data()
        user=get_object_or_404(ExtendedAuthUser,user_id__exact=id)
        form=EssayForm(instance=user)
        data={
                'title':'Write essay',
                'obj':obj,
                'id':id,
                'form':form,
                'user':user,
                'room_name':user.user.username,
            }
        return render(request,'panel/essay.html',context=data)
    def post(self,request,id,*args ,**kwargs):
        user=get_object_or_404(ExtendedAuthUser,user_id__exact=id)
        form=EssayForm(request.POST,request.FILES or None,instance=user)
        if form.is_valid():
            usr=form.save(commit=False)
            usr.essay_part=True
            usr.path="/accounts/login?type=writer"
            usr.save()
            return JsonResponse({'category':user.category,'id':id,'valid':True,'message':'Essay submitted successfuly.','essay':True},content_type='application/json')
        else:
            return JsonResponse({'id':id,'valid':False,'uform_errors':form.errors},content_type='application/json')

#gotodashboard
@api_view(['GET',])
def gotodashboard(request,id):
    obj=check_data()
    user=get_object_or_404(ExtendedAuthUser,user_id__exact=id)
    data={
            'title':'Write essay Complete',
            'obj':obj,
            'user':user,
            'room_name':user.user.username,
        }
    return render(request,'panel/gotodashboard.html',context=data)

#loginnow
@api_view(['GET',])
def loginnow(request):
    obj=check_data()
    data={
            'title':'Profile Setup Complete',
            'obj':obj,
            'room_name':'',
        }
    return render(request,'panel/loginnow.html',context=data)





#dashboard
@login_required(login_url='/accounts/login')
def dashboard(request):
    obj=check_data()
    qs=get_current_writers()
    activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
    if request.user.extendedauthuser.category == 'Admin':

        #count
        admin_compcount=OrderModel.objects.filter(is_completed=True,is_cancelled=False).count()
        writers_count=User.objects.filter(is_active=True,extendedauthuser__category__icontains='writer').count()
        users_count=User.objects.exclude(is_superuser=True).count()
        grammas=User.objects.filter(extendedauthuser__is_verified=False,extendedauthuser__category='writer').order_by("-id")[:5]
        deactivated_users=User.objects.filter(extendedauthuser__is_deactivated=True).order_by("-id")[:7]
        cancelled_orders=OrderModel.objects.filter(is_completed=False,is_cancelled=True).order_by("-id")[:5]
        payments=WithdrawRequestModel.objects.filter(is_accepted=False).order_by("-id")[:10]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        data={
            'title':'Dashboard',
            'obj':obj,
            'writers_count':writers_count,
            'admin_compcount':admin_compcount,
            'online_writers':qs.count(),
            'data':request.user,
            'users_count':users_count,
            'activities':activities,
            'grammas':grammas,
            'contact_messages':contact_messages,
            'contact_count':contact_count,
            'deactivated_users':deactivated_users,
            'cancelled_orders':cancelled_orders,
            'payments':payments,
            'room_name':request.user.username,
        }
    elif request.user.extendedauthuser.category == 'writer':
        active_orders_count=BidModel.objects.filter(user_id=request.user.pk,is_accepted=False,has_bid=False).select_related('parent').count()
        bids=BidModel.objects.filter(user_id__exact=request.user.pk,has_bid=True,is_accepted=False).count()
        inprogress_writer_orders_count=BidModel.objects.filter(user_id=request.user.pk,is_accepted=True,is_completed=False,parent__is_accepted=True,parent__is_completed=False,parent__is_answered=False).count()
        inprogress_writer_submitted_orders_count=BidModel.objects.filter(user_id=request.user.pk,is_accepted=True,is_completed=False,parent__is_accepted=True,parent__is_completed=False,parent__is_answered=True).count()
        pending_count=BidModel.objects.select_related('parent').filter(is_accepted=False,user_id=request.user.pk).count()
        pending_approvals=BidModel.objects.filter(user_id=request.user.pk,has_bid=True,is_accepted=False,parent__is_accepted=False).order_by("-id")[:5]
        messages=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).order_by("-id")
        messages_count=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).count()
        inner_qs=IndividualModel.objects.values_list('parent_id',flat=True).distinct()
        qs1=BidModel.objects.values_list('parent_id',flat=True).distinct()
        qs2=BidModel.objects.filter(user_id=request.user.pk).values_list('user_id',flat=True)
        if request.user.extendedauthuser.is_verified:
            if qs1:
                new_projects=OrderModel.objects.filter(is_cancelled=False,is_completed=False,is_public=True,is_accepted=False).exclude(bidmodel__id__in=qs1).exclude(bidmodel__user_id__in=qs2).count()
            else:
                new_projects=OrderModel.objects.select_related('user').filter(is_cancelled=False,is_completed=False,is_public=True,is_accepted=False).count()
        else:
            new_projects=''

        if inner_qs:
            count1=BroadcastModel.objects.filter(group=True).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
            count2=BroadcastModel.objects.filter(to__icontains=request.user.username).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
        else:
            count1=BroadcastModel.objects.filter(group=True).count()
            count2=BroadcastModel.objects.filter(to__icontains=request.user.username).count()
        broadcasts_count=int(count1+count2)
        data={
            'title':'Dashboard',
            'obj':obj,
            'active_orders_count':active_orders_count,
            'bids':bids,
            'data':request.user,
            'messages':messages,
            'messages_count':messages_count,
            'inprogress_writer_orders_count':inprogress_writer_orders_count,
            'activities':activities,
            'pending_count':pending_count,
            'pending_approvals':pending_approvals,
            'room_name':request.user.username,
            'broadcasts_count':broadcasts_count,
            'new_projects':new_projects,
            'inprogress_writer_submitted_orders_count':inprogress_writer_submitted_orders_count,
        }
    else:
        customer_bids=OrderModel.objects.filter(user_id=request.user.pk,is_accepted=False,is_cancelled=False,is_completed=False).count()
        inprogress_orders_count=OrderModel.objects.filter(user_id=request.user.pk,is_cancelled=False,is_accepted=True,is_completed=False,is_answered=False).count()
        last_activities=OrderModel.objects.filter(user_id=request.user.pk).order_by("-id")[:3]
        messages=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).order_by("-id")
        messages_count=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).count()
        inner_qs=IndividualModel.objects.values_list('parent_id',flat=True).distinct()
        qs2=BidModel.objects.filter(user_id=request.user.pk).values_list('user_id',flat=True)
        if inner_qs:
            count1=BroadcastModel.objects.filter(group=True).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
            count2=BroadcastModel.objects.filter(to__icontains=request.user.username).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
        else:
            count1=BroadcastModel.objects.filter(group=True).count()
            count2=BroadcastModel.objects.filter(to__icontains=request.user.username).count()
        broadcasts_count=int(count1+count2)
        data={
            'title':'Dashboard',
            'obj':obj,
            'active_orders_writers':qs.count(),
            'customer_bids':customer_bids,
            'data':request.user,
            'messages':messages,
            'messages_count':messages_count,
            'activities':activities,
            'last_activities':last_activities,
            'room_name':request.user.username,
            'inprogress_orders_count':inprogress_orders_count,
            'broadcasts_count':broadcasts_count,
        }
    return render(request,'panel/dashboard.html',context=data)


#ProfileView
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
class ProfileView(View):
    def get(self,request,username):
        obj=check_data()
        user = get_object_or_404(User,username=username)
        if request.user.is_superuser:
            form=CurrentStaffLoggedInUserProfileChangeForm(instance=user)
            eform=CurrentAdminExtUserProfileChangeForm(instance=user.extendedauthuser)
        elif request.user.extendedauthuser.category == 'customer':
            form=CurrentLoggedInUserProfileChangeForm(instance=user)
            eform=CurrentCustomerExtUserProfileChangeForm(instance=user.extendedauthuser)
        else:
            form=CurrentLoggedInUserProfileChangeForm(instance=user)
            eform=CurrentWriterExtUserProfileChangeForm(instance=user.extendedauthuser)
        styles=CitationModel.objects.all().order_by("-id")
        languages=LanguageModel.objects.all().order_by("-id")
        degrees=AcademicModel.objects.all().order_by("-id")
        passform=UserPasswordChangeForm()
        profileform=ProfilePicForm()
        inprogress_orders_count=OrderModel.objects.filter(user_id=request.user.pk,is_cancelled=False,is_accepted=True,is_completed=False).count()
        form1=UserSocialForm(instance=user.extendedauthuser)
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        if request.user.extendedauthuser.category == 'writer':
            comments=OrderModel.objects.filter(completer_id=request.user.pk).values('comment','rating').order_by("-id")[:6]
        else:
            comments=BidModel.objects.filter(client_id=request.user.pk).values('comment','rating').order_by("-id")[:6]
        messages=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).order_by("-id")
        messages_count=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).count()
        inner_qs=IndividualModel.objects.values_list('parent_id',flat=True).distinct()
        qs2=BidModel.objects.filter(user_id=request.user.pk).values_list('user_id',flat=True)
        if inner_qs:
            count1=BroadcastModel.objects.filter(group=True).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
            count2=BroadcastModel.objects.filter(to__icontains=request.user.username).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
        else:
            count1=BroadcastModel.objects.filter(group=True).count()
            count2=BroadcastModel.objects.filter(to__icontains=request.user.username).count()
        broadcasts_count=int(count1+count2)
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        data={
            'title':f'Edit profile / {user.get_full_name()}',
            'obj':obj,
            'data':request.user,
            'form':form,
            'eform':eform,
            'activities':activities,
            'inprogress_orders_count':inprogress_orders_count,
            'styles':styles,
            'comments':comments,
            'broadcasts_count':broadcasts_count,
            'messages_count':messages_count,
            'messages':messages,
            'languages':languages,
            'degrees':degrees,
            'editor':user,
            'passform':passform,
            'profileform':profileform,
            'form1':form1,
            'room_name':request.user.username,
            'contact_messages':contact_messages,
            'contact_count':contact_count,
        }
        return render(request,'panel/profile.html',context=data)
 
    def post(self,request,username,*args ,**kwargs):
        if request.user.is_superuser:
            form=CurrentStaffLoggedInUserProfileChangeForm(request.POST or None,instance=request.user)
            eform=CurrentAdminExtUserProfileChangeForm(request.POST,request.FILES or None, instance=request.user.extendedauthuser)
        elif request.user.extendedauthuser.category == 'customer':
            form=CurrentLoggedInUserProfileChangeForm(request.POST or None,instance=request.user)
            eform=CurrentCustomerExtUserProfileChangeForm(request.POST,request.FILES or None,instance=request.user.extendedauthuser)
        else:
            form=CurrentLoggedInUserProfileChangeForm(request.POST or None,instance=request.user)
            eform=CurrentWriterExtUserProfileChangeForm(request.POST,request.FILES or None,instance=request.user.extendedauthuser)
        styles=json.dumps(request.POST.getlist('citation_style'))
        language=json.dumps(request.POST.getlist('native_language'))
        degree=json.dumps(request.POST.getlist('academic_degree'))
        if form.is_valid() and eform.is_valid():
            form.save()
            save_obj=eform.save(commit=False)
            save_obj.citation_style=styles
            save_obj.native_language=language
            save_obj.academic_degree=degree
            save_obj.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-user"></i>',title='Profile update',user_id=request.user.pk,name='Made some changes to your profile') 
            activity.save()
            return JsonResponse({'valid':True,'message':'Profile updated successfully.','profile_pic':True},content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors,'eform_errors':eform.errors,},content_type='application/json')


#EditSite
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')
class EditSite(View):
    def get(self,request):
        obj=check_data()
        form1=SiteForm(instance=obj)
        contacts=ContactModel.objects.filter(is_read=False).order_by("-id")[:7]
        contacts_count=ContactModel.objects.filter(is_read=False).count()
        form2=AddressConfigForm(instance=obj)
        form3=SiteSocialForm(instance=obj)
        form4=WorkingConfigForm(instance=obj)
        form5=LocalizationForm(instance=obj)
        form6=GrammerForm(instance=obj)
        form7=WalletForm(instance=obj)
        form8=CommissionForm(instance=obj)
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        data={
            'title':'Edit Site Settings',
            'obj':obj,
            'data':request.user,
            'form1':form1,
            'form2':form2,
            'activities':activities,
            'form3':form3,
            'form4':form4,
            'form5':form5,
            'form6':form6,
            'form7':form7,
            'form8':form8,
            'contacts':contacts,
            'contacts_count':contacts_count,
            'room_name':request.user.username,
            'contact_messages':contact_messages,
            'contact_count':contact_count,
        }
        return render(request,'panel/site.html',context=data)
    def post(self,request,*args , **kwargs):
        if request.headers.get('x-requested-with') == 'XMLHttpRequest':
            instance_data=SiteModel.objects.all().first()
            form=SiteForm(request.POST,request.FILES or None , instance=instance_data)
            if form.is_valid():
                form.save()
                activity=ActivityModel.objects.create(icon='<i class="fa fa-cog"></i>',title='Site Settings',user_id=request.user.pk,name='Edited site settings') 
                activity.save()
                return JsonResponse({'valid':True,'message':'data saved successfully'},status=200,content_type='application/json')
            else:
                return JsonResponse({'valid':False,'uform_errors':form.errors},status=200,content_type='application/json')


#siteWorking
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['POST',])
def siteWorking(request):
    if request.method == 'POST' and request.headers.get('x-requested-with') == 'XMLHttpRequest':
        instance_data=SiteModel.objects.all().first()
        form=WorkingConfigForm(request.POST, request.FILES or None , instance=instance_data)
        if form.is_valid():
            form.save()
            return JsonResponse({'valid':True,'message':'data saved successfully'},status=200,content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors},status=200,content_type='application/json')

#siteContact
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['POST',])
def siteContact(request):
    if request.method == 'POST' and request.headers.get('x-requested-with') == 'XMLHttpRequest':
        instance_data=SiteModel.objects.all().first()
        form=AddressConfigForm(request.POST or None , instance=instance_data)
        if form.is_valid():
            form.save()
            return JsonResponse({'valid':True,'message':'data saved successfully'},status=200,content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors},status=200,content_type='application/json')


#siteSocial
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['POST',])
def siteSocial(request):
    if request.method == 'POST' and request.headers.get('x-requested-with') == 'XMLHttpRequest':
        instance_data=SiteModel.objects.all().first()
        form=SiteSocialForm(request.POST or None , instance=instance_data)
        if form.is_valid():
            link=form.save(commit=False)
            link.facebook=request.POST['facebook']
            link.twitter=request.POST['twitter']
            link.instagram=request.POST['instagram']
            link.linkedin=request.POST['linkedin']
            link.whatsapp=request.POST['whatsapp']
            link.youtube=request.POST['youtube']
            link.save()
            return JsonResponse({'valid':True,'message':'data saved successfully'},status=200,content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors},status=200,content_type='application/json')

#localization
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['POST',])
def localization(request):
    if request.method == 'POST' and request.headers.get('x-requested-with') == 'XMLHttpRequest':
        instance_data=SiteModel.objects.all().first()
        form=LocalizationForm(request.POST or None , instance=instance_data)
        if form.is_valid():
            form.save()
            return JsonResponse({'valid':True,'message':'data saved successfully'},status=200,content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors},status=200,content_type='application/json')

#grammer
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['POST',])
def grammer(request):
    if request.method == 'POST' and request.headers.get('x-requested-with') == 'XMLHttpRequest':
        instance_data=SiteModel.objects.all().first()
        form=GrammerForm(request.POST or None , instance=instance_data)
        if form.is_valid():
            form.save()
            return JsonResponse({'valid':True,'message':'data saved successfully'},status=200,content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors},status=200,content_type='application/json')

#wallet
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['POST',])
def wallet(request):
    if request.method == 'POST' and request.headers.get('x-requested-with') == 'XMLHttpRequest':
        instance_data=SiteModel.objects.all().first()
        form=WalletForm(request.POST or None , instance=instance_data)
        if form.is_valid():
            form.save()
            return JsonResponse({'valid':True,'message':'data saved successfully'},status=200,content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors},status=200,content_type='application/json')

#commision
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['POST',])
def commision(request):
    if request.method == 'POST' and request.headers.get('x-requested-with') == 'XMLHttpRequest':
        instance_data=SiteModel.objects.all().first()
        form=CommissionForm(request.POST or None , instance=instance_data)
        if form.is_valid():
            form.save()
            return JsonResponse({'valid':True,'message':'data saved successfully'},status=200,content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors},status=200,content_type='application/json')


#payment
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def payment(request):
    obj=check_data()
    data=PaymentModel.objects.all().order_by("-id")
    paginator=Paginator(data,30)
    page_num=request.GET.get('page')
    payments=paginator.get_page(page_num)
    contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
    contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
    activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
    data={
        'title':'Payment Methods',
        'obj':obj,
        'data':request.user,
        'activities':activities,
        'payments':payments,
        'count':paginator.count,
        'room_name':request.user.username,
        'contact_count':contact_count,
        'contact_messages':contact_messages,
    }
    return render(request,'panel/payment.html',context=data)



@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')
class AddPayment(View):
    def get(self,request):
        obj=check_data()
        form=PaymentForm()
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        data={
            'title':'Add payment method',
            'obj':obj,
            'data':request.user,
            'activities':activities,
            'form':form,
            'prev':'Payment Methods',
            'room_name':request.user.username,
            'contact_messages':contact_messages,
            'contact_count':contact_count,
        }
        return render(request,'panel/add_payment.html',context=data)
    def post(self,request):
        form=PaymentForm(request.POST or None)
        if form.is_valid():
            usr=form.save(commit=False)
            usr.user_id=request.user.pk
            usr.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-money"></i>',title='payment method',user_id=request.user.pk,name='Added payment method') 
            activity.save()
            return JsonResponse({'valid':True,'message':'Payment method added successfully.'},content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors},content_type='application/json')

#edit payment
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')
class EditPayment(View):
    def get(self ,request,id):
        obj=check_data()
        user = get_object_or_404(PaymentModel,id=id)
        form=PaymentForm(instance=user)
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        data={
            'title':f'Edit payment method / {user.name}',
            'obj':obj,
            'data':request.user,
            'form':form,
            'activities':activities,
            'prev':'Payment Methods',
            'admin':user,
            'edit':True,
            'room_name':request.user.username,
            'contact_count':contact_count,
            'contact_messages':contact_messages,
        }
        return render(request,'panel/add_payment.html',context=data)

    def post(self,request,id,*args ,**kwargs):
        user=get_object_or_404(PaymentModel,id=id)
        form=PaymentForm(request.POST or None,instance=user)
        if form.is_valid():
            form.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-money"></i>',title='payment method',user_id=request.user.pk,name='Edited payment method') 
            activity.save()
            return JsonResponse({'valid':True,'message':'Payment method updated successfuly.'},content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors,},content_type='application/json')





#delete payment method
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def deletePayment(request,id):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        try:
            obj=get_object_or_404(PaymentModel,id__exact=id)
            activity=ActivityModel.objects.create(icon='<i class="fa fa-money"></i>',title='Payment method',user_id=request.user.pk,name='Deleted payment method') 
            activity.save()
            obj.delete() 
            return JsonResponse({'deleted':False,'message':'Payment method deleted successfully.','id':id},content_type='application/json')       
        except PaymentModel.DoesNotExist:
            return JsonResponse({'deleted':True,'message':'Item does not exist'},content_type='application/json')


#service
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def service(request):
    obj=check_data()
    data=ServiceModel.objects.all().order_by("-id")
    paginator=Paginator(data,30)
    page_num=request.GET.get('page')
    payments=paginator.get_page(page_num)
    activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
    contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
    contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
    data={
        'title':'Service Types',
        'obj':obj,
        'data':request.user,
        'prev':'Service Types',
        'activities':activities,
        'payments':payments,
        'count':paginator.count,
        'room_name':request.user.username,
        'contact_messages':contact_messages,
        'contact_count':contact_count,
    }
    return render(request,'panel/service.html',context=data)


@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')
class AddService(View):
    def get(self,request):
        obj=check_data()
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        form=ServiceForm()
        data={
            'title':'Add service types',
            'obj':obj,
            'data':request.user,
            'form':form,
            'activities':activities,
            'prev':'Service Types',
            'room_name':request.user.username,
            'contact_count':contact_count,
            'contact_messages':contact_messages,
        }
        return render(request,'panel/add_payment.html',context=data)
    def post(self,request):
        form=ServiceForm(request.POST or None)
        if form.is_valid():
            usr=form.save(commit=False)
            usr.user_id=request.user.pk
            usr.save()
            activity=ActivityModel.objects.create(user_id=request.user.pk,name=f'Added service type {form.cleaned_data.get("name",None)}') 
            activity.save()
            return JsonResponse({'valid':True,'message':'Service type added successfully.'},content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors},content_type='application/json')

#edit service
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')
class EditService(View):
    def get(self ,request,id):
        obj=check_data()
        user = get_object_or_404(ServiceModel,id=id)
        form=ServiceForm(instance=user)
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        data={
            'title':f'Edit service type / {user.name}',
            'obj':obj,
            'data':request.user,
            'form':form,
            'admin':user,
            'activities':activities,
            'prev':'Service Types',
            'edit':True,
            'room_name':request.user.username,
            'contact_messages':contact_messages,
            'contact_count':contact_count,
        }
        return render(request,'panel/add_payment.html',context=data)

    def post(self,request,id,*args ,**kwargs):
        user=get_object_or_404(ServiceModel,id=id)
        form=ServiceForm(request.POST or None,instance=user)
        if form.is_valid():
            form.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-cog"></i>',title='Service Type',user_id=request.user.pk,name=f'Edited service type {form.cleaned_data.get("name",None)}') 
            activity.save()
            return JsonResponse({'valid':True,'message':'Service type updated successfuly.'},content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors,},content_type='application/json')

#delete service type
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def deleteService(request,id):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        try:
            obj=ServiceModel.objects.get(id__exact=id)
            activity=ActivityModel.objects.create(icon='<i class="fa fa-cog"></i>',title='Service Type',user_id=request.user.pk,name=f'Deleted service type {obj.name}') 
            activity.save()
            obj.delete() 
            return JsonResponse({'deleted':False,'message':'Service type deleted successfully.','id':id},content_type='application/json')       
        except ServiceModel.DoesNotExist:
            return JsonResponse({'deleted':True,'message':'Item does not exist'},content_type='application/json')


#onlineUsers
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def onlineUsers(request):
    obj=check_data()
    data=User.objects.filter(is_staff=False,extendedauthuser__is_online=True,extendedauthuser__category='writer').order_by("-id")
    paginator=Paginator(data,30)
    page_num=request.GET.get('page')
    writers=paginator.get_page(page_num)

    data1=User.objects.filter(is_staff=False,extendedauthuser__is_online=True,extendedauthuser__category='customer').order_by("-id")
    paginator1=Paginator(data1,30)
    page_num1=request.GET.get('page')
    customers=paginator.get_page(page_num1)
    activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
    contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
    contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
    data={
        'title':'Online Users',
        'obj':obj,
        'data':request.user,
        'activities':activities,
        'writers':writers,
        'customers':customers,
        'wcount':paginator.count,
        'ccount':paginator1.count,
        'room_name':request.user.username,
        'contact_count':contact_count,
        'contact_messages':contact_messages,
    }
    return render(request,'panel/online_users.html',context=data)


#manageSamples
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def manageSamples(request):
    obj=check_data()
    data=SamplesModel.objects.all().order_by("-id")
    paginator=Paginator(data,30)
    page_num=request.GET.get('page')
    samples=paginator.get_page(page_num)
    activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
    contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
    contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
    data={
        'title':'Manage Samples',
        'obj':obj,
        'data':request.user,
        'activities':activities,
        'samples':samples,
        'count':paginator.count,
        'room_name':request.user.username,
        'contact_messages':contact_messages,
        'contact_count':contact_count,
    }
    return render(request,'panel/manage_samples.html',context=data)

#displine
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def displine(request):
    obj=check_data()
    data=DisplineModel.objects.all().order_by("-id")
    paginator=Paginator(data,30)
    page_num=request.GET.get('page')
    payments=paginator.get_page(page_num)
    activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
    contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
    contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
    data={
        'title':'Manage Displines',
        'obj':obj,
        'data':request.user,
        'activities':activities,
        'payments':payments,
        'count':paginator.count,
        'room_name':request.user.username,
        'contact_count':contact_count,
        'contact_messages':contact_messages,
    }
    return render(request,'panel/displines.html',context=data)


#AddBroadcast
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')
class AddBroadcast(View):
    def get(self,request):
        obj=check_data()
        form=BroadcastForm()
        form1=IndividualForm()
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        users=User.objects.exclude(is_staff=True,is_active=False,is_superuser=True).order_by("-id")[:50]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        data={
            'title':'Add Broadcast Message',
            'obj':obj,
            'data':request.user,
            'form':form,
            'form1':form1,
            'users':users,
            'broadcast':True,
            'activities':activities,
            'room_name':request.user.username,
            'contact_messages':contact_messages,
            'contact_count':contact_count,
        }
        return render(request,'panel/add_broadcast.html',context=data)
    def post(self,request):
        form=BroadcastForm(request.POST or None)
        if form.is_valid():
            usr=form.save(commit=False)
            usr.user_id=request.user.pk
            usr.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-cog"></i>',title='Broadcast Message',user_id=request.user.pk,name=f'Added new broadcast message') 
            activity.save()
            return JsonResponse({'valid':True,'message':'Broadcast message added successfully.'},content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors},content_type='application/json')


#AddSingleBroadcast
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')
class AddSingleBroadcast(View):
    def get(self,request):
        obj=check_data()
        form1=BroadcastForm()
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        users=User.objects.exclude(is_staff=True,is_active=False,is_superuser=True).order_by("-id")[:500]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        data={
            'title':'Add Broadcast Message',
            'obj':obj,
            'data':request.user,
            'form1':form1,
            'users':users,
            'broadcast':False,
            'activities':activities,
            'room_name':request.user.username,
            'contact_count':contact_count,
            'contact_messages':contact_messages,
        }
        return render(request,'panel/add_broadcast.html',context=data)
    def post(self,request):
        form=BroadcastForm(request.POST or None)
        if form.is_valid():
            site=check_data()
            usr=form.save(commit=False)
            usr.user_id=request.user.pk
            usr.group=False
            usr.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-cog"></i>',title='Broadcast Message',user_id=request.user.pk,name=f'Added new broadcast message') 
            activity.save()
            user=get_object_or_404(User,username__icontains=form.cleaned_data.get('to',None))
            messages_count=BroadcastModel.objects.filter(to__icontains=form.cleaned_data.get('to',None),is_read=False).count()
            subject=f'You have {messages_count} new unread message(s) from management'
            email=user.email
            message={
                        'user':user,
                        'site_name':site.site_name,
                        'site_url':site.site_url,
                        'address':site.address,
                        'location':site.location,
                        'description':site.description,
                        'phone':site.phone,
                        'message_count':messages_count,
                     } 
            template='emails/messages.html'
            send_email(subject,email,message,template)
            return JsonResponse({'valid':True,'message':'Broadcast message added successfully.'},content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors},content_type='application/json')


#EditSingleBroadcast
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')
class EditSingleBroadcast(View):
    def get(self ,request,id):
        obj=check_data()
        user = get_object_or_404(BroadcastModel,id=id)
        form1=IndividualForm(instance=user)
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        data={
            'title':f'Edit Broadcast Message / {user.message}',
            'obj':obj,
            'data':request.user,
            'form1':form1,
            'activities':activities,
            'edit':True,
            'broadcast':False,
            'room_name':request.user.username,
            'contact_messages':contact_messages,
            'contact_count':contact_count,
        }
        return render(request,'panel/add_broadcast.html',context=data)

    def post(self,request,id,*args ,**kwargs):
        user=get_object_or_404(BroadcastModel,id=id)
        form=IndividualForm(request.POST or None,instance=user)
        if form.is_valid():
            form.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-cog"></i>',title='Broadcast Message',user_id=request.user.pk,name=f'Edited broadcasted message.') 
            activity.save()
            return JsonResponse({'valid':True,'message':'Broadcast message updated successfuly.'},content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors,},content_type='application/json')

#EditBroadcast
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')
class EditBroadcast(View):
    def get(self ,request,id):
        obj=check_data()
        user = get_object_or_404(BroadcastModel,id=id)
        form=BroadcastForm(instance=user)
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        data={
            'title':f'Edit Broadcast Message / {user.message}',
            'obj':obj,
            'data':request.user,
            'form':form,
            'activities':activities,
            'edit':True,
            'broadcast':True,
            'room_name':request.user.username,
            'contact_count':contact_count,
            'contact_messages':contact_messages,
        }
        return render(request,'panel/add_broadcast.html',context=data)

    def post(self,request,id,*args ,**kwargs):
        user=get_object_or_404(BroadcastModel,id=id)
        form=BroadcastForm(request.POST or None,instance=user)
        if form.is_valid():
            saver=form.save(commit=False)
            saver.is_read=False
            saver.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-cog"></i>',title='Broadcast Message',user_id=request.user.pk,name=f'Edited broadcasted message.') 
            activity.save()
            return JsonResponse({'valid':True,'message':'Broadcast message updated successfuly.'},content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors,},content_type='application/json')


#deleteSamples
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def deleteSamples(request,id):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        try:
            obj=SamplesModel.objects.get(id__exact=id)
            obj.delete() 
            activity=ActivityModel.objects.create(icon='<i class="fa fa-cog"></i>',title='Sample paper',user_id=request.user.pk,name=f'Deleted sample paper topic:{obj.topic} successfully') 
            activity.save()
            return JsonResponse({'deleted':False,'message':'Sample paper deleted successfully.','id':id},content_type='application/json')       
        except DisplineModel.DoesNotExist:
            return JsonResponse({'deleted':True,'message':'Item does not exist'},content_type='application/json')


#deleteBroadcast
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def deleteBroadcast(request,id):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        try:
            obj=BroadcastModel.objects.get(id__exact=id)
            obj.delete() 
            activity=ActivityModel.objects.create(icon='<i class="fa fa-cog"></i>',title='Broadcast Message',user_id=request.user.pk,name=f'Deleted broadcasted message successfully') 
            activity.save()
            return JsonResponse({'deleted':False,'message':'Broadcasted Message deleted successfully.','id':id},content_type='application/json')       
        except DisplineModel.DoesNotExist:
            return JsonResponse({'deleted':True,'message':'Item does not exist'},content_type='application/json')

#SamplePaper
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')
class SamplePaper(View):
    def get(self,request):
        obj=check_data()
        form=ContentForm()
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        data={
            'title':'Add Sample Paper',
            'obj':obj,
            'data':request.user,
            'form':form,
            'activities':activities,
            'room_name':request.user.username,
            'contact_messages':contact_messages,
            'contact_count':contact_count,
        }
        return render(request,'panel/add_sample.html',context=data)
    def post(self,request):
        form=ContentForm(request.POST,request.FILES or None)
        if form.is_valid():
            usr=form.save(commit=False)
            usr.user_id=request.user.pk
            usr.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-plus"></i>',title='Sample Paper',user_id=request.user.pk,name=f'Added sample paper of topic {form.cleaned_data.get("topic",None)}') 
            activity.save()
            return JsonResponse({'valid':True,'message':'Sample paper added successfully.'},content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors},content_type='application/json')



@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')
class AddDispline(View):
    def get(self,request):
        obj=check_data()
        form=DisplineForm()
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        data={
            'title':'Add Displine',
            'obj':obj,
            'data':request.user,
            'form':form,
            'activities':activities,
            'prev':'Displines',
            'room_name':request.user.username,
            'contact_count':contact_count,
            'contact_messages':contact_messages,
        }
        return render(request,'panel/add_payment.html',context=data)
    def post(self,request):
        form=DisplineForm(request.POST or None)
        if form.is_valid():
            usr=form.save(commit=False)
            usr.user_id=request.user.pk
            usr.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-cog"></i>',title='Displine',user_id=request.user.pk,name=f'Added displine {form.cleaned_data.get("name",None)}') 
            activity.save()
            return JsonResponse({'valid':True,'message':'Displine added successfully.'},content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors},content_type='application/json')

#EditDispline
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')
class EditDispline(View):
    def get(self ,request,id):
        obj=check_data()
        user = get_object_or_404(DisplineModel,id=id)
        form=DisplineForm(instance=user)
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        data={
            'title':f'Edit Displine / {user.name}',
            'obj':obj,
            'data':request.user,
            'form':form,
            'admin':user,
            'activities':activities,
            'prev':'Displines',
            'edit':True,
            'room_name':request.user.username,
            'contact_messages':contact_messages,
            'contact_count':contact_count,
        }
        return render(request,'panel/add_payment.html',context=data)

    def post(self,request,id,*args ,**kwargs):
        user=get_object_or_404(DisplineModel,id=id)
        form=DisplineForm(request.POST or None,instance=user)
        if form.is_valid():
            form.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-cog"></i>',title='Displine',user_id=request.user.pk,name=f'Edited displine {form.cleaned_data.get("name",None)}') 
            activity.save()
            return JsonResponse({'valid':True,'message':'Displine updated successfuly.'},content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors,},content_type='application/json')

#deleteDispline
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def deleteDispline(request,id):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        try:
            obj=DisplineModel.objects.get(id__exact=id)
            obj.delete() 
            activity=ActivityModel.objects.create(icon='<i class="fa fa-cog"></i>',title='Displine',user_id=request.user.pk,name=f'Deleted displine {obj.name}') 
            activity.save()
            return JsonResponse({'deleted':False,'message':'Displine deleted successfully.','id':id},content_type='application/json')       
        except DisplineModel.DoesNotExist:
            return JsonResponse({'deleted':True,'message':'Item does not exist'},content_type='application/json')

#citation
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def citation(request):
    obj=check_data()
    data=CitationModel.objects.all().order_by("-id")
    paginator=Paginator(data,30)
    activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
    page_num=request.GET.get('page')
    payments=paginator.get_page(page_num)
    contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
    contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
    data={
        'title':'citation Styles',
        'obj':obj,
        'data':request.user,
        'activities':activities,
        'payments':payments,
        'count':paginator.count,
        'room_name':request.user.username,
        'contact_count':contact_count,
        'contact_messages':contact_messages,
    }
    return render(request,'panel/citation.html',context=data)

#add citation
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')
class AddCitation(View):
    def get(self,request):
        obj=check_data()
        form=CitationForm()
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        data={
            'title':'Add Citation',
            'obj':obj,
            'data':request.user,
            'form':form,
            'activities':activities,
            'prev':'Citation Styles',
            'room_name':request.user.username,
            'contact_messages':contact_messages,
            'contact_count':contact_count,
        }
        return render(request,'panel/add_payment.html',context=data)
    def post(self,request):
        form=CitationForm(request.POST or None)
        if form.is_valid():
            usr=form.save(commit=False)
            usr.user_id=request.user.pk
            usr.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-cog"></i>',title='Citation Style',user_id=request.user.pk,name=f'Added citation style {form.cleaned_data.get("name",None)}') 
            activity.save()
            return JsonResponse({'valid':True,'message':'Citation style added successfully.'},content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors},content_type='application/json')


#EditCitation
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')
class EditCitation(View):
    def get(self ,request,id):
        obj=check_data()
        user = get_object_or_404(CitationModel,id=id)
        form=CitationForm(instance=user)
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        data={
            'title':f'Edit citation style / {user.name}',
            'obj':obj,
            'data':request.user,
            'form':form,
            'admin':user,
            'activities':activities,
            'prev':'Citation style',
            'edit':True,
            'room_name':request.user.username,
            'contact_count':contact_count,
            'contact_messages':contact_messages,
        }
        return render(request,'panel/add_payment.html',context=data)

    def post(self,request,id,*args ,**kwargs):
        user=get_object_or_404(CitationModel,id=id)
        form=CitationForm(request.POST or None,instance=user)
        if form.is_valid():
            form.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-cog"></i>',title='Citation Style',user_id=request.user.pk,name=f'Edited citation style {form.cleaned_data.get("name",None)}') 
            activity.save()
            return JsonResponse({'valid':True,'message':'Citation style updated successfuly.'},content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors,},content_type='application/json')

#deleteCitation
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def deleteCitation(request,id):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        try:
            obj=CitationModel.objects.get(id__exact=id)
            activity=ActivityModel.objects.create(icon='<i class="fa fa-cog"></i>',title='Citation Style',user_id=request.user.pk,name=f'Deleted citation style {obj.name}') 
            activity.save()
            obj.delete() 
            return JsonResponse({'deleted':False,'message':'Citation style deleted successfully.','id':id},content_type='application/json')       
        except CitationModel.DoesNotExist:
            return JsonResponse({'deleted':True,'message':'Item does not exist'},content_type='application/json')


#academic
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def academicDegree(request):
    obj=check_data()
    data=AcademicModel.objects.all().order_by("-id")
    paginator=Paginator(data,30)
    activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
    page_num=request.GET.get('page')
    payments=paginator.get_page(page_num)
    contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
    contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
    data={
        'title':'Academic Degrees',
        'obj':obj,
        'data':request.user,
        'activities':activities,
        'payments':payments,
        'count':paginator.count,
        'room_name':request.user.username,
        'contact_messages':contact_messages,
        'contact_count':contact_count,

    }
    return render(request,'panel/academic.html',context=data)


#add academic degree
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')
class AddAcademic(View):
    def get(self,request):
        obj=check_data()
        form=AcademicForm()
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        data={
            'title':'Add Academic Degree',
            'obj':obj,
            'data':request.user,
            'form':form,
            'activities':activities,
            'prev':'Academic Degrees',
            'room_name':request.user.username,
            'contact_count':contact_count,
            'contact_messages':contact_messages,
    
        }
        return render(request,'panel/add_payment.html',context=data)
    def post(self,request):
        form=AcademicForm(request.POST or None)
        if form.is_valid():
            usr=form.save(commit=False)
            usr.user_id=request.user.pk
            usr.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-cog"></i>',title='Academic Degree',user_id=request.user.pk,name=f'Added academic degree {form.cleaned_data.get("name",None)}') 
            activity.save()
            return JsonResponse({'valid':True,'message':'Academic degree added successfully.'},content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors},content_type='application/json')


#EditAcademic
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')

class EditAcademic(View):
    def get(self ,request,id):
        obj=check_data()
        user = get_object_or_404(AcademicModel,id=id)
        form=AcademicForm(instance=user)
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        data={
            'title':f'Edit item / {user.name}',
            'obj':obj,
            'data':request.user,
            'activities':activities,
            'form':form,
            'admin':user,
            'prev':'Academic Degrees',
            'edit':True,
            'room_name':request.user.username,
            'contact_messages':contact_messages,
            'contact_count':contact_count,  
    
        }
        return render(request,'panel/add_payment.html',context=data)

    def post(self,request,id,*args ,**kwargs):
        user=get_object_or_404(AcademicModel,id=id)
        form=AcademicForm(request.POST or None,instance=user)
        if form.is_valid():
            form.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-cog"></i>',title='Academic Degree',user_id=request.user.pk,name=f'Edited academic degree {form.cleaned_data.get("name",None)}') 
            activity.save()
            return JsonResponse({'valid':True,'message':'Item updated successfuly.'},content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors,},content_type='application/json')

#deleteAcademic
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def deleteAcademic(request,id):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        try:
            obj=AcademicModel.objects.get(id__exact=id)
            obj.delete()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-cog"></i>',title='Academic Degree',user_id=request.user.pk,name=f'Deleted academic degree {obj.name}') 
            activity.save()
            return JsonResponse({'deleted':False,'message':'Academic degree deleted successfully.','id':id},content_type='application/json')       
        except AcademicModel.DoesNotExist:
            return JsonResponse({'deleted':True,'message':'Item does not exist'},content_type='application/json')


#language
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def language(request):
    obj=check_data()
    data=LanguageModel.objects.all().order_by("-id")
    paginator=Paginator(data,30)
    activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
    contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
    contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
    page_num=request.GET.get('page')
    payments=paginator.get_page(page_num)
    data={
        'title':'Languages',
        'obj':obj,
        'data':request.user,
        'payments':payments,
        'activities':activities,
        'count':paginator.count,
        'room_name':request.user.username,
        'contact_messages':contact_messages,
        'contact_count':contact_count,  

    }
    return render(request,'panel/language.html',context=data)

#AddLanguage
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')
class AddLanguage(View):
    def get(self,request):
        obj=check_data()
        form=LanguageForm()
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        data={
            'title':'Add Language',
            'obj':obj,
            'data':request.user,
            'form':form,
            'activities':activities,
            'prev':'Languages',
            'room_name':request.user.username,
            'contact_messages':contact_messages,
            'contact_count':contact_count,  
    
        }
        return render(request,'panel/add_payment.html',context=data)
    def post(self,request):
        form=LanguageForm(request.POST or None)
        if form.is_valid():
            usr=form.save(commit=False)
            usr.user_id=request.user.pk
            usr.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-language"></i>',title='language',user_id=request.user.pk,name=f'Added language {form.cleaned_data.get("name",None)}') 
            activity.save()
            return JsonResponse({'valid':True,'message':'Language added successfully.'},content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors},content_type='application/json')


#EditLanguage
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')
class EditLanguage(View):
    def get(self ,request,id):
        obj=check_data()
        user = get_object_or_404(LanguageModel,id=id)
        form=LanguageForm(instance=user)
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        data={
            'title':f'Edit item / {user.name}',
            'obj':obj,
            'data':request.user,
            'form':form,
            'admin':user,
            'activities':activities,
            'prev':'Languages',
            'edit':True,
            'room_name':request.user.username,
            'contact_messages':contact_messages,
            'contact_count':contact_count,  
        }
        return render(request,'panel/add_payment.html',context=data)

    def post(self,request,id,*args ,**kwargs):
        user=get_object_or_404(LanguageModel,id=id)
        form=LanguageForm(request.POST or None,instance=user)
        if form.is_valid():
            form.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-language"></i>',title='language',user_id=request.user.pk,name=f'Edited language {form.cleaned_data.get("name",None)}') 
            activity.save()
            return JsonResponse({'valid':True,'message':'Item updated successfuly.'},content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors,},content_type='application/json')

#deleteLanguage
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def deleteLanguage(request,id):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        try:
            obj=LanguageModel.objects.get(id__exact=id)
            obj.delete() 
            activity=ActivityModel.objects.create(icon='<i class="fa fa-language"></i>',title='language',user_id=request.user.pk,name=f'Deleted language {obj.name}') 
            activity.save()
            return JsonResponse({'deleted':False,'message':'Language deleted successfully.','id':id},content_type='application/json')       
        except LanguageModel.DoesNotExist:
            return JsonResponse({'deleted':True,'message':'Item does not exist'},content_type='application/json')


#paper
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def paperType(request):
    obj=check_data()
    data=PaperModel.objects.all().order_by("-id")
    paginator=Paginator(data,30)
    activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
    contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
    contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
    page_num=request.GET.get('page')
    payments=paginator.get_page(page_num)
    data={
        'title':'Paper Types',
        'obj':obj,
        'data':request.user,
        'activities':activities,
        'payments':payments,
        'count':paginator.count,
        'room_name':request.user.username,
        'contact_messages':contact_messages,
        'contact_count':contact_count,  
    }
    return render(request,'panel/paper.html',context=data)

#AddPaper
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')
class AddPaper(View):
    def get(self,request):
        obj=check_data()
        form=PaperForm()
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        data={
            'title':'Add Paper Type',
            'obj':obj,
            'data':request.user,
            'form':form,
            'activities':activities,
            'prev':'Paper Types',
            'room_name':request.user.username,
            'contact_messages':contact_messages,
            'contact_count':contact_count,  
        }
        return render(request,'panel/add_payment.html',context=data)
    def post(self,request):
        form=PaperForm(request.POST or None)
        if form.is_valid():
            usr=form.save(commit=False)
            usr.user_id=request.user.pk
            usr.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-envelope"></i>',title='Paper type',user_id=request.user.pk,name=f'Added paper type {form.cleaned_data.get("name",None)}') 
            activity.save()
            return JsonResponse({'valid':True,'message':'Paper type added successfully.'},content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors},content_type='application/json')

#EditPaper
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')

class EditPaper(View):
    def get(self ,request,id):
        obj=check_data()
        user = get_object_or_404(PaperModel,id=id)
        form=PaperForm(instance=user)
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        data={
            'title':f'Edit item / {user.name}',
            'obj':obj,
            'data':request.user,
            'form':form,
            'admin':user,
            'activities':activities,
            'prev':'Paper Types',
            'edit':True,
            'room_name':request.user.username,
            'contact_messages':contact_messages,
            'contact_count':contact_count,  
        }
        return render(request,'panel/add_payment.html',context=data)

    def post(self,request,id,*args ,**kwargs):
        user=get_object_or_404(PaperModel,id=id)
        form=PaperForm(request.POST or None,instance=user)
        if form.is_valid():
            form.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-envelope"></i>',title='Paper type',user_id=request.user.pk,name=f'Edited paper type {form.cleaned_data.get("name",None)}') 
            activity.save()
            return JsonResponse({'valid':True,'message':'Item updated successfuly.'},content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors,},content_type='application/json')

#deletePaper
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def deletePaper(request,id):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        try:
            obj=PaperModel.objects.get(id__exact=id)
            obj.delete() 
            activity=ActivityModel.objects.create(icon='<i class="fa fa-envelope"></i>',title='Paper type',user_id=request.user.pk,name=f'Deleted paper type {obj.name}') 
            activity.save()
            return JsonResponse({'deleted':False,'message':'Paper type deleted successfully.','id':id},content_type='application/json')       
        except PaperModel.DoesNotExist:
            return JsonResponse({'deleted':True,'message':'Item does not exist'},content_type='application/json')


#questions
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
def questions(request):
    obj=check_data()
    data=GrammarModel.objects.all().order_by("-id")
    paginator=Paginator(data,30)
    activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
    contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
    contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
    page_num=request.GET.get('page')
    questions=paginator.get_page(page_num)
    data={
        'title':'Grammar Questions',
        'obj':obj,
        'data':request.user,
        'activities':activities,
        'questions':questions,
        'count':paginator.count,
        'contact_messages':contact_messages,
        'contact_count':contact_count,  

    }
    return render(request,'panel/questions.html',context=data)

#AddQuestion
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')
class AddQuestion(View):
    def get(self,request):
        obj=check_data()
        form=GrammarForm()
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        data={
            'title':'Add Question',
            'obj':obj,
            'data':request.user,
            'form':form,
            'prev':'Questions',
            'activities':activities,
            'room_name':request.user.username,
            'contact_messages':contact_messages,
            'contact_count':contact_count,  
    
        }
        return render(request,'panel/add_question.html',context=data)
    def post(self,request):
        form=GrammarForm(request.POST or None)
        if form.is_valid():
            usr=form.save(commit=False)
            usr.user_id=request.user.pk
            usr.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-envelope"></i>',title='Grammar',user_id=request.user.pk,name=f'Added grammar question {form.cleaned_data.get("question",None)}') 
            activity.save()
            return JsonResponse({'valid':True,'message':'Question added successfully.'},content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors},content_type='application/json')

#EditQuestion
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')
class EditQuestion(View):
    def get(self ,request,id):
        obj=check_data()
        user = get_object_or_404(GrammarModel,id=id)
        form=GrammarForm(instance=user)
        form2=AnswerForm(instance=user)
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        data={
            'title':f'Edit item:{user.question}',
            'obj':obj,
            'data':request.user,
            'form':form,
            'form2':form2,
            'admin':user,
            'activities':activities,
            'question_id':id,
            'prev':'Questions',
            'edit':True,
            'room_name':request.user.username,
            'contact_messages':contact_messages,
            'contact_count':contact_count,  
        }
        return render(request,'panel/add_question.html',context=data)

    def post(self,request,id,*args ,**kwargs):
        user=get_object_or_404(GrammarModel,id=id)
        form=GrammarForm(request.POST or None,instance=user)
        if form.is_valid():
            form.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-envelope"></i>',title='Grammer ',user_id=request.user.pk,name=f'Edited grammar question {form.cleaned_data.get("question",None)}') 
            activity.save()
            return JsonResponse({'valid':True,'message':'Question updated successfuly.'},content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors,},content_type='application/json')

#deleteQuestion
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def deleteQuestion(request,id):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        try:
            obj=GrammarModel.objects.get(id__exact=id)
            obj.delete() 
            activity=ActivityModel.objects.create(icon='<i class="fa fa-envelope"></i>',title='Grammar',user_id=request.user.pk,name=f'Deleted grammar question {obj.question}') 
            activity.save()
            return JsonResponse({'deleted':False,'message':'Question deleted successfully.','id':id},content_type='application/json')       
        except GrammarModel.DoesNotExist:
            return JsonResponse({'deleted':True,'message':'Item does not exist'},content_type='application/json')

#answers
@login_required(login_url='accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['POST',])
def answers(request,id):
    if request.method == 'POST' and request.headers.get('x-requested-with') == 'XMLHttpRequest':
        form=AnswerForm(request.POST or None)
        if form.is_valid():
            if GrammarModel.objects.filter(id__exact=id).exists():
                user=GrammarModel.objects.get(id__exact=id)
                form=AnswerForm(request.POST or None,instance=user)
                form.save()
                activity=ActivityModel.objects.create(icon='<i class="fa fa-envelope"></i>',title='Grammar', user_id=request.user.pk,name=f'Added answer to a grammar question {user.question}') 
                activity.save()
                return JsonResponse({'valid':True,'message':'Answers updated successfuly.'},content_type='application/json')
            else:
                user=form.save(commit=False)
                user.question_id=id
                user.save()
                activity=ActivityModel.objects.create(icon='<i class="fa fa-envelope"></i>',title='Grammar',user_id=request.user.pk,name=f'updated answer to a grammar question {user.question}') 
                activity.save()
                return JsonResponse({'valid':True,'message':'Answers added successfuly.'},content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors,},content_type='application/json')

#admins
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def admins(request):
    obj=check_data()
    data=User.objects.filter(is_staff=True,is_superuser=False).order_by("-id")
    paginator=Paginator(data,30)
    page_num=request.GET.get('page')
    admins=paginator.get_page(page_num) 
    activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
    contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
    contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
    data={
        'title':'Manage Admins',
        'obj':obj,
        'data':request.user,
        'admins':admins,
        'activities':activities,
        'wcount':paginator.count,
        'room_name':request.user.username,
        'contact_messages':contact_messages,
        'contact_count':contact_count,  

    }
    return render(request,'panel/admins.html',context=data)

#users
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def users(request):
    obj=check_data()
    if request.GET.get('writer'):
        search=request.GET.get('writer')
        data=searchUser(search)
    else:
        data=User.objects.filter(extendedauthuser__category__icontains='writer').order_by("-id")
    if data:
        paginator=Paginator(data,30)
        page_num=request.GET.get('page')
        writers=paginator.get_page(page_num) 
        wcount=paginator.count
    else:
        writers=''
        wcount=''

    if request.GET.get('customer'):
        search=request.GET.get('customer')
        data1=searchUser(search)
    else:  
        data1=User.objects.filter(extendedauthuser__category__icontains='customer').order_by("-id") 

    if data1:
        paginator1=Paginator(data1,30)
        page_num1=request.GET.get('page')
        customers=paginator1.get_page(page_num1)
        ccount=paginator1.count
    else:
        customers=''
        ccount=''

    activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
    contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
    contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
    data={
        'title':'Manage Users',
        'obj':obj,
        'data':request.user,
        'customers':customers,
        'writers':writers,
        'activities':activities,
        'users':users,
        'wcount':wcount,
        'ccount':ccount,
        'room_name':request.user.username,
        'contact_messages':contact_messages,
        'contact_count':contact_count,  

    }
    return render(request,'panel/users.html',context=data)


#deleteUser
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def deleteUser(request,id):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        try:
            obj=User.objects.get(id__exact=id)
            activity=ActivityModel.objects.create(icon='<i class="fa fa-user"></i>',title='User update',user_id=request.user.pk,name=f'Deleted user {obj.get_full_name()}') 
            activity.save()
            obj.delete()
            return JsonResponse({'deleted':False,'message':'User deleted successfully.','id':id},content_type='application/json')       
        except User.DoesNotExist:
            return JsonResponse({'deleted':True,'message':'Item does not exist'},content_type='application/json')



#account_activities
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def account_activities(request):
    obj=check_data()
    data=ActivityModel.objects.all().order_by("-id")
    paginator=Paginator(data,30)
    page_num=request.GET.get('page')
    activities=paginator.get_page(page_num) 

    data1=UpdatesModel.objects.all().order_by("-id")
    paginator1=Paginator(data1,30)
    page_num1=request.GET.get('page')
    updates=paginator1.get_page(page_num1) 

    data={
        'title':'Manage Account Activities',
        'obj':obj,
        'data':request.user,
        'activities':activities,
        'users':users,
        'wcount':paginator.count,
        'room_name':request.user.username,
        'updates':updates,
        'ucount':paginator1.count,

    }
    return render(request,'panel/activities.html',context=data)

#AdminDetails
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')
class AdminDetails(View):
    def get(self ,request,id):
        obj=check_data()
        user=get_object_or_404(User,id=id)
        form=UserPasswordCreateForm(instance=user)
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        data={
            'title':f'Admin details / {user.get_full_name()}',
            'obj':obj,
            'data':request.user,
            'activities':activities,
            'user':user,
            'form':form,
            'id':id,
            'edit':True,
            'room_name':request.user.username,
            'contact_messages':contact_messages,
            'contact_count':contact_count,  
    
        }
        return render(request,'panel/admin_details.html',context=data)

    def post(self,request,id,*args ,**kwargs):
        user=User.objects.get(id=id)
        form=UserPasswordCreateForm(request.POST or None,instance=user)
        if form.is_valid():
            user=form.save(commit=False)
            user.password=make_password(form.cleaned_data.get('password1',None))
            user.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-lock"></i>',title='Password update',user_id=request.user.pk,name='Changed password successfuly') 
            activity.save()
            return JsonResponse({'valid':True,'message':'Password changed successfuly.'},content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors,},content_type='application/json')

#UserDetails
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')
class UserDetails(View):
    def get(self ,request,id):
        obj=check_data()
        user=get_object_or_404(User,id=id)
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        form=UserPasswordCreateForm(instance=user)
        if user.extendedauthuser.category == 'writer':
            data=BidModel.objects.select_related('parent','user').filter(user_id__exact=user.pk).order_by("-id")
            paginator=Paginator(data,30)
            page_num=request.GET.get('page')
            user_orders=paginator.get_page(page_num)

            data1=WriterTransactionModel.objects.select_related('user').filter(user_id=id).order_by("-id")
            paginator1=Paginator(data1,30)
            page_num1=request.GET.get('page')
            transactions=paginator1.get_page(page_num1)
            correct_answers=AnswerModel.objects.filter(status=True).count()
        else:
            data=OrderModel.objects.select_related('user').filter(user_id__exact=user.pk).order_by("-id")
            paginator=Paginator(data,30)
            page_num=request.GET.get('page')
            user_orders=paginator.get_page(page_num)
            correct_answers=''
            data1=TransactionModel.objects.select_related('user').filter(user_id=id).order_by("-id")
            paginator1=Paginator(data1,30)
            page_num1=request.GET.get('page')
            transactions=paginator1.get_page(page_num1)

        data={
            'title':f'User details / {user.username}',
            'obj':obj,
            'data':request.user,
            'activities':activities,
            'user_orders':user_orders,
            'transactions':transactions,
            'transactions_count':paginator1.count,
            'form':form,
            'user':user,
            'id':id,
            'mcount':paginator.count,
            'edit':True,
            'room_name':request.user.username,
            'contact_messages':contact_messages,
            'contact_count':contact_count,  
            'correct_answers':correct_answers,
    
        }
        return render(request,'panel/user_details.html',context=data)

    def post(self,request,id,*args ,**kwargs):
        user=User.objects.get(id=id)
        form=UserPasswordCreateForm(request.POST or None,instance=user)
        if form.is_valid():
            user=form.save(commit=False)
            user.password=make_password(form.cleaned_data.get('password1',None))
            user.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-lock"></i>',title='Password update',user_id=request.user.pk,name='Changed password successfuly') 
            activity.save()
            return JsonResponse({'valid':True,'message':'Password changed successfuly.'},content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors,},content_type='application/json')


#passform
login_required(login_url='/accounts/login')
@api_view(['POST',])
def passform(request,id,*args ,**kwargs):
    if request.method == 'POST' and request.headers.get('x-requested-with') == 'XMLHttpRequest':
        user=User.objects.get(id=id)
        form=UserPasswordCreateForm(request.POST or None,instance=user)
        if form.is_valid():
            user=form.save(commit=False)
            user.password=make_password(form.cleaned_data.get('password1',None))
            user.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-lock"></i>',title='Password update',user_id=request.user.pk,name='Changed password successfuly') 
            activity.save()
            return JsonResponse({'valid':True,'message':'Password changed successfuly.'},content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors,},content_type='application/json')



#manageFaqs
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def manageFaqs(request):
    obj=check_data()
    data=FaqModel.objects.all().order_by("-id")
    paginator=Paginator(data,30)
    page_num=request.GET.get('page')
    activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
    contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
    contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
    faqs=paginator.get_page(page_num)
    data={
        'title':'Manage FAQS',
        'obj':obj,
        'data':request.user,
        'faqs':faqs,
        'activities':activities,
        'faqs_count':paginator.count,
        'edit':True,
        'room_name':request.user.username,
        'contact_messages':contact_messages,
        'contact_count':contact_count,  

    }
    return render(request,'panel/faqs.html',context=data)


#AddFaq
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')

class AddFaq(View):
    def get(self,request):
        obj=check_data()
        form=FaqForm()
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        data={
            'title':'Add FAQ',
            'obj':obj,
            'data':request.user,
            'form':form,
            'prev':'FAQs',
            'activities':activities,
            'room_name':request.user.username,
            'contact_messages':contact_messages,
            'contact_count':contact_count,  
    
        }
        return render(request,'panel/add_faq.html',context=data)
    def post(self,request):
        form=FaqForm(request.POST or None)
        if form.is_valid():
            usr=form.save(commit=False)
            usr.user_id=request.user.pk
            usr.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-cog"></i>',title='Faq update',user_id=request.user.pk,name=f'Added FAQ on category {form.cleaned_data.get("category",None)}') 
            activity.save()
            return JsonResponse({'valid':True,'message':'FAQ added successfully.'},content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors},content_type='application/json')

#EditFaq
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')

class EditFaq(View):
    def get(self ,request,id):
        obj=check_data()
        user = get_object_or_404(FaqModel,id=id)
        form=FaqForm(instance=user)
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        data={
            'title':f'Edit item:{user.text}',
            'obj':obj,
            'data':request.user,
            'form':form,
            'activities':activities,
            'prev':'FAQs',
            'edit':True,
            'room_name':request.user.username,
            'contact_messages':contact_messages,
            'contact_count':contact_count,  
    
        }
        return render(request,'panel/add_faq.html',context=data)

    def post(self,request,id,*args ,**kwargs):
        user=FaqModel.objects.get(id=id)
        form=FaqForm(request.POST or None,instance=user)
        if form.is_valid():
            form.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-cog"></i>',title='Faq update',user_id=request.user.pk,name=f'Edited FAQ on category {form.cleaned_data.get("category",None)}') 
            activity.save()
            return JsonResponse({'valid':True,'message':'FAQ updated successfuly.'},content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors,},content_type='application/json')



#deleteUpdate
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def deleteUpdate(request,id):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        try:
            obj=UpdatesModel.objects.get(id__exact=id)
            obj.delete() 
            return JsonResponse({'deleted':False,'message':'Update deleted successfully.','id':id},content_type='application/json')       
        except FaqModel.DoesNotExist:
            return JsonResponse({'deleted':True,'message':'Item does not exist'},content_type='application/json')

#deleteActivity
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def deleteActivity(request,id):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        try:
            obj=ActivityModel.objects.get(id__exact=id)
            obj.delete() 
            return JsonResponse({'deleted':False,'message':'Activity deleted successfully.','id':id},content_type='application/json')       
        except FaqModel.DoesNotExist:
            return JsonResponse({'deleted':True,'message':'Item does not exist'},content_type='application/json')



#deleteAllUpdates
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def deleteAllUpdates(request):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        try:
            obj=UpdatesModel.objects.all()
            obj.delete() 
            return JsonResponse({'deleted':False,'message':'All updates deleted successfully.'},content_type='application/json')       
        except FaqModel.DoesNotExist:
            return JsonResponse({'deleted':True,'message':'Item does not exist'},content_type='application/json')


#deleteAllActivity
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def deleteAllActivity(request):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        try:
            obj=ActivityModel.objects.all()
            obj.delete() 
            return JsonResponse({'deleted':False,'message':'All activities deleted successfully.'},content_type='application/json')       
        except FaqModel.DoesNotExist:
            return JsonResponse({'deleted':True,'message':'Item does not exist'},content_type='application/json')

#deleteFaq
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def deleteFaq(request,id):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        try:
            obj=FaqModel.objects.get(id__exact=id)
            obj.delete() 
            activity=ActivityModel.objects.create(icon='<i class="fa fa-cog"></i>',title='Faq update',user_id=request.user.pk,name=f'Deleted FAQ on category {obj.category}') 
            activity.save()
            return JsonResponse({'deleted':False,'message':'FAQ deleted successfully.','id':id},content_type='application/json')       
        except FaqModel.DoesNotExist:
            return JsonResponse({'deleted':True,'message':'Item does not exist'},content_type='application/json')

#howitworks
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def howitworks(request):
    obj=check_data()
    data=HowItWorksModel.objects.all().order_by("-id")
    paginator=Paginator(data,30)
    page_num=request.GET.get('page')
    works=paginator.get_page(page_num)
    activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
    contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
    contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
    data={
        'title':'Manage How it works page',
        'obj':obj,
        'data':request.user,
        'works':works,
        'activities':activities,
        'count':paginator.count,
        'edit':True,
        'room_name':request.user.username,
        'contact_messages':contact_messages,
        'contact_count':contact_count,  

    }
    return render(request,'panel/howitworks.html',context=data)

#AddWork
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')
class AddWork(View):
    def get(self,request):
        obj=check_data()
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        form=HowItWorksForm()
        data={
            'title':'Add Item',
            'obj':obj,
            'data':request.user,
            'form':form,
            'activities':activities,
            'prev':'How it works items',
            'room_name':request.user.username,
            'contact_messages':contact_messages,
            'contact_count':contact_count,  
    
        }
        return render(request,'panel/add_work.html',context=data)
    def post(self,request):
        form=HowItWorksForm(request.POST,request.FILES  or None)
        if form.is_valid():
            usr=form.save(commit=False)
            usr.user_id=request.user.pk
            usr.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-cog"></i>',title='Work update',user_id=request.user.pk,name='Made some changes How It Works Page') 
            activity.save()
            return JsonResponse({'valid':True,'message':'Item added successfully.'},content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors},content_type='application/json')

#EditWork
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')

class EditWork(View):
    def get(self ,request,id):
        obj=check_data()
        user = get_object_or_404(HowItWorksModel,id=id)
        form=HowItWorksForm(instance=user)
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        data={
            'title':f'Edit item:{user.text}',
            'obj':obj,
            'data':request.user,
            'form':form,
            'activities':activities,
            'prev':'How it works items',
            'edit':True,
            'room_name':request.user.username,
            'contact_messages':contact_messages,
            'contact_count':contact_count,  
    
        }
        return render(request,'panel/add_work.html',context=data)

    def post(self,request,id,*args ,**kwargs):
        user=get_object_or_404(HowItWorksModel,id=id)
        form=HowItWorksForm(request.POST,request.FILES or None,instance=user)
        if form.is_valid():
            form.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-cog"></i>',title='Work update',user_id=request.user.pk,name='Edited some changes How It Works Page') 
            activity.save()
            return JsonResponse({'valid':True,'message':'Item updated successfuly.'},content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors,},content_type='application/json')

#deleteWork
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def deleteWork(request,id):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        try:
            obj=HowItWorksModel.objects.get(id__exact=id)
            obj.delete()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-cog"></i>',title='Work update',user_id=request.user.pk,name='Edited some changes How It Works Page') 
            activity.save()
            return JsonResponse({'deleted':False,'message':'Item deleted successfully.','id':id},content_type='application/json')       
        except HowItWorksModel.DoesNotExist:
            return JsonResponse({'deleted':True,'message':'Item does not exist'},content_type='application/json')

#infotips
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def infotips(request):
    obj=check_data()
    data=InfotipsModel.objects.all().order_by("-id")
    paginator=Paginator(data,30)
    activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
    contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
    contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
    page_num=request.GET.get('page')
    infotips=paginator.get_page(page_num)
    data={
        'title':'Manage infotips',
        'obj':obj,
        'data':request.user,
        'infotips':infotips,
        'activities':activities,
        'count':paginator.count,
        'edit':True,
        'room_name':request.user.username,
        'contact_messages':contact_messages,
            'contact_count':contact_count,  
    }
    return render(request,'panel/infotips.html',context=data)

#AddInfotip
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')
class AddInfotip(View):
    def get(self,request):
        obj=check_data()
        form=InfotipsForm()
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        data={
            'title':'Add Infotip',
            'obj':obj,
            'data':request.user,
            'form':form,
            'activities':activities,
            'prev':'Manage Infotips',
            'room_name':request.user.username,
            'contact_messages':contact_messages,
            'contact_count':contact_count,  
    
        }
        return render(request,'panel/add_infotip.html',context=data)
    def post(self,request):
        form=InfotipsForm(request.POST,request.FILES  or None)
        if form.is_valid():
            usr=form.save(commit=False)
            usr.user_id=request.user.pk
            usr.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-cog"></i>',title='Infotip',user_id=request.user.pk,name='Added Infotip ') 
            activity.save()
            return JsonResponse({'valid':True,'message':'Infotip added successfully.'},content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors},content_type='application/json')

#EditInfotip
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')
class EditInfotip(View):
    def get(self ,request,id):
        obj=check_data()
        user = get_object_or_404(InfotipsModel,id=id)
        form=InfotipsForm(instance=user)
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        data={
            'title':f'Edit item:{user.service_rate_tag}',
            'obj':obj,
            'data':request.user,
            'form':form,
            'activities':activities,
            'prev':'Manage Infotips',
            'edit':True,
            'room_name':request.user.username,
            'contact_messages':contact_messages,
            'contact_count':contact_count,  
    
        }
        return render(request,'panel/add_infotip.html',context=data)

    def post(self,request,id,*args ,**kwargs):
        user=get_object_or_404(InfotipsModel,id=id)
        form=InfotipsForm(request.POST,request.FILES or None,instance=user)
        if form.is_valid():
            form.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-cog"></i>',title='Infotip update',user_id=request.user.pk,name='Edited infotip') 
            activity.save()
            return JsonResponse({'valid':True,'message':'Infotip updated successfuly.'},content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors,},content_type='application/json')

#deleteInfotip
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def deleteInfotip(request,id):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        try:
            obj=InfotipsModel.objects.get(id__exact=id)
            obj.delete() 
            activity=ActivityModel.objects.create(icon='<i class="fa fa-cog"></i>',title='Infotip update',user_id=request.user.pk,name='Deleted Infotip') 
            activity.save()
            return JsonResponse({'deleted':False,'message':'Infotip deleted successfully.','id':id},content_type='application/json')       
        except InfotipsModel.DoesNotExist:
            return JsonResponse({'deleted':True,'message':'Item does not exist'},content_type='application/json')


#cms
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')
class Cms(View):
    def get(self ,request):
        obj=check_data()
        data=InfotipsModel.objects.all().order_by("-id")
        paginator=Paginator(data,30)
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        if CmsModel.objects.count() > 0:
            form=CmsForm(instance=CmsModel.objects.all().last())
            form1=WitersForm(instance=CmsModel.objects.all().last())
            form2=MoneyForm(instance=CmsModel.objects.all().last())
            form3=PrivacyForm(instance=CmsModel.objects.all().last())
            form4=StudirreForm(instance=CmsModel.objects.all().last())
            form5=ConfedialityForm(instance=CmsModel.objects.all().last())
            form6=WebmasterForm(instance=CmsModel.objects.all().last())
            form7=WritingForm(instance=CmsModel.objects.all().last())
            form8=ResearchForm(instance=CmsModel.objects.all().last())
            form9=ThesisForm(instance=CmsModel.objects.all().last())
            form10=DissertationForm(instance=CmsModel.objects.all().last())
            form11=PersonalForm(instance=CmsModel.objects.all().last())
            form12=ResourcesForm(instance=CmsModel.objects.all().last())
            form13=CMSSampleForm(instance=CmsModel.objects.all().last())
            form14=OneHourForm(instance=CmsModel.objects.all().last())
        else:
            form=CmsForm()
            form1=WitersForm()
            form2=MoneyForm()
            form3=PrivacyForm()
            form4=StudirreForm()
            form5=ConfedialityForm()
            form6=WebmasterForm()
            form7=WritingForm()
            form8=ResearchForm()
            form9=ThesisForm()
            form10=DissertationForm()
            form11=PersonalForm()
            form12=ResourcesForm()
            form13=CMSSampleForm()
            form14=OneHourForm()
        page_num=request.GET.get('page')
        infotips=paginator.get_page(page_num)
        data={
            'title':'Manage CMS',
            'obj':obj,
            'data':request.user,
            'infotips':infotips,
            'form':form,
            'form1':form1,
            'form2':form2,
            'form3':form3,
            'form4':form4,
            'activities':activities,
            'form5':form5,
            'form6':form6,
            'form7':form7,
            'form8':form8,
            'form9':form9,
            'form10':form10,
            'form11':form11,
            'form12':form12,
            'form13':form13,
            'form14':form14,
            'count':paginator.count,
            'edit':True,
            'room_name':request.user.username,
            'contact_messages':contact_messages,
            'contact_count':contact_count,  
    
        }
        return render(request,'panel/cms.html',context=data)
    def post(self,request,*args ,**kwargs):
        user=CmsModel.objects.get(user_id=request.user.pk)
        form=CmsForm(request.POST,request.FILES or None,instance=user)
        if form.is_valid():
            form.save()
            return JsonResponse({'valid':True,'message':'Data updated successfuly.'},content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors,},content_type='application/json')

#app_writers
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['POST',])
def app_writers(request):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest' and request.method == 'POST':
        user=CmsModel.objects.get(user_id=request.user.pk)
        form=WitersForm(request.POST,request.FILES or None,instance=user)
        if form.is_valid():
            form.save()
            return JsonResponse({'valid':True,'message':'Content submitted successfully'},content_type='application/json')
        return JsonResponse({'valid':False,'uform_errors':form.errors},content_type='application/json')

#money
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['POST',])
def money(request):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest' and request.method == 'POST':
        user=CmsModel.objects.get(user_id=request.user.pk)
        form=MoneyForm(request.POST,request.FILES or None,instance=user)
        if form.is_valid():
            form.save()
            return JsonResponse({'valid':True,'message':'Content submitted successfully'},content_type='application/json')
        return JsonResponse({'valid':False,'uform_errors':form.errors},content_type='application/json')

#privacy
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['POST',])
def privacy(request):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest' and request.method == 'POST':
        user=CmsModel.objects.get(user_id=request.user.pk)
        form=PrivacyForm(request.POST,request.FILES or None,instance=user)
        if form.is_valid():
            form.save()
            return JsonResponse({'valid':True,'message':'Content submitted successfully'},content_type='application/json')
        return JsonResponse({'valid':False,'uform_errors':form.errors},content_type='application/json')

#help
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['POST',])
def help(request):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest' and request.method == 'POST':
        user=CmsModel.objects.get(user_id=request.user.pk)
        form=StudirreForm(request.POST,request.FILES or None,instance=user)
        if form.is_valid():
            form.save()
            return JsonResponse({'valid':True,'message':'Content submitted successfully'},content_type='application/json')
        return JsonResponse({'valid':False,'uform_errors':form.errors},content_type='application/json')

#confediality
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['POST',])
def confediality(request):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest' and request.method == 'POST':
        user=CmsModel.objects.get(user_id=request.user.pk)
        form=ConfedialityForm(request.POST,request.FILES or None,instance=user)
        if form.is_valid():
            form.save()
            return JsonResponse({'valid':True,'message':'Content submitted successfully'},content_type='application/json')
        return JsonResponse({'valid':False,'uform_errors':form.errors},content_type='application/json')

#webmaster
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['POST',])
def webmaster(request):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest' and request.method == 'POST':
        user=CmsModel.objects.get(user_id=request.user.pk)
        form=WebmasterForm(request.POST,request.FILES or None,instance=user)
        if form.is_valid():
            form.save()
            return JsonResponse({'valid':True,'message':'Content submitted successfully'},content_type='application/json')
        return JsonResponse({'valid':False,'uform_errors':form.errors},content_type='application/json')

#paper
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['POST',])
def cmsPaper(request):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest' and request.method == 'POST':
        user=CmsModel.objects.get(user_id=request.user.pk)
        form=WritingForm(request.POST,request.FILES or None,instance=user)
        if form.is_valid():
            form.save()
            return JsonResponse({'valid':True,'message':'Content submitted successfully'},content_type='application/json')
        return JsonResponse({'valid':False,'uform_errors':form.errors},content_type='application/json')

#research
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['POST',])
def research(request):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest' and request.method == 'POST':
        user=CmsModel.objects.get(user_id=request.user.pk)
        form=ResearchForm(request.POST,request.FILES or None,instance=user)
        if form.is_valid():
            form.save()
            return JsonResponse({'valid':True,'message':'Content submitted successfully'},content_type='application/json')
        return JsonResponse({'valid':False,'uform_errors':form.errors},content_type='application/json')

#thesis
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['POST',])
def thesis(request):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest' and request.method == 'POST':
        user=CmsModel.objects.get(user_id=request.user.pk)
        form=ThesisForm(request.POST,request.FILES or None,instance=user)
        if form.is_valid():
            form.save()
            return JsonResponse({'valid':True,'message':'Content submitted successfully'},content_type='application/json')
        return JsonResponse({'valid':False,'uform_errors':form.errors},content_type='application/json')

#dissertation
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['POST',])
def dissertation(request):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest' and request.method == 'POST':
        user=CmsModel.objects.get(user_id=request.user.pk)
        form=DissertationForm(request.POST,request.FILES or None,instance=user)
        if form.is_valid():
            form.save()
            return JsonResponse({'valid':True,'message':'Content submitted successfully'},content_type='application/json')
        return JsonResponse({'valid':False,'uform_errors':form.errors},content_type='application/json')

#personal
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['POST',])
def personal(request):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest' and request.method == 'POST':
        user=CmsModel.objects.get(user_id=request.user.pk)
        form=PersonalForm(request.POST,request.FILES or None,instance=user)
        if form.is_valid():
            form.save()
            return JsonResponse({'valid':True,'message':'Content submitted successfully'},content_type='application/json')
        return JsonResponse({'valid':False,'uform_errors':form.errors},content_type='application/json')

#resources
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['POST',])
def resources(request):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest' and request.method == 'POST':
        user=CmsModel.objects.get(user_id=request.user.pk)
        form=ResourcesForm(request.POST,request.FILES or None,instance=user)
        if form.is_valid():
            form.save()
            return JsonResponse({'valid':True,'message':'Content submitted successfully'},content_type='application/json')
        return JsonResponse({'valid':False,'uform_errors':form.errors},content_type='application/json')

#samples
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['POST',])
def CMSsamples(request):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest' and request.method == 'POST':
        user=CmsModel.objects.get(user_id=request.user.pk)
        form=CMSSampleForm(request.POST,request.FILES or None,instance=user)
        if form.is_valid():
            form.save()
            return JsonResponse({'valid':True,'message':'Content submitted successfully'},content_type='application/json')
        return JsonResponse({'valid':False,'uform_errors':form.errors},content_type='application/json')

#one
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['POST',])
def one(request):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest' and request.method == 'POST':
        user=CmsModel.objects.get(user_id=request.user.pk)
        form=OneHourForm(request.POST,request.FILES or None,instance=user)
        if form.is_valid():
            form.save()
            return JsonResponse({'valid':True,'message':'Content submitted successfully'},content_type='application/json')
        return JsonResponse({'valid':False,'uform_errors':form.errors},content_type='application/json')



#newOrder
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins','customer']),name='dispatch')
class newOrder(View):
    def get(self,request):
        obj=check_data()
        papers=PaperModel.objects.all().order_by('-id')
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        styles=CitationModel.objects.all().order_by('-id')
        writers=User.objects.filter(extendedauthuser__category__icontains='writer')
        messages=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).order_by("-id")
        messages_count=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).count()
        inner_qs=IndividualModel.objects.values_list('parent_id',flat=True).distinct()
        qs2=BidModel.objects.filter(user_id=request.user.pk).values_list('user_id',flat=True)
        if inner_qs:
            count1=BroadcastModel.objects.filter(group=True).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
            count2=BroadcastModel.objects.filter(to__icontains=request.user.username).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
        else:
            count1=BroadcastModel.objects.filter(group=True).count()
            count2=BroadcastModel.objects.filter(to__icontains=request.user.username).count()
        broadcasts_count=int(count1+count2)
        form=OrderForm()
        form1=ExamForm()
        data={
            'title':'Place New Order',
            'obj':obj,
            'data':request.user,
            'form':form,
            'form1':form1,
            'prev':'My Orders',
            'activities':activities,
            'papers':papers,
            'writers':writers,
            'styles':styles,
            'room_name':request.user.username,
            'messages':messages,
            'broadcasts_count':broadcasts_count,
            'messages_count':messages_count,
            'contact_messages':contact_messages,
            'contact_count':contact_count,  
    
        }
        return render(request,'panel/new_order.html',context=data)
    def post(self,request):
        form=OrderForm(request.POST,request.FILES  or None)
        obj=check_data()
        if form.is_valid():
            inviter=form.cleaned_data.get('inviter',None)
            files=request.FILES.getlist('additional_materials')
            usr=form.save(commit=False)
            usr.user_id=request.user.pk
            usr.order_id=generate_id()
            send_noti=False
            if inviter:
                user=User.objects.get(username__exact=inviter)
                usr.inviter_id=user.id
                usr.is_public=False
                send_noti=True
            usr.save()
            if send_noti:
                email=user.email
                subject=f'Order #{usr.order_id} invitation request from {request.user.get_full_name()}'
                message={
                        'user':request.user,
                        'target':user,
                        'site_name':obj.site_name,
                        'site_url':obj.site_url,
                        'address':obj.address,
                        'location':obj.location,
                        'description':obj.description,
                        'phone':obj.phone,
                        'order_id':usr.order_id,
                        'id':usr.id
                    }
                template='emails/inviter.html'
                send_email(subject,email,message,template)

            subject=f'New order #{usr.order_id} just posted!'
            admin_email=obj.site_email
            message={
                        'site_name':obj.site_name,
                        'site_url':obj.site_url,
                        'address':obj.address,
                        'location':obj.location,
                        'description':obj.description,
                        'phone':obj.phone,
                        'order':usr,
                        'user':obj.user,
                        'obj':obj,
                     } 
            template='emails/new_order.html'
            send_email(subject,admin_email,message,template)
            for file in files:
               OrderMediaModel.objects.create(user_id=request.user.pk,parent_id=usr.id,additional_materials=file)
            activity=ActivityModel.objects.create(icon='<i class="fa fa-shopping-cart"></i>',title='Order update',user_id=request.user.pk,name=f'Added new order on topic {form.cleaned_data.get("topic",None)}') 
            activity.save()
            updates=UpdatesModel.objects.create(user_id=request.user.pk,name=f'Added new order on topic {form.cleaned_data.get("topic",None)}')
            updates.save()
            return JsonResponse({'valid':True,'message':'Order submitted successfully.'},content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors},content_type='application/json')



#examOrder
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins','customer'])
@api_view(['POST',])
def examOrder(request):
    if request.method == 'POST' and request.headers.get('x-requested-with') == 'XMLHttpRequest':
        form=ExamForm(request.POST,request.FILES  or None)
        if form.is_valid():
            files=request.FILES.getlist('additional_materials')
            usr=form.save(commit=False)
            usr.user_id=request.user.pk
            usr.order_id=generate_serial()
            usr.save()
            for file in files:
               ExamMediaModel.objects.create(user_id=request.user.pk,parent_id=usr.id,additional_materials=file)
            activity=ActivityModel.objects.create(icon='<i class="fa fa-shopping-cart"></i>',title='Order update',user_id=request.user.pk,name=f'Added new order on topic {form.cleaned_data.get("topic",None)}') 
            activity.save()
            updates=UpdatesModel.objects.create(user_id=request.user.pk,name=f'Added new order on topic {form.cleaned_data.get("topic",None)}')
            updates.save()
            return JsonResponse({'id':usr.id,'country':form.cleaned_data.get("country",None),'no_essay':form.cleaned_data.get("no_of_essay_quiz",None),'mcq':form.cleaned_data.get("no_of_mcq_quiz",None),'topic':form.cleaned_data.get("topic",None),'valid':True,'message':'Order submitted successfully.','whatsapp':True},content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors},content_type='application/json')

#editorexamOrder
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins','customer'])
@api_view(['POST',])
def editorexamOrder(request,id):
    if request.method == 'POST' and request.headers.get('x-requested-with') == 'XMLHttpRequest':
        usr=get_object_or_404(ExamModel,id__exact=id)
        form=ExamForm(request.POST,request.FILES  or None,instance=usr)
        if form.is_valid():
            files=request.FILES.getlist('additional_materials')
            form.save()
            for file in files:
               ExamMediaModel.objects.create(user_id=request.user.pk,parent_id=id,additional_materials=file)
            activity=ActivityModel.objects.create(icon='<i class="fa fa-shopping-cart"></i>',title='Order update',user_id=request.user.pk,name=f'Edited question order of topic:{form.cleaned_data.get("topic",None)}') 
            activity.save()
            updates=UpdatesModel.objects.create(user_id=request.user.pk,name=f'Edited question order on topic {form.cleaned_data.get("topic",None)}')
            updates.save()
            return JsonResponse({'id':id,'country':form.cleaned_data.get("country",None),'no_essay':form.cleaned_data.get("no_of_essay_quiz",None),'mcq':form.cleaned_data.get("no_of_mcq_quiz",None),'topic':form.cleaned_data.get("topic",None),'valid':True,'message':'Order submitted successfully.','whatsapp':True},content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors},content_type='application/json')


#orderPreview
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
class orderPreview(View):
    def get(self,request,id):
        obj=check_data()
        if BidModel.objects.select_related('parent').filter(parent_id__exact=id).exists():
            order=BidModel.objects.select_related('parent').filter(parent_id__exact=id).last()
        else:
            order=''
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        media=OrderMediaModel.objects.filter(parent_id__exact=id)
        answers=SubmittedAnswerModel.objects.filter(parent_id__exact=id)
        messages=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).order_by("-id")
        messages_count=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).count()
        inner_qs=IndividualModel.objects.values_list('parent_id',flat=True).distinct()
        qs2=BidModel.objects.filter(user_id=request.user.pk).values_list('user_id',flat=True)
        if inner_qs:
            count1=BroadcastModel.objects.filter(group=True).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
            count2=BroadcastModel.objects.filter(to__icontains=request.user.username).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
        else:
            count1=BroadcastModel.objects.filter(group=True).count()
            count2=BroadcastModel.objects.filter(to__icontains=request.user.username).count()
        broadcasts_count=int(count1+count2)
        data={
            'title':'Order Preview',
            'obj':obj,
            'data':request.user,
            'order':order,
            'activities':activities,
            'prev':'Orders Pending Approval',
            'room_name':request.user.username,
            'media':media,
            'answers':answers,
            'messages':messages,
            'broadcasts_count':broadcasts_count,
            'messages_count':messages_count,
            'contact_messages':contact_messages,
            'contact_count':contact_count,  
        }
        return render(request,'panel/order_preview.html',context=data)
    def post(self,request):
        form=InfotipsForm(request.POST,request.FILES  or None)
        if form.is_valid():
            usr=form.save(commit=False)
            usr.user_id=request.user.pk
            usr.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-cog"></i>',title='Infotip update',user_id=request.user.pk,name='Made changes in order preview page') 
            activity.save()
            return JsonResponse({'valid':True,'message':'Infotip added successfully.'},content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors},content_type='application/json')

#confirmOrder
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins','customer'])
@api_view(['GET',])
def confirmOrder(request,id):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        order=get_object_or_404(OrderModel,id__exact=id)
        order.status='Active'
        order.save()
        activity=ActivityModel.objects.create(icon='<i class="fa fa-shopping-cart"></i>',title='Order update',user_id=request.user.pk,name=f'Updated order #{order.order_id}') 
        activity.save()
        updates=UpdatesModel.objects.create(user_id=request.user.pk,name=f'Updated order #{order.order_id}')
        updates.save()
        return JsonResponse({'valid':True,'message':'Order updated successfully'},status=200,content_type='application/json')

#bids
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def bids(request,id):
    obj=SiteModel.objects.count()
    obj=check_data()
    order=get_object_or_404(OrderModel,id__exact=id)
    activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
    contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
    contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
    data_count=BidModel.objects.select_related('parent').filter(parent_id__exact=id).count()
    data=BidModel.objects.select_related('parent').filter(parent_id__exact=id)
    paginator=Paginator(data,30)
    page_num=request.GET.get('page')
    order_bids=paginator.get_page(page_num)
    messages=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).order_by("-id")
    messages_count=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).count()
    inner_qs=IndividualModel.objects.values_list('parent_id',flat=True).distinct()
    qs2=BidModel.objects.filter(user_id=request.user.pk).values_list('user_id',flat=True)
    if inner_qs:
        count1=BroadcastModel.objects.filter(group=True).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
        count2=BroadcastModel.objects.filter(to__icontains=request.user.username).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
    else:
        count1=BroadcastModel.objects.filter(group=True).count()
        count2=BroadcastModel.objects.filter(to__icontains=request.user.username).count()
    broadcasts_count=int(count1+count2)
    data={
        'title':f'Bids on #{order.order_id}',
        'obj':obj,
        'data':request.user,
        'id':id,
        'data_count':data_count,
        'order_bids':order_bids,
        'activities':activities,
        'room_name':request.user.username,
        'messages':messages,
        'broadcasts_count':broadcasts_count,
        'messages_count':messages_count,
        'contact_messages':contact_messages,
        'contact_count':contact_count,  

    }
    return render(request,'panel/bids.html',context=data)


#allOrders
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def allOrders(request):
    obj=check_data()
    activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
    contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
    contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
    if request.GET.get('search'):
        search=request.GET.get('search')
        data=OrderModel.objects.select_related('user').filter(order_id__icontains=search).order_by("-id")
    else:
        data=OrderModel.objects.select_related('user').all().order_by("-id")
    paginator=Paginator(data,30)
    page_num=request.GET.get('page')
    orders=paginator.get_page(page_num)

    data1=ExamModel.objects.select_related('user').all().order_by("-id")
    paginator1=Paginator(data1,30)
    page_num1=request.GET.get('page')
    exams=paginator1.get_page(page_num1)


    data2=OrderModel.objects.filter(is_completed=False,is_cancelled=True).order_by("-id")
    paginator2=Paginator(data2,30)
    page_num2=request.GET.get('page')
    cancelled_orders=paginator2.get_page(page_num2)

    data3=OrderModel.objects.filter(is_completed=False,is_cancelled=False,inviter__isnull=False,is_public=False).order_by("-id")
    paginator3=Paginator(data3,30)
    page_num3=request.GET.get('page')
    invites=paginator3.get_page(page_num3)

    data4=OrderModel.objects.filter(user_id=request.user.pk,is_cancelled=False,is_accepted=True,is_completed=False,verdict__isnull=True).order_by("-id")
    paginator4=Paginator(data4,30)
    page_num4=request.GET.get('page')
    inprogress_orders=paginator4.get_page(page_num4)


    data5=OrderModel.objects.filter(is_completed=True,is_cancelled=False).order_by("-id")
    paginator5=Paginator(data5,30)
    page_num5=request.GET.get('page')
    completed_orders=paginator5.get_page(page_num5)


    data7=OrderModel.objects.filter(is_cancelled=False,is_accepted=True,is_completed=False,verdict__icontains='dispute').order_by("-id")
    paginator7=Paginator(data7,30)
    page_num7=request.GET.get('page')
    disputes=paginator7.get_page(page_num7) 

    data8=OrderModel.objects.filter(is_cancelled=False,is_accepted=True,is_completed=False,verdict__icontains='revision').order_by("-id")
    paginator8=Paginator(data8,30)
    page_num8=request.GET.get('page')
    revisions=paginator8.get_page(page_num8)
    data={
        'title':'View all Orders',
        'obj':obj,
        'data':request.user,
        'orders':orders,
        'activities':activities,
        'exams':exams,
        'room_name':request.user.username,
        'disputes':disputes,
        'revisions':revisions,
        'completed_orders':completed_orders,
        'inprogress_orders':inprogress_orders,
        'invites':invites,
        'cancelled_orders':cancelled_orders,
        'all_count':paginator.count,
        'ecount':paginator1.count,
        'ccount':paginator2.count,
        'icount':paginator3.count,
        'incount':paginator4.count,
        'compcount':paginator5.count,
        'dcount':paginator7.count,
        'rcount':paginator8.count,
        'contact_messages':contact_messages,
        'contact_count':contact_count,  

    }
    return render(request,'panel/all_orders.html',context=data)




#manageRatings
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def manageRatings(request):
    obj=SiteModel.objects.count()
    obj=check_data()
    data=OrderModel.objects.select_related('user').all().order_by("-id")
    paginator=Paginator(data,30)
    page_num=request.GET.get('page')
    customers=paginator.get_page(page_num)

    data1=BidModel.objects.select_related('user').all().order_by("-id")
    paginator1=Paginator(data1,30)
    page_num1=request.GET.get('page')
    writers=paginator1.get_page(page_num1)
    activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
    contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
    contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
    data={
        'title':'Ratings and Reviews',
        'obj':obj,
        'data':request.user,
        'customers':customers,
        'writers':writers,
        'activities':activities,
        'ccount':paginator.count,
        'wcount':paginator1.count,
        'room_name':request.user.username,
        'contact_messages':contact_messages,
        'contact_count':contact_count,  

    }
    return render(request,'panel/ratings.html',context=data)



#editCustomerRating
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')
class editCustomerRating(View):
    def get(self ,request,id):
        obj=check_data()
        user = get_object_or_404(OrderModel,id=id)
        form=FinalForm(instance=user)
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        data={
            'title':f'Edit customer rating / {user.user.get_full_name()}',
            'obj':obj,
            'data':request.user,
            'form':form,
            'user':user,
            'activities':activities,
            'edit':True,
            'room_name':request.user.username,
            'contact_messages':contact_messages,
            'contact_count':contact_count,  
    
        }
        return render(request,'panel/edit_rating.html',context=data)

    def post(self,request,id,*args ,**kwargs):
        user = get_object_or_404(OrderModel,id=id)
        uform=FinalForm(request.POST or None,instance=user)
        if uform.is_valid():
            uform.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-user"></i>',title='User rating',user_id=request.user.pk,name=f'Updated customer {user.user.get_full_name()} rating') 
            activity.save()
            return JsonResponse({'valid':True,'message':'Customer rating updated successfully'},content_type="application/json")
        else:
            return JsonResponse({'valid':False,'uform_errors':uform.errors},content_type="application/json")


#editWriterRating
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')
class editWriterRating(View):
    def get(self ,request,id):
        obj=check_data()
        user = get_object_or_404(BidModel,id=id)
        form=FinalForm(instance=user)
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        data={
            'title':f'Edit writer rating / {user.user.get_full_name()}',
            'obj':obj,
            'data':request.user,
            'form':form,
            'user':user,
            'activities':activities,
            'edit':True,
            'room_name':request.user.username,
            'contact_messages':contact_messages,
            'contact_count':contact_count,  
    
        }
        return render(request,'panel/edit_rating.html',context=data)

    def post(self,request,id,*args ,**kwargs):
        user = get_object_or_404(BidModel,id=id)
        uform=FinalForm(request.POST or None,instance=user)
        if uform.is_valid():
            uform.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-user"></i>',title='User rating',user_id=request.user.pk,name=f'Updated writer {user.user.get_full_name()} rating') 
            activity.save()
            return JsonResponse({'valid':True,'message':'Writer rating updated successfully'},content_type="application/json")
        else:
            return JsonResponse({'valid':False,'uform_errors':uform.errors},content_type="application/json")

#addAdmin
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')
class addAdmin(View):
    def get(self,request):
        obj=check_data()
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        form=UserReg()
        form2=CurrentAdminExtUserProfileChangeForm()        
        data={
            'title':'Add New Admin',
            'obj':obj,
            'data':request.user,
            'form':form,
            'form2':form2,
            'activities':activities,
            'room_name':request.user.username,
            'contact_messages':contact_messages,
            'contact_count':contact_count,  
    
        }
        return render(request,'panel/add_admin.html',context=data)
    def post(self,request):
        site=check_data()
        uform=UserReg(request.POST or None)
        eform=CurrentAdminExtUserProfileChangeForm(request.POST,request.FILES or None)
        if uform.is_valid() and  eform.is_valid():
            userme=uform.save(commit=False)
            password=(uform.cleaned_data.get('email',None).split('@')[0]).capitalize()+str(todays_date.year)
            userme.is_active = True
            username.password=make_password(password)
            userme.is_staff = True
            userme.save()
            if not Group.objects.filter(name='admins').exists():
                group=Group.objects.create(name='admins')
                group.user_set.add(userme)
                group.save()
            else:
                group=Group.objects.get(name__icontains='admins')
                group.user_set.add(userme)
                group.save()
            extended=eform.save(commit=False)
            extended.user=userme
            extended.category='Admin'
            extended.role='Admin'
            extended.initials=uform.cleaned_data.get('first_name')[0].upper()+uform.cleaned_data.get('last_name')[0].upper()
            extended.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-user"></i>',title='Admin created',user_id=request.user.pk,name=f'Created writer {uform.cleaned_data.get("first_name",None)} {uform.cleaned_data.get("last_name",None)} successfully') 
            activity.save()
            subject='Successfully added to the community as a staff member'
            email=uform.cleaned_data.get('email',None)
            message={
                        'user':userme,
                        'site_name':site.site_name,
                        'site_url':site.site_url,
                        'address':site.address,
                        'location':site.location,
                        'description':site.description,
                        'phone':site.phone,
                        'email':email,
                        'username':userme.username,
                        'password':password,
                        'category':'admin',
                     } 
            template='emails/new_user.html'
            send_email(subject,email,message,template)
            return JsonResponse({'valid':True,'message':'Admin created successfully'},content_type="application/json")
        else:
            return JsonResponse({'valid':False,'uform_errors':uform.errors,'eform_errors':eform.errors},content_type="application/json")

#addWriter
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')
class addWriter(View):
    def get(self,request):
        obj=check_data()
        contacts=ContactModel.objects.filter(is_read=False).order_by("-id")[:7]
        contacts_count=ContactModel.objects.filter(is_read=False).count()
        styles=CitationModel.objects.all().order_by("-id")
        languages=LanguageModel.objects.all().order_by("-id")
        degrees=AcademicModel.objects.all().order_by("-id")
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        form=UserReg()
        form2=ExtendedRegisterForm()        
        data={
            'title':'Add New Writer',
            'obj':obj,
            'data':request.user,
            'form':form,
            'form2':form2,
            'activities':activities,
            'styles':styles,
            'languages':languages,
            'degrees':degrees,
            'contacts_count':contacts_count,
            'contacts':contacts,
            'room_name':request.user.username,
            'contact_messages':contact_messages,
            'contact_count':contact_count,  
    
        }
        return render(request,'panel/add_users.html',context=data)
    def post(self,request):
        uform=UserReg(request.POST or None)
        site=check_data()
        eform=ExtendedRegisterForm(request.POST,request.FILES or None)
        if uform.is_valid() and  eform.is_valid():
            userme=uform.save(commit=False)
            password=(uform.cleaned_data.get('email',None).split('@')[0]).capitalize()+str(todays_date.year)
            userme.is_active = True
            userme.password = make_password(password)
            userme.save()
            if not Group.objects.filter(name='writer').exists():
                group=Group.objects.create(name='writer')
                group.user_set.add(userme)
                group.save()
            else:
                group=Group.objects.get(name__icontains='writer')
                group.user_set.add(userme)
                group.save()
            extended=eform.save(commit=False)
            extended.user=userme
            extended.citation_style=json.dumps(request.POST.getlist('citation_style'))
            extended.native_language=json.dumps(request.POST.getlist('native_language'))
            extended.academic_degree=json.dumps(request.POST.getlist('academic_degree'))
            extended.exam_complete=True
            extended.test_passed=True
            extended.category='writer'
            extended.is_verified=True
            extended.test_reason='Writer created by admin'
            extended.profile_reg=True
            extended.role='writer'
            extended.initials=uform.cleaned_data.get('first_name')[0].upper()+uform.cleaned_data.get('last_name')[0].upper()
            extended.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-user"></i>',title='Writer created',user_id=request.user.pk,name=f'Created writer {uform.cleaned_data.get("first_name",None)} {uform.cleaned_data.get("last_name",None)} successfully') 
            activity.save()
            subject='Successfully added to the community'
            email=uform.cleaned_data.get('email',None)
            message={
                        'user':userme,
                        'site_name':site.site_name,
                        'site_url':site.site_url,
                        'address':site.address,
                        'location':site.location,
                        'description':site.description,
                        'phone':site.phone,
                        'email':email,
                        'username':userme.username,
                        'password':password,
                        'category':'writer',
                     } 
            template='emails/new_user.html'
            send_email(subject,email,message,template)
            return JsonResponse({'valid':True,'message':'Writer created successfully'},content_type="application/json")
        else:
            return JsonResponse({'valid':False,'uform_errors':uform.errors,'eform_errors':eform.errors},content_type="application/json")

#editAdmin
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')
class editAdmin(View):
    def get(self ,request,id):
        obj=check_data()
        user = get_object_or_404(User,id=id)
        form=UserProfile(instance=user)
        form2=CurrentAdminExtUserProfileChangeForm(instance=user.extendedauthuser)   
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        data={
            'title':f'Edit admin / {user.get_full_name()}',
            'obj':obj,
            'data':request.user,
            'form':form,
            'user':user,
            'form2':form2,
            'activities':activities,
            'edit':True,
            'room_name':request.user.username,
            'contact_messages':contact_messages,
            'contact_count':contact_count,  
    
        }
        return render(request,'panel/add_admin.html',context=data)

    def post(self,request,id,*args ,**kwargs):
        user=get_object_or_404(User,id=id)
        uform=UserProfile(request.POST or None,instance=user)
        eform=CurrentAdminExtUserProfileChangeForm(request.POST,request.FILES or None,instance=user.extendedauthuser)
        if uform.is_valid() and  eform.is_valid():
            uform.save()
            eform.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-user"></i>',title='Admin update',user_id=request.user.pk,name=f'Updated writer {uform.cleaned_data.get("first_name",None)} {uform.cleaned_data.get("last_name",None)}') 
            activity.save()
            return JsonResponse({'valid':True,'message':'Admin updated successfully'},content_type="application/json")
        else:
            return JsonResponse({'valid':False,'uform_errors':uform.errors,'eform_errors':eform.errors},content_type="application/json")

#editWriter
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')
class editWriter(View):
    def get(self ,request,id):
        obj=check_data()
        user = get_object_or_404(User,id=id)
        form=UserProfile(instance=user)
        form2=ExtendedRegisterForm(instance=user.extendedauthuser)   
        messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        styles=CitationModel.objects.all().order_by("-id")
        languages=LanguageModel.objects.all().order_by("-id")
        degrees=AcademicModel.objects.all().order_by("-id")
        form3=UserPasswordCreateForm(instance=user)
        data={
            'title':f'Edit writer / {user.get_full_name()}',
            'obj':obj,
            'data':request.user,
            'form':form,
            'user':user,
            'form2':form2,
            'form3':form3,
            'activities':activities,
            'count':count,
            'prev':'Witers',
            'styles':styles,
            'languages':languages,
            'degrees':degrees,
            'messages':messages,
            'edit':True,
            'room_name':request.user.username,
            'contact_messages':contact_messages,
            'contact_count':contact_count,  
    
        }
        return render(request,'panel/add_users.html',context=data)

    def post(self,request,id,*args ,**kwargs):
        user=get_object_or_404(User,id=id)
        obj=check_data()
        uform=UserProfile(request.POST or None,instance=user)
        eform=ExtendedRegisterForm(request.POST,request.FILES or None,instance=user.extendedauthuser)
        if uform.is_valid() and  eform.is_valid():
            styles=json.dumps(request.POST.getlist('citation_style'))
            language=json.dumps(request.POST.getlist('native_language'))
            degree=json.dumps(request.POST.getlist('academic_degree'))
            userme=uform.save(commit=False)
            userme.is_active = True
            edata=eform.save(commit=False)
            edata.citation_style=styles
            edata.native_language=language
            edata.academic_degree=degree
            if eform.cleaned_data.get('is_deactivated',None):
                userme.is_active=False
                edata.is_deactivated=True
                subject='Account deactivated'
                email=uform.cleaned_data.get('email',None)
                message={
                        'user':userme,
                        'site_name':obj.site_name,
                        'site_url':obj.site_url,
                        'address':obj.address,
                        'location':obj.location,
                        'description':obj.description,
                        'phone':obj.phone
                    }
                template='emails/deactivated.html'
                send_email(subject,email,message,template)
            else:
                userme.is_active=True
                edata.is_deactivated=False 
            userme.save()
            edata.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-user"></i>',title='writer update',user_id=request.user.pk,name=f'Updated writer {uform.cleaned_data.get("first_name",None)} {uform.cleaned_data.get("last_name",None)}') 
            activity.save()
            return JsonResponse({'valid':True,'message':'Writer updated successfully'},content_type="application/json")
        else:
            return JsonResponse({'valid':False,'uform_errors':uform.errors,'eform_errors':eform.errors},content_type="application/json")


#addCustomer
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')
class addCustomer(View):
    def get(self,request):
        obj=check_data()
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:7]
        contacts_count=ContactModel.objects.filter(is_read=False).count()
        form=UserReg()
        form2=ExtendedCustomerRegisterForm() 
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        activity_count=ActivityModel.objects.filter(user_id=request.user.pk).count()       
        data={
            'title':'Add New Customer',
            'obj':obj,
            'data':request.user,
            'form':form,
            'activities':activities,
            'form2':form2,
            'room_name':request.user.username,
            'contact_messages':contact_messages,
            'contact_count':contacts_count,  
    
        }
        return render(request,'panel/add_customer.html',context=data)
    def post(self,request):
        site=check_data()
        uform=UserReg(request.POST or None)
        eform=ExtendedCustomerRegisterForm(request.POST,request.FILES or None)
        if uform.is_valid() and  eform.is_valid():
            userme=uform.save(commit=False)
            password=(uform.cleaned_data.get('email',None).split('@')[0]).capitalize()+str(todays_date.year)
            userme.is_active = True
            userme.password = make_password(password)
            userme.save()
            if not Group.objects.filter(name='customer').exists():
                group=Group.objects.create(name='customer')
                group.user_set.add(userme)
                group.save()
            else:
                group=Group.objects.get(name__icontains='customer')
                group.user_set.add(userme)
                group.save()
            extended=eform.save(commit=False)
            extended.user=userme
            extended.category='customer'
            extended.is_verified=True
            extended.profile_reg=True
            extended.role='customer'
            extended.initials=uform.cleaned_data.get('first_name')[0].upper()+uform.cleaned_data.get('last_name')[0].upper()
            extended.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-user"></i>',title='Customer created',user_id=request.user.pk,name=f'Created customer {uform.cleaned_data.get("first_name",None)} {uform.cleaned_data.get("last_name",None)} successfully') 
            activity.save()
            subject='Successfully added to the community'
            email=uform.cleaned_data.get('email',None)
            message={
                        'user':userme,
                        'site_name':site.site_name,
                        'site_url':site.site_url,
                        'address':site.address,
                        'location':site.location,
                        'description':site.description,
                        'phone':site.phone,
                        'email':email,
                        'username':userme.username,
                        'password':password,
                        'category':'Customer',
                     } 
            template='emails/new_user.html'
            send_email(subject,email,message,template)
            return JsonResponse({'valid':True,'message':'Customer created successfully'},content_type="application/json")
        else:
            return JsonResponse({'valid':False,'uform_errors':uform.errors,'eform_errors':eform.errors},content_type="application/json")

#editCustomer
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')
class editCustomer(View):
    def get(self ,request,id):
        obj=check_data()
        user = get_object_or_404(User,id=id)
        form=UserProfile(instance=user)
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        form2=ExtendedCustomerRegisterForm(instance=user.extendedauthuser)   
        messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        form3=UserPasswordCreateForm(instance=user)
        data={
            'title':f'Edit customer / {user.get_full_name()}',
            'obj':obj,
            'data':request.user,
            'form':form,
            'activities':activities,
            'user':user,
            'form2':form2,
            'count':count,
            'form3':form3,
            'prev':'Customers',
            'messages':messages,
            'edit':True,
            'room_name':request.user.username,
            'contact_messages':contact_messages,
            'contact_count':contact_count,  
    
        }
        return render(request,'panel/add_customer.html',context=data)

    def post(self,request,id,*args ,**kwargs):
        obj=check_data()
        user=get_object_or_404(User,id=id)
        uform=UserProfile(request.POST or None,instance=user)
        eform=ExtendedCustomerRegisterForm(request.POST,request.FILES or None,instance=user.extendedauthuser)
        if uform.is_valid() and  eform.is_valid():
            userme=uform.save(commit=False)
            edata=eform.save(commit=False)
            if eform.cleaned_data.get('is_deactivated',None):
                userme.is_active=False
                edata.is_deactivated=True
                subject='Account deactivated'
                email=uform.cleaned_data.get('email',None)
                message={
                        'user':userme,
                        'site_name':obj.site_name,
                        'site_url':obj.site_url,
                        'address':obj.address,
                        'location':obj.location,
                        'description':obj.description,
                        'phone':obj.phone
                    }
                template='emails/deactivated.html'
                send_email(subject,email,message,template)
            else:
                userme.is_active=True
                edata.is_deactivated=False 
            userme.save()
            edata.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-user"></i>',title='Customer update',user_id=request.user.pk,name=f'Customer updated {uform.cleaned_data.get("first_name",None)} {uform.cleaned_data.get("last_name",None)} successfully') 
            activity.save()
            return JsonResponse({'valid':True,'message':'Customer updated successfully'},content_type="application/json")
        else:
            return JsonResponse({'valid':False,'uform_errors':uform.errors,'eform_errors':eform.errors},content_type="application/json")


#publishOrder
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')
class publishOrder(View):
    def get(self,request,id):
        obj=check_data()
        order = get_object_or_404(OrderModel,id=id)
        form=OrderForm(instance=order)
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        styles=CitationModel.objects.all().order_by('-id')
        data=BidModel.objects.filter(parent_id__exact=id).order_by("-id")
        paginator=Paginator(data,30)
        page_num=request.GET.get('page')
        bids=paginator.get_page(page_num)
        data={
            'title':f'Publish Order id #{order.order_id}',
            'obj':obj,
            'data':request.user,
            'order':order,
            'id':id,
            'prev':'All Orders',
            'activities':activities,
            'bids':bids,
            'data_count':paginator.count,
            'styles':styles,
            'form':form,
            'edit':True,
            'assingment':True,
            'room_name':request.user.username,
            'contact_messages':contact_messages,
            'contact_count':contact_count,  
    
        }
        return render(request,'panel/new_order.html',context=data)
    def post(self,request,id):
        order=get_object_or_404(OrderModel,id__exact=id)
        form=OrderForm(request.POST,request.FILES  or None,instance=order)
        if form.is_valid():
            form.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-shopping-cart"></i>',title='Order update',user_id=request.user.pk,name=f'Published order on topic {form.cleaned_data.get("topic",None)}') 
            activity.save()
            return JsonResponse({'valid':True,'message':'Order published successfully.'},content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':form.errors},content_type='application/json')

#editOrder
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins','customer']),name='dispatch')
class editOrder(View):
    def get(self ,request,id):
        obj=check_data()
        order = get_object_or_404(OrderModel,id=id)
        form=OrderForm(instance=order)
        styles=CitationModel.objects.all().order_by('-id')
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
    
        data=BidModel.objects.select_related('user').filter(parent_id__exact=id).order_by("-id")
        paginator=Paginator(data,30)
        page_num=request.GET.get('page')
        bids=paginator.get_page(page_num)
        messages=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).order_by("-id")
        messages_count=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).count()
        inner_qs=IndividualModel.objects.values_list('parent_id',flat=True).distinct()
        qs2=BidModel.objects.filter(user_id=request.user.pk).values_list('user_id',flat=True)
        if inner_qs:
            count1=BroadcastModel.objects.filter(group=True).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
            count2=BroadcastModel.objects.filter(to__icontains=request.user.username).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
        else:
            count1=BroadcastModel.objects.filter(group=True).count()
            count2=BroadcastModel.objects.filter(to__icontains=request.user.username).count()
        broadcasts_count=int(count1+count2)
        if request.user.is_staff:
            data=order.user
        else:
            data=request.user
        data={
            'title':f'Edit order:#{order.order_id}',
            'obj':obj,
            'data':data,
            'form':form,
            'id':id,
            'order':order,
            'activities':activities,
            'styles':styles,
            'bids':bids,
            'mcount':paginator.count,
            'prev':'My Orders',
            'edit':True,
            'assingment':True,
            'room_name':request.user.username,
            'messages':messages,
            'messages_count':messages_count,
            'broadcasts_count':broadcasts_count,
            'contact_messages':contact_messages,
            'contact_count':contact_count,  
    
        }
        return render(request,'panel/new_order.html',context=data)

    def post(self,request,id,*args ,**kwargs):
        order=get_object_or_404(OrderModel,id=id)
        uform=OrderForm(request.POST,request.FILES or None,instance=order)
        files=request.FILES.getlist('additional_materials')
        if uform.is_valid():
            inviter=request.POST['inviter']
            d=uform.save(commit=False)
            if inviter:
                d.is_public=False
            else:
                d.is_public=True
            d.save()
            if BidModel.objects.filter(parent_id=id).exists():
                bids=BidModel.objects.filter(parent_id=id)
                bids.delete()
            for file in files:
               OrderMediaModel.objects.create(user_id=request.user.pk,parent_id=id,additional_materials=file)
            activity=ActivityModel.objects.create(icon='<i class="fa fa-shopping-cart"></i>',title='Order update',user_id=request.user.pk,name=f'Order #{order.order_id} updated successfully') 
            activity.save()
            updates=UpdatesModel.objects.create(user_id=request.user.pk,name=f'Order #{order.order_id} updated successfully')
            updates.save()
            return JsonResponse({'order_update':True,'valid':True,'message':f'Order #{order.order_id} updated successfully'},content_type="application/json")
        else:
            return JsonResponse({'valid':False,'uform_errors':uform.errors},content_type="application/json")

#cancelOrder
@login_required(login_url='/accounts/login')
@api_view(['GET',])
@allowed_users(allowed_roles=['admins','customer'])
def cancelOrder(request,id):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        try:
            obj=OrderModel.objects.get(id__exact=id)
            obj.is_cancelled=True
            obj.status='Cancelled'
            obj.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-shopping-cart"></i>',title='Order cancel',user_id=request.user.pk,name=f'Order #{obj.order_id} cancelled successfully') 
            activity.save()
            updates=UpdatesModel.objects.create(user_id=request.user.pk,name=f'Order #{obj.order_id} cancelled successfully')
            updates.save()
            return JsonResponse({'is_cancelled':False,'status':obj.status,'deleted':True,'message':f'Order #{obj.order_id} cancelled successfully.','id':id},content_type='application/json')       
        except PaymentModel.DoesNotExist:
            return JsonResponse({'deleted':True,'message':'Item does not exist'},content_type='application/json')


#uncancelOrder
@login_required(login_url='/accounts/login')
@api_view(['GET',])
@allowed_users(allowed_roles=['admins','customer'])
def uncancelOrder(request,id):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        try:
            obj=OrderModel.objects.get(id__exact=id)
            obj.is_cancelled=False
            obj.status='Uncancelled'
            obj.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-shopping-cart"></i>',title='Order uncancel',user_id=request.user.pk,name=f'Order #{obj.order_id} cancelled successfully') 
            activity.save()
            updates=UpdatesModel.objects.create(user_id=request.user.pk,name=f'Order #{obj.order_id} uncancelled successfully')
            updates.save()
            return JsonResponse({'is_cancelled':False,'status':obj.status,'deleted':True,'message':f'Order #{obj.order_id} uncancelled successfully.','id':id},content_type='application/json')       
        except PaymentModel.DoesNotExist:
            return JsonResponse({'deleted':True,'message':'Item does not exist'},content_type='application/json')

#readall
@login_required(login_url='/accounts/login')
@api_view(['GET',])
def readall(request,id):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        try:
            obj=ActivityModel.objects.filter(user_id__exact=id)
            obj.delete() 
            return JsonResponse({'deleted':True},content_type='application/json')       
        except ActivityModel.DoesNotExist:
            return JsonResponse({'deleted':True,'message':'Item does not exist'},content_type='application/json')


#deleteBid
@login_required(login_url='/accounts/login')
@api_view(['GET',])
@allowed_users(allowed_roles=['admins','writer'])
def deleteBid(request,id):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        try:
            obj=BidModel.objects.get(id__exact=id)
            obj.delete() 
            activity=ActivityModel.objects.create(icon='<i class="fa fa-shopping-cart"></i>',title='Bid deleted',user_id=request.user.pk,name=f'Bid on order #{obj.parent.order_id} deleted successfully') 
            activity.save()
            updates=UpdatesModel.objects.create(user_id=request.user.pk,name=f'Bid on order #{obj.parent.order_id} deleted')
            updates.save()
            return JsonResponse({'deleted':False,'message':f'Bid on order #{obj.parent.order_id} deleted successfully.','id':obj.order_id},content_type='application/json')       
        except PaymentModel.DoesNotExist:
            return JsonResponse({'deleted':True,'message':'Item does not exist'},content_type='application/json')

#deleteOrder
@login_required(login_url='/accounts/login')
@api_view(['GET',])
@allowed_users(allowed_roles=['admins','customer'])
def deleteOrder(request,id):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        try:
            obj=OrderModel.objects.get(id__exact=id)
            obj.delete() 
            activity=ActivityModel.objects.create(icon='<i class="fa fa-shopping-cart"></i>',title='Order deleted',user_id=request.user.pk,name=f'Order #{obj.order_id} deleted successfully') 
            activity.save()
            updates=UpdatesModel.objects.create(user_id=request.user.pk,name=f'Order #{obj.order_id} deleted')
            updates.save()
            return JsonResponse({'deleted':False,'message':f'Order #{obj.order_id} deleted successfully.','id':obj.order_id},content_type='application/json')       
        except PaymentModel.DoesNotExist:
            return JsonResponse({'deleted':True,'message':'Item does not exist'},content_type='application/json')

#publicOrder
@login_required(login_url='/accounts/login')
@api_view(['GET',])
@allowed_users(allowed_roles=['admins','customer'])
def publicOrder(request,id):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        try:
            site=check_data()
            obj=OrderModel.objects.get(id__exact=id)
            writer=get_object_or_404(User,id__exact=obj.inviter_id)
            email=writer.email
            obj.is_public=True
            obj.inviter=None
            obj.save() 
            activity=ActivityModel.objects.create(icon='<i class="fa fa-shopping-cart"></i>',title='Order public',user_id=request.user.pk,name=f'Order #{obj.order_id} made public successfully.') 
            activity.save()
            subject=f'Order #{obj.order_id} made public'
            time=timeago.format(obj.created_on, datetime.datetime.now())
            message={
                        'user':writer,
                        'site_name':site.site_name,
                        'site_url':site.site_url,
                        'address':site.address,
                        'location':site.location,
                        'description':site.description,
                        'phone':site.phone,
                        'order_id':obj.order_id,
                        'date':time,
                        'customer':obj.user.get_full_name(),
                     } 
            template='emails/answer.html'
            send_email(subject,email,message,template)
            return JsonResponse({'deleted':False,'message':f'Order #{obj.order_id} made public successfully.','id':id},content_type='application/json')       
        except PaymentModel.DoesNotExist:
            return JsonResponse({'deleted':True,'message':'Item does not exist'},content_type='application/json')



#placeBid
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['writer',]),name='dispatch')
class placeBid(View):
    def get(self,request,id):
        obj=check_data()
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        order=OrderModel.objects.select_related('user').filter(id__exact=id).last()
        media=OrderMediaModel.objects.filter(parent_id__exact=id)
        comments=OrderModel.objects.filter(user_id=order.user.id).values('comment').order_by("-id")[:10]
        if BidModel.objects.filter(parent_id__exact=id,user_id=request.user.pk).exists():
            bid=BidModel.objects.filter(parent_id__exact=id).last()
        else:
            bid=''
        form=BidForm()
        messages=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).order_by("-id")
        messages_count=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).count()
        inner_qs=IndividualModel.objects.values_list('parent_id',flat=True).distinct()
        qs2=BidModel.objects.filter(user_id=request.user.pk).values_list('user_id',flat=True)
        if inner_qs:
            count1=BroadcastModel.objects.filter(group=True).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
            count2=BroadcastModel.objects.filter(to__icontains=request.user.username).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
        else:
            count1=BroadcastModel.objects.filter(group=True).count()
            count2=BroadcastModel.objects.filter(to__icontains=request.user.username).count()
        broadcasts_count=int(count1+count2)
        qs1=BidModel.objects.values_list('parent_id',flat=True).distinct()
        qs2=BidModel.objects.filter(user_id=request.user.pk).values_list('user_id',flat=True)
        if request.user.extendedauthuser.is_verified:
            if qs1:
                new_projects=OrderModel.objects.filter(is_cancelled=False,is_completed=False,is_public=True,is_accepted=False).exclude(bidmodel__id__in=qs1).exclude(bidmodel__user_id__in=qs2).count()
            else:
                new_projects=OrderModel.objects.select_related('user').filter(is_cancelled=False,is_completed=False,is_public=True,is_accepted=False).count()
        else:
            new_projects=''
        data={
            'title':'Place Bid',
            'obj':obj,
            'data':request.user,
            'form':form,
            'bid':bid,
            'order':order,
            'media':media,
            'comments':comments,
            'activities':activities,
            'room_name':request.user.username,
            'messages':messages,
            'messages_count':messages_count,
            'broadcasts_count':broadcasts_count,
            'new_projects':new_projects,
    
        }
        return render(request,'panel/bid.html',context=data)
    def post(self,request,id,*args ,**kwargs):
        site=check_data()
        order=get_object_or_404(OrderModel,id__exact=id)
        uform=BidForm(request.POST or None)
        if uform.is_valid():
            if not order.is_accepted:
                data=uform.save(commit=False)
                data.user_id=request.user.pk
                data.order_id=order.order_id
                data.parent_id=id
                data.bid_id=generate_id()
                data.has_bid=True
                data.save()
                bids=BidModel.objects.filter(parent_id__exact=id).count()
                order.bids=bids
                order.save()
                activity=ActivityModel.objects.create(icon='<i class="fa fa-hand-pointer-o"></i>',title='Place Bid ',user_id=request.user.pk,name=f'Bid on order #{order.order_id} placed successfully') 
                activity.save()
                updates=UpdatesModel.objects.create(user_id=request.user.pk,name=f'Placed bid on order #{order.order_id} ')
                updates.save()
                subject=f'Your order #{order.order_id} has just received a new bid!!'
                email=order.user.email
                message={
                            'site_name':site.site_name,
                            'site_url':site.site_url,
                            'address':site.address,
                            'location':site.location,
                            'description':site.description,
                            'phone':site.phone,
                            'order':order,
                            'sender':request.user,
                         } 
                template='emails/new_bid.html'
                send_email(subject,email,message,template)
                return JsonResponse({'valid':True,'message':f'Bid on order #{order.order_id} placed successfully','bid':True},content_type="application/json")
            else:
                return JsonResponse({'deny':True,'valid':False,'message':'You cannot place bid on order accepted for another writer'},content_type="application/json") 
        else:
            return JsonResponse({'valid':False,'uform_errors':uform.errors},content_type="application/json")



#examOrderPrev
@login_required(login_url='/accounts/login')
@api_view(['GET',])
def examOrderPrev(request,id):
    obj=check_data()
    order=ExamModel.objects.select_related('user').filter(id__exact=id).last()
    media=ExamMediaModel.objects.filter(parent_id__exact=id)
    if BidModel.objects.filter(parent_id__exact=id,has_bid=True).select_related('user').exists():
        bids=BidModel.objects.select_related('user').filter(parent_id__exact=id,has_bid=True)
    else:
        data=BidModel.objects.filter(parent_id__exact=id).select_related('user').order_by("-id")
        paginator=Paginator(data,30)
        page_num=request.GET.get('page')
        bids=paginator.get_page(page_num)
    activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
    contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
    contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
    messages=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).order_by("-id")
    messages_count=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).count()
    inner_qs=IndividualModel.objects.values_list('parent_id',flat=True).distinct()
    qs2=BidModel.objects.filter(user_id=request.user.pk).values_list('user_id',flat=True)
    if inner_qs:
        count1=BroadcastModel.objects.filter(group=True).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
        count2=BroadcastModel.objects.filter(to__icontains=request.user.username).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
    else:
        count1=BroadcastModel.objects.filter(group=True).count()
        count2=BroadcastModel.objects.filter(to__icontains=request.user.username).count()
    broadcasts_count=int(count1+count2)
    data={
        'title':f'Preview Order #{order.order_id}',
        'obj':obj,
        'data':request.user,
        'order':order,
        'bids':bids,
        'media':media,
        'activities':activities,
        'mcount':paginator.count,
        'room_name':request.user.username,
        'messages':messages,
        'messages_count':messages_count,
        'broadcasts_count':broadcasts_count,
        'contact_messages':contact_messages,
        'contact_count':contact_count,  

    }
    return render(request,'panel/exam_order_prev.html',context=data)

#orderPrev
@login_required(login_url='/accounts/login')
@api_view(['GET',])
def orderPrev(request,id):
    obj=SiteModel.objects.count()
    obj=check_data()
    order=get_object_or_404(OrderModel,id__exact=id)
    messages=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).order_by("-id")
    messages_count=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).count()
    inner_qs=IndividualModel.objects.values_list('parent_id',flat=True).distinct()
    qs2=BidModel.objects.filter(user_id=request.user.pk).values_list('user_id',flat=True)
    if inner_qs:
        count1=BroadcastModel.objects.filter(group=True).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
        count2=BroadcastModel.objects.filter(to__icontains=request.user.username).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
    else:
        count1=BroadcastModel.objects.filter(group=True).count()
        count2=BroadcastModel.objects.filter(to__icontains=request.user.username).count()
    broadcasts_count=int(count1+count2)
    media=OrderMediaModel.objects.filter(parent_id__exact=id)
    if BidModel.objects.filter(parent_id__exact=id,has_bid=True).select_related('user').exists():
        bids=BidModel.objects.select_related('user').filter(parent_id__exact=id,has_bid=True)
        mcount=BidModel.objects.select_related('user').filter(parent_id__exact=id,has_bid=True).count()
    else:
        data=BidModel.objects.filter(parent_id__exact=id).select_related('user').order_by("-id")
        paginator=Paginator(data,30)
        page_num=request.GET.get('page')
        bids=paginator.get_page(page_num)
        mcount=paginator.count
    form=FinalForm()
    form1=OrderAnswerPad()
    comments=OrderModel.objects.filter(user_id=order.user.id).values('comment').order_by("-id")[:10]
    activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
    contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
    contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
    if SubmittedAnswerModel.objects.filter(parent_id__exact=id).exists():
        answers=SubmittedAnswerModel.objects.filter(parent_id__exact=id)
    else:
        answers=''
    data={
        'title':f'Preview Order #{order.order_id}',
        'obj':obj,
        'data':request.user,
        'order':order,
        'bids':bids,
        'media':media,
        'form':form,
        'form1':form1,
        'comments':comments,
        'answers':answers,
        'activities':activities,
        'mcount':mcount,
        'room_name':request.user.username,
        'messages':messages,
        'messages_count':messages_count,
        'broadcasts_count':broadcasts_count,
        'contact_messages':contact_messages,
        'contact_count':contact_count,  

    }
    return render(request,'panel/order_prev.html',context=data)


#BidDetails
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
class BidDetails(View):
    def get(self ,request,id):
        obj=SiteModel.objects.count()
        obj=check_data()
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        bid=BidModel.objects.select_related('parent','user').filter(id__exact=id).last()
        form=BiddingForm(instance=bid.parent)
        form1=ChatForm()
        if ChatModel.objects.select_related('user').filter(parent_id=bid.parent_id).exists():
            messages=ChatModel.objects.select_related('sender').filter(parent_id=bid.parent_id).order_by("id")
        else:
            messages=''
        messages_count=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).count()
        inner_qs=IndividualModel.objects.values_list('parent_id',flat=True).distinct()
        qs2=BidModel.objects.filter(user_id=request.user.pk).values_list('user_id',flat=True)
        if inner_qs:
            count1=BroadcastModel.objects.filter(group=True).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
            count2=BroadcastModel.objects.filter(to__icontains=request.user.username).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
        else:
            count1=BroadcastModel.objects.filter(group=True).count()
            count2=BroadcastModel.objects.filter(to__icontains=request.user.username).count()
        broadcasts_count=int(count1+count2)
        data={
            'title':f'Bid Details Of Order #{bid.parent.order_id}',
            'obj':obj,
            'data':request.user,
            'bid':bid,
            'form':form,
            'form1':form1,
            'activities':activities,
            'bid_details':True,
            'messages':messages,
            'edit':True,
            'bid_id':id,
            'room_name':request.user.username,
            'messages_count':messages_count,
            'broadcasts_count':broadcasts_count,
            'contact_messages':contact_messages,
            'contact_count':contact_count,  

        }
        return render(request,'panel/detailed_bid.html',context=data)
    def post(self,request,id,*args ,**kwargs):
        site=check_data()
        bid=BidModel.objects.select_related('parent','user').filter(id__exact=id).last()
        uform=BiddingForm(request.POST or None,instance=bid)
        if uform.is_valid():
            if not bid.parent.is_accepted:
                uform.save()
                order=OrderModel.objects.get(id__exact=bid.parent_id)
                service_charge=bid.service_charge
                deduct=int(order.budget)+int(service_charge)
                user=ExtendedAuthUser.objects.get(user_id__exact=request.user.pk)
                balance= user.balance
                time=timeago.format(bid.created_on, datetime.datetime.now())
                reserved= user.reserved_money
                if float(balance-deduct) > 0:
                    balance=float(balance-deduct)
                    reserved=int(reserved+bid.payable)
                    user.balance=balance
                    user.reserved_money=reserved
                    bid.is_accepted=True
                    bid.status='In Progress'
                    order.is_accepted=True
                    order.inprogress=True
                    order.status='In Progress'
                    bid.save() 
                    order.save()
                    user.save()
                    activity=ActivityModel.objects.create(icon='<i class="fa fa-hand-pointer-o"></i>',title='Bid Confirm',user_id=request.user.pk,name=f'Accepted bid on order #{bid.parent.order_id}') 
                    activity.save()
                    updates=UpdatesModel.objects.create(user_id=request.user.pk,name=f'{request.user.get_full_name()} accepted bid on order #{bid.parent.order_id}')
                    updates.save()
                    transaction=TransactionModel.objects.create(particulars=f'Funds paid for order id #{bid.parent.order_id}',status='Completed',user_id=request.user.pk,order_id=bid.parent.order_id,debit=deduct,balance=balance,reserved_money=request.user.extendedauthuser.reserved_money,mode='Wallet')
                    transaction.save()
                    subject=f'Bid Accepted on order #{bid.parent.order_id}'
                    email=bid.user.email
                    message={
                                'user':bid.user,
                                'site_name':site.site_name,
                                'site_url':site.site_url,
                                'address':site.address,
                                'location':site.location,
                                'description':site.description,
                                'phone':site.phone,
                                'order_id':bid.parent.order_id,
                                'date':time,
                             } 
                    template='emails/bid.html'
                    send_email(subject,email,message,template)
                    return JsonResponse({'accepted':True,'valid':True,'message':f'Bid on order #{bid.parent.order_id} confirmed successfully.','id':id},content_type='application/json')
                else:
                    return JsonResponse({'money':str(float(balance-deduct)).replace("-",""),'topup':True,'valid':False,'message':f'Insufficient balance.Please top up {site.currency_symbol}:{str(float(balance-deduct)).replace("-","")}'},content_type='application/json')
            else:
                return JsonResponse({'valid':False, 'deny':True,'message':f'Bid on order #{bid.parent.order_id} has already been accepted'},content_type="application/json")
        else:
            return JsonResponse({'valid':False,'uform_errors':uform.errors},content_type="application/json")

#writerOrderSummary
@login_required(login_url='/panel')
@api_view(['GET',])
@allowed_users(allowed_roles=['admins','writer'])
def writerOrderSummary(request):
    obj=check_data()
    activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
    contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
    contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()

    data1=BidModel.objects.select_related('parent').filter(parent__is_completed=False,parent__is_cancelled=True,user_id=request.user.pk).order_by("-id")
    paginator1=Paginator(data1,30)
    page_num1=request.GET.get('page')
    cancelled_orders=paginator1.get_page(page_num1)

    data2=BidModel.objects.select_related('parent').filter(user_id=request.user.pk,parent__is_cancelled=False,parent__inviter__isnull=False).order_by("-id")
    paginator2=Paginator(data2,30)
    page_num2=request.GET.get('page')
    invites=paginator2.get_page(page_num1)

    data3=BidModel.objects.select_related('parent').filter(parent__is_answered=False,parent__verdict__isnull=True,user_id=request.user.pk,parent__is_completed=False,parent__is_cancelled=False,parent__is_accepted=True,is_accepted=True).order_by("-id")
    paginator3=Paginator(data3,30)
    page_num3=request.GET.get('page')
    inprogress_orders=paginator3.get_page(page_num3)


    data6=BidModel.objects.select_related('parent').filter(user_id=request.user.pk,parent__is_completed=True,parent__is_cancelled=False).order_by("-id")
    paginator6=Paginator(data6,30)
    page_num6=request.GET.get('page')
    completed_orders=paginator6.get_page(page_num6)

    data7=BidModel.objects.select_related('parent').filter(user_id=request.user.pk,parent__is_completed=False,parent__is_cancelled=False,is_accepted=True,parent__verdict__icontains='dispute').order_by("-id")
    paginator7=Paginator(data7,30)
    page_num7=request.GET.get('page')
    disputes=paginator7.get_page(page_num7) 

    data8=BidModel.objects.select_related('parent').filter(user_id=request.user.pk,parent__is_completed=False,parent__is_cancelled=False,is_accepted=True,parent__verdict__icontains='revision').order_by("-id")
    paginator8=Paginator(data8,30)
    page_num8=request.GET.get('page')
    revisions=paginator8.get_page(page_num8)

    data9=BidModel.objects.filter(parent__is_answered=True,parent__is_completed=False,user_id=request.user.pk,parent__is_cancelled=False,is_accepted=True,is_completed=False,parent__is_accepted=True,parent__verdict__isnull=True).order_by("-id")
    paginator9=Paginator(data9,30)
    page_num9=request.GET.get('page')
    answered_orders=paginator9.get_page(page_num9)

    messages=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).order_by("-id")
    messages_count=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).count()
    inner_qs=IndividualModel.objects.values_list('parent_id',flat=True).distinct()
    qs2=BidModel.objects.filter(user_id=request.user.pk).values_list('user_id',flat=True)
    if inner_qs:
        count1=BroadcastModel.objects.filter(group=True).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
        count2=BroadcastModel.objects.filter(to__icontains=request.user.username).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
    else:
        count1=BroadcastModel.objects.filter(group=True).count()
        count2=BroadcastModel.objects.filter(to__icontains=request.user.username).count()
    broadcasts_count=int(count1+count2)
    data={
        'title':'My Orders',
        'obj':obj,
        'data':request.user,
        'invites':invites,
        'cancelled_orders':cancelled_orders,
        'inprogress_orders':inprogress_orders,
        'ccount':paginator1.count,
        'icount':paginator2.count,
        'incount':paginator3.count,
        'compcount':paginator6.count,
        'completed_orders':completed_orders,
        'room_name':request.user.username,
        'activities':activities,
        'disputes':disputes,
        'revisions':revisions,
        'dcount':paginator7.count,
        'rcount':paginator8.count,
        'messages':messages,
        'messages_count':messages_count,
        'broadcasts_count':broadcasts_count,
        'contact_messages':contact_messages,
        'contact_count':contact_count,  
        'answered_orders':answered_orders,
        'answered_orders_count':paginator9.count,

    }
    return render(request,'panel/writer_order_summary.html',context=data)

#orderSummary
@api_view(['GET',])
@allowed_users(allowed_roles=['admins','customer'])
def orderSummary(request):
    obj=check_data()
    activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
    contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
    contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
    data=OrderModel.objects.filter(is_completed=False,is_accepted=False,user_id=request.user.pk,is_cancelled=False).order_by("-id")
    paginator=Paginator(data,30)
    page_num=request.GET.get('page')
    orders=paginator.get_page(page_num)

    data1=OrderModel.objects.filter(is_completed=False,user_id=request.user.pk,is_cancelled=True).order_by("-id")
    paginator1=Paginator(data1,30)
    page_num1=request.GET.get('page')
    cancelled_orders=paginator1.get_page(page_num1)

    data2=OrderModel.objects.filter(is_completed=False,user_id=request.user.pk,is_cancelled=False,inviter__isnull=False,is_public=False).order_by("-id")
    paginator2=Paginator(data2,30)
    page_num2=request.GET.get('page')
    invites=paginator2.get_page(page_num1)

    data3=OrderModel.objects.filter(is_answered=False,user_id=request.user.pk,is_cancelled=False,is_accepted=True,is_completed=False,verdict__isnull=True).order_by("-id")
    paginator3=Paginator(data3,30)
    page_num3=request.GET.get('page')
    inprogress_orders=paginator3.get_page(page_num3)


    data5=OrderModel.objects.filter(is_completed=True,user_id=request.user.pk,is_cancelled=False).order_by("-id")
    paginator5=Paginator(data5,30)
    page_num5=request.GET.get('page')
    completed_orders=paginator5.get_page(page_num5)

    data7=OrderModel.objects.filter(user_id=request.user.pk,is_cancelled=False,is_accepted=True,is_completed=False,verdict__icontains='dispute').order_by("-id")
    paginator7=Paginator(data7,30)
    page_num7=request.GET.get('page')
    disputes=paginator7.get_page(page_num7) 

    data8=OrderModel.objects.filter(user_id=request.user.pk,is_cancelled=False,is_accepted=True,is_completed=False,verdict__icontains='revision').order_by("-id")
    paginator8=Paginator(data8,30)
    page_num8=request.GET.get('page')
    revisions=paginator8.get_page(page_num8)

    data9=OrderModel.objects.filter(verdict__isnull=True,is_answered=True,is_completed=False,user_id=request.user.pk,is_cancelled=False).order_by("-id")
    paginator9=Paginator(data9,30)
    page_num9=request.GET.get('page')
    answered_orders=paginator9.get_page(page_num9)

    messages=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).order_by("-id")
    messages_count=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).count()
    inner_qs=IndividualModel.objects.values_list('parent_id',flat=True).distinct()
    qs2=BidModel.objects.filter(user_id=request.user.pk).values_list('user_id',flat=True)
    if inner_qs:
        count1=BroadcastModel.objects.filter(group=True).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
        count2=BroadcastModel.objects.filter(to__icontains=request.user.username).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
    else:
        count1=BroadcastModel.objects.filter(group=True).count()
        count2=BroadcastModel.objects.filter(to__icontains=request.user.username).count()
    broadcasts_count=int(count1+count2)
    data={
        'title':'My Orders',
        'obj':obj,
        'data':request.user,
        'orders':orders,
        'activities':activities,
        'invites':invites,
        'completed_orders':completed_orders,
        'cancelled_orders':cancelled_orders,
        'inprogress_orders':inprogress_orders,
        'answered_orders':answered_orders,
        'answered_orders_count':paginator9.count,
        'ocount':paginator.count,
        'ccount':paginator1.count,
        'icount':paginator2.count,
        'incount':paginator3.count,
        'compcount':paginator5.count,
        'room_name':request.user.username,
        'revisions':revisions,
        'disputes':disputes,
        'dcount':paginator7.count,
        'rcount':paginator8.count,
        'messages':messages,
        'messages_count':messages_count,
        'broadcasts_count':broadcasts_count,
        'contact_messages':contact_messages,
        'contact_count':contact_count,  

    }
    return render(request,'panel/order_summary.html',context=data)

#writer
@api_view(['GET',])
def writer(request,id):
    obj=check_data()
    user=get_object_or_404(User,id__exact=id)
    activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
    contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
    contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
    if user.extendedauthuser.category =='writer':
        t1=OrderModel.objects.values_list('id',flat=True)
        comments_count=BidModel.objects.filter(user_id__exact=id,parent_id__in=t1,comment__isnull=False).count()
        data1=OrderModel.objects.filter(completer_id__exact=id).order_by("-id")
        paginator1=Paginator(data1,30)
        page_num1=request.GET.get('page')
        client_comments=paginator1.get_page(page_num1)
    else:
        t1=BidModel.objects.filter(user_id=request.user.pk).values_list('user_id',flat=True)
        data1=BidModel.objects.filter(client_id__exact=id).order_by("-id")
        paginator1=Paginator(data1,30)
        page_num1=request.GET.get('page')
        client_comments=paginator1.get_page(page_num1)

    messages=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).order_by("-id")
    messages_count=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).count()
    inner_qs=IndividualModel.objects.values_list('parent_id',flat=True).distinct()
    qs2=BidModel.objects.filter(user_id=request.user.pk).values_list('user_id',flat=True)
    if inner_qs:
        count1=BroadcastModel.objects.filter(group=True).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
        count2=BroadcastModel.objects.filter(to__icontains=request.user.username).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
    else:
        count1=BroadcastModel.objects.filter(group=True).count()
        count2=BroadcastModel.objects.filter(to__icontains=request.user.username).count()
    broadcasts_count=int(count1+count2)
    category= (user.extendedauthuser.category).capitalize()
    data={
        'title':f'{category} / {user.get_full_name()}',
        'obj':obj,
        'data':request.user,
        'user':user,
        'activities':activities,
        'bids':bids,
        'room_name':request.user.username,
        'messages':messages,
        'messages_count':messages_count,
        'broadcasts_count':broadcasts_count,
        'client_comments':client_comments,
        'client_comments_count':paginator1.count,
        'contact_messages':contact_messages,
        'contact_count':contact_count,  


    }
    return render(request,'panel/writer.html',context=data)


#acceptBid
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['customer','admins'])
@api_view(['GET',])
def acceptBid(request,id):
    obj=SiteModel.objects.count()
    obj=check_data()
    messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
    count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
    bid=get_object_or_404(BidModel,id__exact=id)
    activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
    contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
    contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
    order=OrderModel.objects.get(id__exact=bid.order_id)
    messages=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).order_by("-id")
    messages_count=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).count()
    inner_qs=IndividualModel.objects.values_list('parent_id',flat=True).distinct()
    qs2=BidModel.objects.filter(user_id=request.user.pk).values_list('user_id',flat=True)
    if inner_qs:
        count1=BroadcastModel.objects.filter(group=True).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
        count2=BroadcastModel.objects.filter(to__icontains=request.user.username).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
    else:
        count1=BroadcastModel.objects.filter(group=True).count()
        count2=BroadcastModel.objects.filter(to__icontains=request.user.username).count()
    broadcasts_count=int(count1+count2)
    data={
        'title':f'Accept bid on #{order.order_id}',
        'obj':obj,
        'order':order,
        'activities':activities,
        'data':request.user,
        'bid':bid,
        'messages':messages,
        'count':count,
        'room_name':request.user.username,
        'messages':messages,
        'messages_count':messages_count,
        'broadcasts_count':broadcasts_count,
        'contact_messages':contact_messages,
        'contact_count':contact_count,  

    }
    return render(request,'panel/accept_bid.html',context=data)



#deleteMessage
@login_required(login_url='/accounts/login')

@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def deleteMessage(request,id):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        try:
            obj=ContactModel.objects.get(id__exact=id)
            activity=ActivityModel.objects.create(icon='<i class="fa fa-envelope"></i>',title='Message update',user_id=request.user.pk,name=f'Deleted message {obj.message}') 
            activity.save()
            obj.delete()
            return JsonResponse({'deleted':False,'message':'Message deleted successfully.','id':id},content_type='application/json')       
        except ContactModel.DoesNotExist:
            return JsonResponse({'deleted':True,'message':'Item does not exist'},content_type='application/json')


#deleteday
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def rejectEssay(request,id):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        try:
            site=check_data()
            obj=ExtendedAuthUser.objects.get(user_id__exact=id)
            obj.essay_ok=False
            obj.save()
            user=User.objects.get(id__exact=obj.user_id)
            activity=ActivityModel.objects.create(icon='<i class="fa fa-cog"></i>',title='Essay Rejected',user_id=request.user.pk,name=f'Rejected essay {user.get_full_name()}') 
            activity.save()
            updates=UpdatesModel.objects.create(user_id=request.user.pk,name=f'Essay of  {user.get_full_name()} was rejected')
            updates.save()
            subject='Essay Rejected'
            email=obj.user.email
            message={
                        'user':obj.user,
                        'site_name':site.site_name,
                        'site_url':site.site_url,
                        'address':site.address,
                        'location':site.location,
                        'description':site.description,
                        'phone':site.phone,
                     } 
            template='emails/essay.html'
            send_email(subject,email,message,template)
            return JsonResponse({'deleted':False,'message':'Essay rejected successfully.','id':id},content_type='application/json')       
        except ExtendedAuthUser.DoesNotExist:
            return JsonResponse({'deleted':True,'message':'Item does not exist'},content_type='application/json')

#verifyUser
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def verifyUser(request,id):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        try:
            obj=ExtendedAuthUser.objects.get(user_id__exact=id)
            obj.is_verified=True
            obj.save()
            user=User.objects.get(id__exact=obj.user_id)
            activity=ActivityModel.objects.create(icon='<i class="fa fa-user"></i>',title='Verify user ',user_id=request.user.pk,name=f'Verified user {user.get_full_name()}') 
            activity.save()
            return JsonResponse({'deleted':False,'message':f'User {user.get_full_name()} verified successfully.','id':id},content_type='application/json')       
        except ExtendedAuthUser.DoesNotExist:
            return JsonResponse({'deleted':True,'message':'Item does not exist'},content_type='application/json')


#bidPreview
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins','writer']),name='dispatch')
class bidPreview(View):
    def get(self,request,id):
        obj=SiteModel.objects.count()
        obj=check_data()
        order=BidModel.objects.select_related('parent').filter(id__exact=id).last()
        form=WriterFinalForm(instance=order)
        form1=AnswerPad()
        messages=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).order_by("-id")
        messages_count=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).count()
        inner_qs=IndividualModel.objects.values_list('parent_id',flat=True).distinct()
        qs2=BidModel.objects.filter(user_id=request.user.pk).values_list('user_id',flat=True)
        if inner_qs:
            count1=BroadcastModel.objects.filter(group=True).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
            count2=BroadcastModel.objects.filter(to__icontains=request.user.username).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
        else:
            count1=BroadcastModel.objects.filter(group=True).count()
            count2=BroadcastModel.objects.filter(to__icontains=request.user.username).count()
        broadcasts_count=int(count1+count2)
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        if SubmittedAnswerModel.objects.filter(parent_id__exact=order.parent.id).exists():
            answers=SubmittedAnswerModel.objects.filter(parent_id__exact=order.parent.id)
        else:
            answers=''
        media=OrderMediaModel.objects.filter(parent_id__exact=order.parent_id)
        data={
            'title':'Bid Preview',
            'obj':obj,
            'activities':activities,
            'data':request.user,
            'order':order,
            'form':form,
            'form1':form1,
            'answers':answers,
            'media':media,
            'room_name':request.user.username,
            'messages':messages,
            'messages_count':messages_count,
            'broadcasts_count':broadcasts_count,
            'contact_messages':contact_messages,
            'contact_count':contact_count,  

        }
        return render(request,'panel/bid_preview.html',context=data)
    def post(self,request,id,*args ,**kwargs):
        order=BidModel.objects.select_related('parent').filter(id__exact=id).last()
        uform=WriterFinalForm(request.POST or None,instance=order)
        if uform.is_valid():
            uform.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-user"></i>',title='Client review',user_id=request.user.pk,name=f'Rated client {order.user.get_full_name()} {uform.cleaned_data.get("rating",None)}  star(s).') 
            activity.save()
            updates=UpdatesModel.objects.create(user_id=request.user.pk,name=f'{request.user.get_full_name()} rated client {order.user.get_full_name()} {uform.cleaned_data.get("rating",None)}  star(s).')
            updates.save()
            avg_rating=OrderModel.objects.filter(user_id=order.parent.user_id).aggregate(Avg('rating'))
            rating_count=OrderModel.objects.filter(user_id=order.parent.user_id,rating__isnull=False).count()
            user=ExtendedAuthUser.objects.get(user_id=order.parent.user_id)
            user.rating=round(avg_rating['rating__avg'])
            user.rating_counter=rating_count
            user.save()
            return JsonResponse({'valid':True,'message':'Client review submitted successfully'},content_type="application/json")
        else:
            return JsonResponse({'valid':False,'uform_errors':uform.errors},content_type="application/json")



#manageCustomerReviews
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')
class manageCustomerReviews(View):
    def get(self ,request):
        obj=check_data()
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        form=ReviewForm()
        data={
            'title':'Manage Customer Reviews',
            'obj':obj,
            'data':request.user,
            'activities':activities,
            'form':form,
            'messages':messages,
            'writer':False,
            'room_name':request.user.username,
            'contact_messages':contact_messages,
            'contact_count':contact_count,  
    
        }
        return render(request,'panel/reviews.html',context=data)

    def post(self,request,*args ,**kwargs):
        uform=ReviewForm(request.POST,request.FILES or None)
        if uform.is_valid():
            usr=uform.save(commit=False)
            usr.user_id=request.user.pk
            usr.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-cog"></i>',title='Writer review',user_id=request.user.pk,name=f'Added writer review  of  {uform.cleaned_data.get("name",None)}  successfully') 
            activity.save()
            updates=UpdatesModel.objects.create(user_id=request.user.pk,name=f'Writer:{request.user.get_full_name()} placed a review')
            updates.save()
            return JsonResponse({'valid':True,'message':'Writer review created successfully'},content_type="application/json")
        else:
            return JsonResponse({'valid':False,'uform_errors':uform.errors},content_type="application/json")

#manageReviews
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')
class manageReviews(View):
    def get(self ,request):
        obj=check_data()
        messages=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).order_by("-id")
        messages_count=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).count()
        inner_qs=IndividualModel.objects.values_list('parent_id',flat=True).distinct()
        qs2=BidModel.objects.filter(user_id=request.user.pk).values_list('user_id',flat=True)
        if inner_qs:
            count1=BroadcastModel.objects.filter(group=True).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
            count2=BroadcastModel.objects.filter(to__icontains=request.user.username).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
        else:
            count1=BroadcastModel.objects.filter(group=True).count()
            count2=BroadcastModel.objects.filter(to__icontains=request.user.username).count()
        broadcasts_count=int(count1+count2)
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        form=ReviewForm()
        data={
            'title':'Manage Writer Reviews',
            'obj':obj,
            'data':request.user,
            'activities':activities,
            'form':form,
            'messages':messages,
            'messages_count':messages_count,
            'broadcasts_count':broadcasts_count,
            'writer':True,
            'room_name':request.user.username,
            'contact_messages':contact_messages,
            'contact_count':contact_count,  
    
        }
        return render(request,'panel/reviews.html',context=data)

    def post(self,request,*args ,**kwargs):
        uform=ReviewForm(request.POST,request.FILES or None)
        if uform.is_valid():
            usr=uform.save(commit=False)
            usr.user_id=request.user.pk
            usr.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-cog"></i>',title='Writer review',user_id=request.user.pk,name=f'Added writer review  of  {uform.cleaned_data.get("name",None)}  successfully') 
            activity.save()
            updates=UpdatesModel.objects.create(user_id=request.user.pk,name=f'Writer:{request.user.get_full_name()} placed a review')
            updates.save()
            return JsonResponse({'valid':True,'message':'Writer review created successfully'},content_type="application/json")
        else:
            return JsonResponse({'valid':False,'uform_errors':uform.errors},content_type="application/json")


#customerReview
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def customerReview(request):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        uform=ReviewForm(request.POST,request.FILES or None)
        if uform.is_valid():
            usr=uform.save(commit=False)
            usr.user_id=request.user.pk
            usr.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-cog"></i>',title='Cuustomer review',user_id=request.user.pk,name=f'Added customer review  of  {uform.cleaned_data.get("name",None)}  successfully') 
            activity.save()
            updates=UpdatesModel.objects.create(user_id=request.user.pk,name=f'Customer {request.user.get_full_name()} placed a review')
            updates.save()
            return JsonResponse({'valid':True,'message':'Customer review created successfully'},content_type="application/json")
        else:
            return JsonResponse({'valid':False,'uform_errors':uform.errors},content_type="application/json")

#allReviews
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def allReviews(request):
    obj=check_data()
    activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
    data=ReviewModel.objects.filter(category__icontains='writer').order_by("-id")
    paginator=Paginator(data,30)
    page_num=request.GET.get('page')
    writer_reviews=paginator.get_page(page_num)

    data1=ReviewModel.objects.filter(category__icontains='customer').order_by("-id")
    paginator1=Paginator(data1,30)
    page_num1=request.GET.get('page')
    customer_reviews=paginator1.get_page(page_num1)
    contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
    contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
    data={
        'title':'All Reviews',
        'obj':obj,
        'activities':activities,
        'data':request.user,
        'writer_reviews':writer_reviews,
        'customer_reviews':customer_reviews,
        'writer_count':paginator.count,
        'customer_count':paginator1.count,
        'room_name':request.user.username,
        'contact_messages':contact_messages,
        'contact_count':contact_count,  

    }
    return render(request,'panel/all_reviews.html',context=data)

#EditWriterReview
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')
class EditWriterReview(View):
    def get(self ,request,id):
        obj=check_data()
        user = get_object_or_404(ReviewModel,id=id)
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        form=ReviewForm(instance=user)
        data={
            'title':f'Edit Writer Review / {user.message}',
            'obj':obj,
            'data':request.user,
            'activities':activities,
            'form':form,
            'writer':True,
            'room_name':request.user.username,
            'contact_messages':contact_messages,
            'contact_count':contact_count,  
    
        }
        return render(request,'panel/reviews.html',context=data)

    def post(self,request,id,*args ,**kwargs):
        user=get_object_or_404(ReviewModel,id__exact=id)
        uform=ReviewForm(request.POST,request.FILES or None,instance=user)
        if uform.is_valid():
            uform.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-cog"></i>',title='Writer review', user_id=request.user.pk,name=f'Edited writer review  of  {uform.cleaned_data.get("name",None)}  successfully') 
            activity.save()
            return JsonResponse({'valid':True,'message':'Writer review updated successfully'},content_type="application/json")
        else:
            return JsonResponse({'valid':False,'uform_errors':uform.errors},content_type="application/json")


#EditCustomerReview
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')
class EditCustomerReview(View):
    def get(self ,request,id):
        obj=check_data()
        user = get_object_or_404(ReviewModel,id=id)
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        form=ReviewForm(instance=user)
        data={
            'title':f'Edit Customer Review / {user.message}',
            'obj':obj,
            'data':request.user,
            'activities':activities,
            'form':form,
            'id':id,
            'writer':False,
            'room_name':request.user.username,
            'contact_messages':contact_messages,
            'contact_count':contact_count,  
    
        }
        return render(request,'panel/reviews.html',context=data)

    def post(self,request,id,*args ,**kwargs):
        user=get_object_or_404(ReviewModel,id__exact=id)
        uform=ReviewForm(request.POST,request.FILES or None,instance=user)
        if uform.is_valid():
            uform.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-cog"></i>',title='Customer update',user_id=request.user.pk,name=f'Edited customer review  of  {uform.cleaned_data.get("name",None)}  successfully') 
            activity.save()
            return JsonResponse({'valid':True,'message':'Customer review updated successfully'},content_type="application/json")
        else:
            return JsonResponse({'valid':False,'uform_errors':uform.errors},content_type="application/json")

#deleteAdmin
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def deleteAdmin(request,id):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        try:
            obj=User.objects.get(id__exact=id)
            activity=ActivityModel.objects.create(icon='<i class="fa fa-trash"></i>',title='Admin delete', user_id=request.user.pk,name=f'Deleted writer review of {obj.name}') 
            activity.save()
            obj.delete() 
            return JsonResponse({'deleted':False,'message':'Admin deleted successfully.','id':id},content_type='application/json')       
        except ReviewModel.DoesNotExist:
            return JsonResponse({'deleted':True,'message':'Item does not exist'},content_type='application/json')


#deleteWriterReview
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def deleteWriterReview(request,id):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        try:
            obj=ReviewModel.objects.get(id__exact=id)
            activity=ActivityModel.objects.create(icon='<i class="fa fa-trash"></i>',title='Writer review delete', user_id=request.user.pk,name=f'Deleted writer review of {obj.name}') 
            activity.save()
            obj.delete() 
            return JsonResponse({'deleted':False,'message':'Review deleted successfully.','id':id},content_type='application/json')       
        except ReviewModel.DoesNotExist:
            return JsonResponse({'deleted':True,'message':'Item does not exist'},content_type='application/json')

#deleteCustomerReview
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def deleteCustomerReview(request,id):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        try:
            obj=ReviewModel.objects.get(id__exact=id)
            activity=ActivityModel.objects.create(icon='<i class="fa fa-trash"></i>',title='Customer review delete',user_id=request.user.pk,name=f'Deleted customer review of {obj.name}') 
            activity.save()
            obj.delete() 
            return JsonResponse({'deleted':False,'message':'Customer review deleted successfully.','id':id},content_type='application/json')       
        except ReviewModel.DoesNotExist:
            return JsonResponse({'deleted':True,'message':'Item does not exist'},content_type='application/json')


#samples
@api_view(['GET',])
def samples(request):
    obj=check_data()
    messages=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).order_by("-id")
    messages_count=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).count()
    inner_qs=IndividualModel.objects.values_list('parent_id',flat=True).distinct()
    qs2=BidModel.objects.filter(user_id=request.user.pk).values_list('user_id',flat=True)
    if inner_qs:
        count1=BroadcastModel.objects.filter(group=True).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
        count2=BroadcastModel.objects.filter(to__icontains=request.user.username).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
    else:
        count1=BroadcastModel.objects.filter(group=True).count()
        count2=BroadcastModel.objects.filter(to__icontains=request.user.username).count()
    broadcasts_count=int(count1+count2)
    activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
    contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
    contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
    if request.GET.get('topic'):
        data=SamplesModel.objects.filter(topic__icontains=request.GET.get('topic'))
    else:
        data=SamplesModel.objects.all().order_by("-id")
    paginator=Paginator(data,15)
    page_num=request.GET.get('page')
    samples=paginator.get_page(page_num)
    data={
        'title':'Samples',
        'obj':obj,
        'data':request.user,
        'activities':activities,
        'messages':messages,
        'messages_count':messages_count,
        'broadcasts_count':broadcasts_count,
        'samples':samples,
        'room_name':request.user.username,
        'contact_messages':contact_messages,
        'contact_count':contact_count,

    }
    return render(request,'panel/samples.html',context=data)

#editSample
class editSample(View):
    def get(self,request,id):
        obj=check_data()
        messages=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).order_by("-id")
        messages_count=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).count()
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        sample=get_object_or_404(SamplesModel,id__exact=id)
        data=CommentModel.objects.filter(blog_id=id).order_by("-id")
        paginator=Paginator(data,30)
        page_num=request.GET.get('page')
        comments=paginator.get_page(page_num)
        form=ContentForm(instance=sample) 
        data={
            'title':'Edit sample paper ',
            'obj':obj,
            'data':request.user,
            'activities':activities,
            'messages':messages,
            'comments':comments,
            'messages_count':messages_count,
            'sample':sample,
            'form':form,
            'room_name':request.user.username,
            'contact_messages':contact_messages,
            'contact_count':contact_count,

        }
        return render(request,'panel/edit_sample.html',context=data)

    def post(self,request,id):
        sample=get_object_or_404(SamplesModel,id__exact=id)
        uform=ContentForm(request.POST,request.FILES or None,instance=sample)
        if uform.is_valid():
            uform.save()
            return JsonResponse({'valid':True,'message':'Sample paper content update successfully'},content_type="application/json")
        else:
            return JsonResponse({'valid':False,'uform_errors':uform.errors},content_type="application/json")
#sampleSingle
class sampleSingle(View):
    def get(self,request,id):
        obj=check_data()
        sample=get_object_or_404(SamplesModel,id=id)
        messages=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).order_by("-id")
        messages_count=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).count()
        inner_qs=IndividualModel.objects.values_list('parent_id',flat=True).distinct()
        qs2=BidModel.objects.filter(user_id=request.user.pk).values_list('user_id',flat=True)
        if inner_qs:
            count1=BroadcastModel.objects.filter(group=True).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
            count2=BroadcastModel.objects.filter(to__icontains=request.user.username).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
        else:
            count1=BroadcastModel.objects.filter(group=True).count()
            count2=BroadcastModel.objects.filter(to__icontains=request.user.username).count()
        broadcasts_count=int(count1+count2)
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        topics=SamplesModel.objects.exclude(id=id).order_by("-id")
        comments=CommentModel.objects.all().order_by("-id")[:5]
        comment_count=CommentModel.objects.filter(blog_id__exact=id).count()
        form=CommentForm()
        data={
            'title':f'Topic {sample.topic}',
            'obj':obj,
            'data':request.user,
            'activities':activities,
            'messages':messages,
            'messages_count':messages_count,
            'broadcasts_count':broadcasts_count,
            'topics':topics,
            'comments':comments,
            'form':form,
            'sample':sample,
            'comment_count':comment_count,
            'room_name':request.user.username,
            'contact_messages':contact_messages,
            'contact_count':contact_count,

        }
        return render(request,'panel/sample_single.html',context=data)

    def post(self,request,id):
        uform=CommentForm(request.POST or None)
        if uform.is_valid():
            userme=uform.save(commit=False)
            userme.blog_id=id
            userme.save()
            sample=SamplesModel.objects.get(id__exact=id)
            sample.comment_count=CommentModel.objects.filter(blog_id__exact=id).count()
            sample.save()
            return JsonResponse({'valid':True,'message':'Comment submitted successfully'},content_type="application/json")
        else:
            return JsonResponse({'valid':False,'uform_errors':uform.errors},content_type="application/json")

#addCategory
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')
class addCategory(View):
    def get(self,request):
        obj=check_data()
        messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        form=CategoryForm()   
        data={
            'title':'Add Blog Category',
            'obj':obj,
            'data':request.user,
            'form':form,
            'activities':activities,
            'count':count,
            'messages':messages,
            'room_name':request.user.username,
            'contact_messages':contact_messages,
            'contact_count':contact_count,
    
        }
        return render(request,'panel/add_category.html',context=data)
    def post(self,request):
        uform=CategoryForm(request.POST or None)
        if uform.is_valid():
            userme=uform.save(commit=False)
            userme.user_id =request.user.pk
            userme.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-blog"></i>',title='Blog Category', user_id=request.user.pk,name=f'Created blog category {uform.cleaned_data.get("name",None)} successfully') 
            activity.save()
            return JsonResponse({'valid':True,'message':'Blog Category created successfully'},content_type="application/json")
        else:
            return JsonResponse({'valid':False,'uform_errors':uform.errors},content_type="application/json")

#editComment
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')
class editComment(View):
    def get(self,request,id):
        obj=check_data()
        comment=get_object_or_404(CommentModel,id__exact=id)
        messages=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).order_by("-id")
        messages_count=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).count()
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        form=CommentForm(instance=comment)
        data={
            'title':'Edit Comment',
            'obj':obj,
            'data':request.user,
            'form':form,
            'activities':activities,
            'messages':messages,
            'comment':comment,
            'messages_count':messages_count,
            'room_name':request.user.username,
            'contact_messages':contact_messages,
            'contact_count':contact_count,
    
        }
        return render(request,'panel/edit_comment.html',context=data)
    def post(self,request,id):
        comment=get_object_or_404(CommentModel,id__exact=id)
        uform=CommentForm(request.POST or None,instance=comment)
        if uform.is_valid():
            uform.save()
            activity=ActivityModel.objects.create(icon='<i class="ti-comments"></i>',title='Comment ', user_id=request.user.pk,name=f"Updated {uform.cleaned_data.get('name',None)}'s comment.")
            activity.save()
            return JsonResponse({'valid':True,'message':'User comment updated successfully'},content_type="application/json")
        else:
            return JsonResponse({'valid':False,'uform_errors':uform.errors},content_type="application/json")


#deleteComment
@login_required(login_url='/panel')
@allowed_users(allowed_roles=['admins'])
def deleteComment(request,id):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        try:
            obj=CommentModel.objects.get(id__exact=id)
            activity=ActivityModel.objects.create(user_id=request.user.pk,name=f'Deleted comment beloging to {obj.name}') 
            activity.save()
            obj.delete() 
            return JsonResponse({'valid':True,'message':'Comment deleted successfully.','id':id},content_type='application/json')       
        except CommentModel.DoesNotExist:
            return JsonResponse({'valid':False,'message':'Item does not exist'},content_type='application/json')

#editCategory
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')
class editCategory(View):
    def get(self,request,id):
        obj=check_data()
        user = get_object_or_404(CategoryModel,id=id)
        messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        form=CategoryForm(instance=user)   
        data={
            'title':f'Edit Blog Category / {user.name}',
            'obj':obj,
            'data':request.user,
            'form':form,
            'activities':activities,
            'count':count,
            'messages':messages,
            'edit':True,
            'room_name':request.user.username,
            'contact_messages':contact_messages,
            'contact_count':contact_count,
    
        }
        return render(request,'panel/add_category.html',context=data)
    def post(self,request,id):
        user=get_object_or_404(CategoryModel,id__exact=id)
        uform=CategoryForm(request.POST or None,instance=user)
        if uform.is_valid():
            uform.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-blog"></i>',title='Edit category', user_id=request.user.pk,name=f'Edited blog category {uform.cleaned_data.get("name",None)} successfully') 
            activity.save()
            return JsonResponse({'valid':True,'message':'Blog Category updated successfully'},content_type="application/json")
        else:
            return JsonResponse({'valid':False,'uform_errors':uform.errors},content_type="application/json")

#deleteBlogCategory
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def deleteBlogCategory(request,id):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        try:
            obj=CategoryModel.objects.get(id__exact=id)
            activity=ActivityModel.objects.create(icon='<i class="fa fa-trash"></i>',title='Delete blog category',user_id=request.user.pk,name=f'Deleted blog category {obj.name}') 
            activity.save()
            obj.delete() 
            return JsonResponse({'deleted':False,'message':'Blog category deleted successfully.','id':id},content_type='application/json')       
        except CategoryModel.DoesNotExist:
            return JsonResponse({'deleted':True,'message':'Item does not exist'},content_type='application/json')


#blogPosts
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def blogPosts(request):
    if SiteModel.objects.count() == 0:
        return redirect('/installation/')
    obj=SiteModel.objects.all()[0]
    activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
    contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
    contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
    messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
    count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
    data=BlogModel.objects.all().order_by("-id")
    paginator=Paginator(data,30)
    page_num=request.GET.get('page')
    posts=paginator.get_page(page_num)
    data={
        'title':'Blog Posts',
        'obj':obj,
        'data':request.user,
        'activities':activities,
        'count':count,
        'mcount':paginator.count,
        'messages':messages,
        'posts':posts,
        'room_name':request.user.username,
        'contact_messages':contact_messages,
        'contact_count':contact_count,

    }
    return render(request,'panel/blog_posts.html',context=data)


#addBlogPost
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')
class addBlogPost(View):
    def get(self,request):
        obj=check_data()
        messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        categories=CategoryModel.objects.all().order_by("-id")
        form=BlogForm()   
        data={
            'title':'Add Blog Post',
            'obj':obj,
            'data':request.user,
            'form':form,
            'categories':categories,
            'activities':activities,
            'count':count,
            'messages':messages,
            'room_name':request.user.username,
            'contact_messages':contact_messages,
            'contact_count':contact_count,
    
        }
        return render(request,'panel/add_blog.html',context=data)
    def post(self,request):
        uform=BlogForm(request.POST,request.FILES or None)
        if uform.is_valid():
            userme=uform.save(commit=False)
            userme.user_id =request.user.pk
            userme.name =request.user.get_full_name()
            userme.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-plus"></i>',title='Blog Post', user_id=request.user.pk,name=f'Created blog post {uform.cleaned_data.get("title",None)} successfully') 
            activity.save()
            return JsonResponse({'valid':True,'message':'Blog post created successfully'},content_type="application/json")
        else:
            return JsonResponse({'valid':False,'uform_errors':uform.errors},content_type="application/json")


#EditBlogPost
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')
class EditBlogPost(View):
    def get(self,request,id):
        obj=check_data()
        user = get_object_or_404(BlogModel,id=id)
        messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        categories=CategoryModel.objects.all().order_by("-id")
        form=BlogForm(instance=user)   
        data={
            'title':f'Edit Blog Post / {user.name}',
            'obj':obj,
            'data':request.user,
            'form':form,
            'categories':categories,
            'activities':activities,
            'count':count,
            'messages':messages,
            'edit':True,
            'room_name':request.user.username,
            'contact_messages':contact_messages,
            'contact_count':contact_count,
    
        }
        return render(request,'panel/add_blog.html',context=data)
    def post(self,request,id):
        user=get_object_or_404(BlogModel,id__exact=id)
        uform=BlogForm(request.POST,request.FILES or None,instance=user)
        if uform.is_valid():
            uform.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-blog"></i>',title='Blog update',user_id=request.user.pk,name=f'Edited blog post {uform.cleaned_data.get("title",None)} successfully') 
            activity.save()
            return JsonResponse({'valid':True,'message':'Blog post updated successfully'},content_type="application/json")
        else:
            return JsonResponse({'valid':False,'uform_errors':uform.errors},content_type="application/json")

#deleteBlogPost
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def deleteBlogPost(request,id):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        try:
            obj=BlogModel.objects.get(id__exact=id)
            activity=ActivityModel.objects.create(icon='<i class="fa fa-trash"></i>',title='Delete Blog post',user_id=request.user.pk,name=f'Deleted blog post {obj.title}') 
            activity.save()
            obj.delete() 
            return JsonResponse({'deleted':False,'message':'Blog post deleted successfully.','id':id},content_type='application/json')       
        except BlogModel.DoesNotExist:
            return JsonResponse({'deleted':True,'message':'Item does not exist'},content_type='application/json')


#blogDetails
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def blogDetails(request,id):
    if SiteModel.objects.count() == 0:
        return redirect('/installation/')
    obj=SiteModel.objects.all()[0]
    activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
    contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
    contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
    messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
    count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
    blog=BlogModel.objects.get(id__exact=id)
    data=CommentModel.objects.filter(blog_id__exact=id).order_by("-id")
    paginator=Paginator(data,30)
    page_num=request.GET.get('page')
    comments=paginator.get_page(page_num)
    data={
        'title':'Blog Details',
        'obj':obj,
        'data':request.user,
        'activities':activities,
        'count':count,
        'comments':comments,
        'blog':blog,
        'mcount':paginator.count,
        'messages':messages,
        'room_name':request.user.username,
        'contact_messages':contact_messages,
        'contact_count':contact_count,

    }
    return render(request,'panel/blog_details.html',context=data)


#deleteComment
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def deleteComment(request,id):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        try:
            obj=CommentModel.objects.get(id__exact=id)
            activity=ActivityModel.objects.create(icon='<i class="fa fa-trash"></i>',title='Delete Comment',user_id=request.user.pk,name=f'Deleted comment beloging to {obj.name}') 
            activity.save()
            obj.delete() 
            return JsonResponse({'deleted':False,'message':'Comment deleted successfully.','id':id},content_type='application/json')       
        except CommentModel.DoesNotExist:
            return JsonResponse({'deleted':True,'message':'Item does not exist'},content_type='application/json')

#markComplete
@login_required(login_url='/accounts/login')
@api_view(['GET',])
def markComplete(request,id):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        try:
            obj=get_object_or_404(OrderModel,id__exact=id)
            activity=ActivityModel.objects.create(icon='<i class="fa fa-shopping-cart"></i>',title='Order Competed',user_id=request.user.pk,name=f'Marked order {obj.order_id} complete') 
            activity.save()
            updates=UpdatesModel.objects.create(user_id=request.user.pk,name=f'Order #{obj.order_id} marked complete')
            updates.save()
            obj.status='Completed'
            obj.is_completed=True
            obj.save()
            inprogress_writer_orders=OrderModel.objects.filter(writer_id=obj.writer_id,is_completed=False,is_accepted=True).count()
            completed_writer_orders=OrderModel.objects.filter(writer_id=obj.writer_id,is_completed=True).count()
            completed_customer_orders=OrderModel.objects.filter(user_id=request.user.pk,is_completed=True).count()
            user1=ExtendedAuthUser.objects.get(user_id=obj.writer_id)
            user2=ExtendedAuthUser.objects.get(user_id=obj.user_id)
            user1.inprogress_projects=inprogress_writer_orders
            user1.completed_projects=completed_writer_orders
            user2.completed_projects=completed_customer_orders
            user1.save()
            user2.save()
            return JsonResponse({'valid':True,'message':'Order completed successfully.','id':id},content_type='application/json')       
        except CommentModel.DoesNotExist:
            return JsonResponse({'valid':False,'message':'Item does not exist'},content_type='application/json')


#Topup
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['customer']),name='dispatch')
class Topup(View):
    def get(self,request):
        obj=check_data()
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        data=TransactionModel.objects.filter(user_id=request.user.pk).order_by("-id")
        paginator=Paginator(data,30)
        page_num=request.GET.get('page')
        transactions=paginator.get_page(page_num)
        messages=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).order_by("-id")
        messages_count=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).count()
        inner_qs=IndividualModel.objects.values_list('parent_id',flat=True).distinct()
        qs2=BidModel.objects.filter(user_id=request.user.pk).values_list('user_id',flat=True)
        if inner_qs:
            count1=BroadcastModel.objects.filter(group=True).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
            count2=BroadcastModel.objects.filter(to__icontains=request.user.username).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
        else:
            count1=BroadcastModel.objects.filter(group=True).count()
            count2=BroadcastModel.objects.filter(to__icontains=request.user.username).count()
        broadcasts_count=int(count1+count2)
        data={
            'title':'Account Topup',
            'obj':obj,
            'data':request.user,
            'activities':activities,
            'transactions':transactions,
            'mcount':paginator.count,
            'messages':messages,
            'room_name':request.user.username,
            'messages_count':messages_count,
            'broadcasts_count':broadcasts_count,
    
        }
        return render(request,'panel/topup.html',context=data)


#InboxMessages
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
def InboxMessages(request):
    obj=check_data()
    activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
    data=ContactModel.objects.filter(is_read=False).order_by("-id")
    paginator=Paginator(data,30)
    page_num=request.GET.get('page')
    messages=paginator.get_page(page_num)

    data1=ContactModel.objects.filter(is_read=True).order_by("-id")
    paginator1=Paginator(data1,30)
    page_num1=request.GET.get('page')
    read_messages=paginator1.get_page(page_num1)
    data={
        'title':'Inbox',
        'obj':obj,
        'data':request.user,
        'activities':activities,
        'contact_count':paginator.count,
        'contact_messages':messages,
        'room_name':request.user.username,
        'read_messages':read_messages,
        'read_messages_count':paginator1.count,

    }
    return render(request,'panel/inbox_messages.html',context=data)

#messageSingle
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
def messageSingle(request,id):
    obj=check_data()
    activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
    message=get_object_or_404(ContactModel,id__exact=id)
    messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:5]
    count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
    data={
        'title':'View Message',
        'obj':obj,
        'data':request.user,
        'activities':activities,
        'contact_count':count,
        'contact_messages':messages,
        'message':message,
        'room_name':request.user.username,

    }
    return render(request,'panel/message_single.html',context=data)

#myMessages
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['writer','customer'])
def myMessages(request):
    obj=check_data()
    activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
    contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
    contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
    messages=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).order_by("-id")
    messages_count=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).count()
    inner_qs=IndividualModel.objects.values_list('parent_id',flat=True).distinct()
    inner_qs1=IndividualModel.objects.filter(user_id=request.user.pk).values_list('user_id',flat=True)
    print('inner query:',inner_qs1)
    if inner_qs and inner_qs1:
        data=BroadcastModel.objects.filter(group=True).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=inner_qs1).order_by("-id")
        data1=BroadcastModel.objects.filter(group=False,to__icontains=request.user.username).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=inner_qs1).order_by("-id")
        count1=BroadcastModel.objects.filter(group=True).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=inner_qs1).count()
        data1=BroadcastModel.objects.filter(group=False,to__icontains=request.user.username).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=inner_qs1).order_by("-id")
        count2=BroadcastModel.objects.filter(group=False,to__icontains=request.user.username).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=inner_qs1).count()
        data3=IndividualModel.objects.filter(user=request.user.pk).order_by("-id")
        paginator3=Paginator(data3,30)
        page_num3=request.GET.get('page')
        read_messages=paginator3.get_page(page_num3)
        read_count=IndividualModel.objects.filter(user=request.user.pk).count()
    else:
        data=BroadcastModel.objects.filter(group=True).order_by("-id")
        data1=BroadcastModel.objects.filter(to__icontains=request.user.username).order_by("-id")
        count1=BroadcastModel.objects.filter(group=True).count()
        count2=BroadcastModel.objects.filter(to__icontains=request.user.username).count()
        read_messages=''
        read_count=''

    paginator=Paginator(data,30)
    page_num=request.GET.get('page')
    broadcasts=paginator.get_page(page_num)

    paginator1=Paginator(data1,30)
    page_num1=request.GET.get('page')
    individuals=paginator1.get_page(page_num1)
    broadcasts_count=int(count1+count2)

    data={
        'title':'My messages',
        'obj':obj,
        'data':request.user,
        'activities':activities,
        'messages_count':messages_count,
        'messages':messages,
        'read_messages':read_messages,
        'broadcasts':broadcasts,
        'room_name':request.user.username,
        'individuals':individuals,
        'public_count':count1,
        'private_count':count2,
        'read_count':read_count,
        'broadcasts_count':broadcasts_count,
        'contact_messages':contact_messages,
        'contact_count':contact_count,
    }
    return render(request,'panel/my_messages.html',context=data)

#moneyReceived
@login_required(login_url='/accounts/login')
@api_view(['POST',])
def moneyReceived(request):
    if request.method == 'POST' and request.headers.get('x-requested-with') == 'XMLHttpRequest':
        price=request.POST['price']
        tx_id=request.POST['transaction_id']
        tx_status=request.POST['transaction_status'].lower()
        obj=SiteModel.objects.all()[0]
        user=ExtendedAuthUser.objects.get(user_id=request.user.pk)
        balance=user.balance
        user.balance=float(balance)+int(price)
        user.save()
        activity=ActivityModel.objects.create(icon='<i class="fa fa-money"></i>',title='Money received',user_id=request.user.pk,name=f'Deposited funds to your account.Trasaction id {tx_id}.New balance is {obj.currency_symbol} {float(balance)+int(price)}') 
        activity.save()
        updates=UpdatesModel.objects.create(user_id=request.user.pk,name=f'{request.user.get_full_name()} deposited funds.')
        updates.save()
        transaction=TransactionModel.objects.create(transaction_id=tx_id,particulars=f'Funds deposited, transaction id: {tx_id}',status=tx_status,user_id=request.user.pk,credit=price,balance=float(balance)+int(price),reserved_money=request.user.extendedauthuser.reserved_money,mode='System')
        transaction.save()
        return JsonResponse({'valid':True,'message':'Payments made successfully.'},content_type='application/json')       
    else:
       return JsonResponse({'valid':False,'message':'Something went wrong while processing your payments.'},content_type='application/json')


#earnings
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['writer']),name='dispatch')
class Earnings(View):
    def get(self,request):
        obj=check_data()
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        form=WithdrawForm()

        data=WriterTransactionModel.objects.filter(user_id=request.user.pk).order_by("-id")
        paginator=Paginator(data,30)
        page_num=request.GET.get('page')
        transactions=paginator.get_page(page_num)

        data1=WithdrawRequestModel.objects.filter(user_id=request.user.pk).order_by("-id")
        paginator1=Paginator(data1,30)
        page_num1=request.GET.get('page')
        payments=paginator1.get_page(page_num1)
        messages=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).order_by("-id")
        messages_count=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).count()
        inner_qs=IndividualModel.objects.values_list('parent_id',flat=True).distinct()
        qs2=BidModel.objects.filter(user_id=request.user.pk).values_list('user_id',flat=True)
        if inner_qs:
            count1=BroadcastModel.objects.filter(group=True).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
            count2=BroadcastModel.objects.filter(to__icontains=request.user.username).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
        else:
            count1=BroadcastModel.objects.filter(group=True).count()
            count2=BroadcastModel.objects.filter(to__icontains=request.user.username).count()
        broadcasts_count=int(count1+count2)
        withdraw_period=request.user.extendedauthuser.withdraw_period
        if withdraw_period:
            today=datetime.datetime.now()
            days=(withdraw_period-today).days
        else:
            days=''
        data={
            'title':'My Payments',
            'obj':obj,
            'form':form,
            'activities':activities,
            'data':request.user,
            'transactions':transactions,
            'pcount':paginator1.count,
            'payments':payments,
            'mcount':paginator.count,
            'days':days,
            'messages':messages,
            'room_name':request.user.username,
            'messages_count':messages_count,
            'broadcasts_count':broadcasts_count,
    
        }
        return render(request,'panel/earnings.html',context=data)

    def post(self,request,*args ,**kwargs):
        uform=WithdrawForm(request.POST or None,instance=request.user.extendedauthuser)
        if uform.is_valid():
            obj=check_data()
            user=ExtendedAuthUser.objects.get(user_id=request.user.pk)
            user.balance=int(user.balance) - int(uform.cleaned_data.get("amount",None))
            saveobj=WithdrawRequestModel.objects.create(mode=uform.cleaned_data.get("mode",None),balance=user.balance,amount=uform.cleaned_data.get("amount",None),user_id=request.user.pk,email=request.user.email,name=request.user.get_full_name())
            saveobj.save()
            user.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-money"></i>',title='Earnings',user_id=request.user.pk,name=f'Requested funds withdraw amounting {obj.currency_symbol} {uform.cleaned_data.get("amount",None)}') 
            activity.save()
            updates=UpdatesModel.objects.create(user_id=request.user.pk,name=f'{request.user.get_full_name()} requested funds withdraw ')
            updates.save()
            return JsonResponse({'valid':True,'message':'Funds withdraw requested submitted successfuly awaiting approval.'},content_type="application/json")
        else:
            return JsonResponse({'valid':False,'uform_errors':uform.errors},content_type="application/json")


#approvePayments
@login_required(login_url='/accounts/login')
@api_view(['GET',])
@allowed_users(allowed_roles=['admins',])
def approvePayments(request,id):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        obj=check_data()
        transaction=get_object_or_404(WithdrawRequestModel,id__exact=id)
        transaction.user.extendedauthuser.balance=int(transaction.user.extendedauthuser.balance)-int(transaction.amount)
        transaction.status='Approved'
        transaction.is_accepted=True
        transaction.save()
        activity=ActivityModel.objects.create(icon='<i class="fa fa-money"></i>',title='Approve payment',user_id=request.user.pk,name=f'Approved withdraw request amounting {obj.currency_symbol} {transaction.amount} for user:{transaction.name}') 
        activity.save()
        updates=UpdatesModel.objects.create(user_id=request.user.pk,name=f'{request.user.get_full_name()} funds withdraw approved')
        updates.save()
        writer_last_order=WriterTransactionModel.objects.filter(user_id=transaction.user.id).last()
        transaction=WriterTransactionModel.objects.create(parent_id=writer_last_order.parent_id,particulars=f'Funds withdrawn from your account.',status='Completed',user_id=transaction.user_id,order_id=None,debit=transaction.amount,balance=transaction.user.extendedauthuser.balance,mode='System')
        transaction.save()
        return JsonResponse({'deleted':False,'message':'Payments approved successfully.'},content_type='application/json') 

#declinePayments      
@login_required(login_url='/accounts/login')
@api_view(['GET',])
@allowed_users(allowed_roles=['admins',])
def declinePayments(request,id):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        obj=SiteModel.objects.all()[0]
        transaction=get_object_or_404(WithdrawRequestModel,id_exact=id)
        transaction.status='Declined'
        transaction.is_declined=True
        transaction.save()
        activity=ActivityModel.objects.create(icon='<i class="fa fa-money"></i>',title='Payments declined',user_id=request.user.pk,name=f'Declined withdraw request amounting {obj.currency_symbol} {transaction.amount} for user:{transaction.name}') 
        activity.save()
        updates=UpdatesModel.objects.create(user_id=request.user.pk,name=f'{request.user.get_full_name()}  funds withdraw  declined')
        updates.save()
        return JsonResponse({'valid':True,'message':'Payments declined successfully.'},content_type='application/json')



#screenLock
@api_view(['GET',])
def screenLock(request,username):
    activity=ActivityModel.objects.create(icon='<i class="fa fa-key"></i>',title='Screen lock',user_id=request.user.pk,name='Perfomed screen lock') 
    activity.save()
    logout(request)
    return redirect(f'/unlock/screen/{username}')

#screenUnlock
@api_view(['GET',])
def screenUnlock(request,username):
    obj=check_data()
    user = get_object_or_404(User,username=username)
    data={
            'title':'Screen Unlock',
            'obj':obj,
            'data':user,
            'room_name':request.user.username,
        }
    return render(request,'panel/screen_unlock.html',context=data)  

#profilePic
@login_required(login_url='/accounts/login')
@api_view(['POST',])
def profilePic(request):
    if request.method == 'POST' and request.headers.get('x-requested-with') == 'XMLHttpRequest':
        profilePicform=ProfilePicForm(request.POST , request.FILES or None , instance=request.user.extendedauthuser)
        if profilePicform.has_changed():
            if profilePicform.is_valid():
                userme=profilePicform.save(commit=False)
                userme.user=request.user
                userme.save()
                activity=ActivityModel.objects.create(icon='<i class="fa fa-picture"></i>',title='Profile picture', user_id=request.user.pk,name='Changed profile picture') 
                activity.save()
                return JsonResponse({'valid':True,'message':'Profile picture updated.'},status=200,safe=False)
            else:
                return JsonResponse({'valid':False,'uform_errors':profilePicform.errors},status=200)    
        return JsonResponse({'valid':False,'error':'No changes made'},content_type='application/json')


#action
@login_required(login_url='/accounts/login')
@api_view(['POST',])
@allowed_users(allowed_roles=['admins',])
def action(request):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        action=request.POST['action']
        if 'true' in action:
            user=ExtendedAuthUser.objects.get(user_id=request.user.pk)
            user.is_deactivated=True
            user.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-lock"></i>',title='Account activate',user_id=request.user.pk,name='Deactivated account') 
            activity.save()
            return JsonResponse({'valid':True,'message':'Account deactivated successfully.'},status=200,safe=False)
        else:
            user=ExtendedAuthUser.objects.get(user_id=request.user.pk)
            user.is_deactivated=False
            user.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-lock"></i>',title='Account activate',user_id=request.user.pk,name='Activated the account') 
            activity.save()
            return JsonResponse({'valid':True,'message':'Account activated successfully.'},status=200,safe=False)


#social
@login_required(login_url='/accounts/login')
@api_view(['POST',])
def edit_social_link(request):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        form=UserSocialForm(request.POST or None , instance=request.user.extendedauthuser)
        if form.has_changed():
            if form.is_valid():
                link=form.save(commit=False)
                link.facebook=request.POST['facebook']
                link.twitter=request.POST['twitter']
                link.instagram=request.POST['instagram']
                link.linkedin=request.POST['linkedin']
                link.save()
                activity=ActivityModel.objects.create(icon='<i class="fa fa-link"></i>',title='Social link',user_id=request.user.pk,name='Edited social links') 
                activity.save()
                return JsonResponse({'valid':True,'message':'Social link(s) updated.'},status=200,safe=False)
            else:
                return JsonResponse({'valid':False,'uform_errors':form.errors},status=200)    
        return JsonResponse({'valid':False,'error':'No changes made'},content_type='application/json')

#passwordChange
@login_required(login_url='/accounts/login')
@api_view(['POST',])
def passwordChange(request):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        passform=UserPasswordChangeForm(request.POST or None,instance=request.user)
        if passform.is_valid():
            user=User.objects.get(username__exact=request.user.username)
            user.password=make_password(passform.cleaned_data.get('password1'))
            user.save()
            activity=ActivityModel.objects.create(icon='<i class="fa fa-lock"></i>',title='Password changed',user_id=request.user.pk,name='Changed password') 
            activity.save()
            update_session_auth_hash(request,request.user)
            return JsonResponse({'valid':True,'message':'Password changed successfully.'},content_type='application/json')
        else:
            return JsonResponse({'valid':False,'uform_errors':passform.errors},content_type='application/json')

#messages
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def messages(request):
    obj=check_data()
    count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
    data=ContactModel.objects.filter(is_read=False).all().order_by("-id")
    paginator=Paginator(data,30)
    activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
    contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
    contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
    page_num=request.GET.get('page')
    messages=paginator.get_page(page_num)
    data={
        'title':'Messages',
        'obj':obj,
        'activities':activities,
        'data':request.user,
        'messages':messages,
        'count':count,
        'room_name':request.user.username,
        'contact_messages':contact_messages,
        'contact_count':contact_count,

    }
    return render(request,'panel/messages.html',context=data)
       



#ViewMessage
@method_decorator(login_required(login_url='/panel/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins']),name='dispatch')
class ViewMessage(View):
    def get(self,request,id):
        obj=check_data()
        message=get_object_or_404(ContactModel,id=id)
        form=UsersReplyForm(instance=message)
        message_count=ContactModel.objects.filter(reply__isnull=True).count()
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        data={
            'title':f'Viewing {message.name} message',
            'obj':obj,
            'data':request.user,
            'form':form,
            'message':message,
            'message_count':message_count,
            'room_name':request.user.username,
            'activities':activities,
            'contact_messages':contact_messages,
            'contact_count':contact_count,
    
        }
        return render(request,'panel/view_message.html',context=data)
    def post(self,request,id):
        if request.method == 'POST' and request.headers.get('x-requested-with') == 'XMLHttpRequest':
            obj=SiteConstants.objects.all()[0]
            message=get_object_or_404(ContactModel,id=id)
            form=UsersReplyForm(request.POST or None , instance=message)
            email=request.POST['email']
            subject=request.POST['subject']
            if form.is_valid():
                t=form.save(commit=False)
                t.isread=True
                t.save()
                message={
                        'user':email.split('@')[0],
                        'site_name':obj.site_name,
                        'site_url':obj.site_url,
                        'message':form.cleaned_data.get('reply')
                    }
                template='emails/reply.html'
                send_email(subject,email,message,template)
                activity=ActivityModel.objects.create(icon='<i class="fa fa-envelope"></i>',title='Replied message',user_id=request.user.pk,name='Replied email') 
                activity.save()
                return JsonResponse({'valid':True,'message':'Message replied successfully'},content_type="application/json")
            else:
                return JsonResponse({'valid':False,'uform_errors':form.errors},content_type="application/json")



#deleteMessage
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
def deleteMessage(request,id):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        try:
            obj=ContactModel.objects.get(id__exact=id)
            activity=ActivityModel.objects.create(icon='<i class="fa fa-trash"></i>',title='Delete message',user_id=request.user.pk,name=f'Deleted message {obj.message}') 
            activity.save()
            obj.delete()
            return JsonResponse({'deleted':False,'message':'Message deleted successfully.','id':id},content_type='application/json')       
        except ContactModel.DoesNotExist:
            return JsonResponse({'deleted':True,'message':'Item does not exist'},content_type='application/json')

#browseOrders
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['writer'])
@api_view(['GET',])
def browseOrders(request):
    obj=check_data()
    if request.user.extendedauthuser.is_verified:
        data4=OrderModel.objects.filter(inviter_id=request.user.pk,is_cancelled=False,is_completed=False,is_public=False,is_accepted=False).order_by("-id")
        paginator4=Paginator(data4,30)
        page_num4=request.GET.get('page')
        private_orders=paginator4.get_page(page_num4)
        private_orders_count=paginator4.count
        inner_qs=BidModel.objects.values_list('parent_id',flat=True).distinct()
        inner_qs1=BidModel.objects.filter(user_id=request.user.pk).values_list('user_id',flat=True)
        if inner_qs:
            data=OrderModel.objects.select_related('user').filter(is_cancelled=False,is_completed=False,is_public=True,is_accepted=False).exclude(bidmodel__id__in=inner_qs).exclude(bidmodel__user_id__in=inner_qs1).order_by("-id")
            paginator=Paginator(data,30)
            page_num=request.GET.get('page')
            public_orders=paginator.get_page(page_num)
        else:
            data=OrderModel.objects.select_related('user').filter(is_cancelled=False,is_completed=False,is_public=True,is_accepted=False).order_by("-id")
            paginator=Paginator(data,30)
            page_num=request.GET.get('page')
            public_orders=paginator.get_page(page_num)
        mcount=paginator.count
    else:
        public_orders,private_orders,published,private_orders_count,mcount=["" for i in range(5)]

    messages=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).order_by("-id")
    messages_count=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).count()
    inner_qs=IndividualModel.objects.values_list('parent_id',flat=True).distinct()
    qs2=BidModel.objects.filter(user_id=request.user.pk).values_list('user_id',flat=True)
    if inner_qs:
        count1=BroadcastModel.objects.filter(group=True).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
        count2=BroadcastModel.objects.filter(to__icontains=request.user.username).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
    else:
        count1=BroadcastModel.objects.filter(group=True).count()
        count2=BroadcastModel.objects.filter(to__icontains=request.user.username).count()
    broadcasts_count=int(count1+count2)
    activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
    data={
        'title':'Browse Orders',
        'obj':obj,
        'data':request.user,
        'private_orders_count':private_orders_count,
        'private_orders':private_orders,
        'mcount':mcount,
        'public_orders':public_orders,
        'room_name':request.user.username,
        'activities':activities,
        'messages':messages,
        'messages_count':messages_count,
        'broadcasts_count':broadcasts_count,

    }
    return render(request,'panel/browse_orders.html',context=data)
       

#myOrders
@login_required(login_url='/accounts/login')
@api_view(['GET',])
@allowed_users(allowed_roles=['admins','customer',])
def myOrders(request):
    obj=check_data()
    activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
    contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
    contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()

    data=OrderModel.objects.filter(user_id=request.user.pk,is_completed=False,is_accepted=False,is_cancelled=False).order_by("-id")
    paginator=Paginator(data,30)
    page_num=request.GET.get('page')
    orders=paginator.get_page(page_num)

    data1=OrderModel.objects.filter(user_id=request.user.pk,is_completed=False,is_cancelled=True).order_by("-id")
    paginator1=Paginator(data1,30)
    page_num1=request.GET.get('page')
    cancelled_orders=paginator1.get_page(page_num1)

    data2=OrderModel.objects.filter(user_id=request.user.pk,is_completed=False,is_cancelled=False,inviter__isnull=False,is_public=False).order_by("-id")
    paginator2=Paginator(data2,30)
    page_num2=request.GET.get('page')
    invites=paginator2.get_page(page_num2)

    data3=OrderModel.objects.filter(user_id=request.user.pk,is_cancelled=False,is_accepted=True,inprogress=True,is_answered=False,verdict__isnull=True).order_by("-id")
    paginator3=Paginator(data3,30)
    page_num3=request.GET.get('page')
    inprogress_orders=paginator3.get_page(page_num3)

    data5=OrderModel.objects.filter(user_id=request.user.pk,is_completed=True,is_cancelled=False).order_by("-id")
    paginator5=Paginator(data5,30)
    page_num5=request.GET.get('page')
    completed_orders=paginator5.get_page(page_num5)

    data6=ExamModel.objects.filter(user_id=request.user.pk).order_by("-id")
    paginator6=Paginator(data6,30)
    page_num6=request.GET.get('page')
    exams=paginator6.get_page(page_num6) 

    data7=OrderModel.objects.filter(user_id=request.user.pk,is_cancelled=False,is_accepted=True,is_completed=False,verdict__icontains='dispute').order_by("-id")
    paginator7=Paginator(data7,30)
    page_num7=request.GET.get('page')
    disputes=paginator7.get_page(page_num7) 

    data8=OrderModel.objects.filter(user_id=request.user.pk,is_cancelled=False,is_accepted=True,is_completed=False,verdict__icontains='revision').order_by("-id")
    paginator8=Paginator(data8,30)
    page_num8=request.GET.get('page')
    revisions=paginator8.get_page(page_num8)


    data9=OrderModel.objects.filter(verdict__isnull=True,user_id=request.user.pk,is_answered=True,inprogress=True,).order_by("-id")
    paginator9=Paginator(data9,30)
    page_num9=request.GET.get('page')
    answered_orders=paginator9.get_page(page_num9)

    messages=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).order_by("-id")
    messages_count=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).count()
    inner_qs=IndividualModel.objects.values_list('parent_id',flat=True).distinct()
    qs2=BidModel.objects.filter(user_id=request.user.pk).values_list('user_id',flat=True)
    if inner_qs:
        count1=BroadcastModel.objects.filter(group=True).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
        count2=BroadcastModel.objects.filter(to__icontains=request.user.username).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
    else:
        count1=BroadcastModel.objects.filter(group=True).count()
        count2=BroadcastModel.objects.filter(to__icontains=request.user.username).count()
    broadcasts_count=int(count1+count2)
    data={
        'title':'My Orders',
        'obj':obj,
        'data':request.user,
        'orders':orders,
        'exams':exams,
        'activities':activities,
        'invites':invites,
        'completed_orders':completed_orders,
        'cancelled_orders':cancelled_orders,
        'inprogress_orders':inprogress_orders,
        'answered_orders':answered_orders,
        'answered_orders_count':paginator9.count,
        'ocount':paginator.count,
        'ccount':paginator1.count,
        'icount':paginator2.count,
        'incount':paginator3.count,
        'compcount':paginator5.count,
        'ecount':paginator6.count,
        'disputes':disputes,
        'revisions':revisions,
        'dcount':paginator7.count,
        'rcount':paginator8.count,
        'room_name':request.user.username,
        'messages':messages,
        'messages_count':messages_count,
        'broadcasts_count':broadcasts_count,
        'contact_messages':contact_messages,
        'contact_count':contact_count,

    }
    return render(request,'panel/my_orders.html',context=data)

#myOrdersWriter
@login_required(login_url='/panel')
@api_view(['GET',])
@allowed_users(allowed_roles=['admins','writer',])
def myOrdersWriter(request):
    obj=check_data()
    activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
    contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
    contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()

    data=BidModel.objects.select_related('parent').filter(user_id=request.user.pk,parent__is_accepted=False,is_accepted=False,parent__is_completed=False,is_completed=False,parent__is_public=True,parent__is_cancelled=False,has_bid=True).order_by("-id")
    paginator=Paginator(data,30)
    page_num=request.GET.get('page')
    orders=paginator.get_page(page_num)

    data1=BidModel.objects.select_related('parent').filter(parent__is_completed=False,parent__is_cancelled=True,user_id=request.user.pk).order_by("-id")
    paginator1=Paginator(data1,30)
    page_num1=request.GET.get('page')
    cancelled_orders=paginator1.get_page(page_num1)

    data2=BidModel.objects.select_related('parent').filter(user_id=request.user.pk,parent__is_cancelled=False,parent__inviter__isnull=False).order_by("-id")
    paginator2=Paginator(data2,30)
    page_num2=request.GET.get('page')
    invites=paginator2.get_page(page_num1)

    data3=BidModel.objects.select_related('parent').filter(parent__is_answered=False,parent__verdict__isnull=True, user_id=request.user.pk,parent__is_completed=False,parent__is_cancelled=False,is_accepted=True).order_by("-id")
    paginator3=Paginator(data3,30)
    page_num3=request.GET.get('page')
    inprogress_orders=paginator3.get_page(page_num3)

    data4=BidModel.objects.select_related('parent').filter(has_bid=True,parent__inviter_id=request.user.pk,parent__is_cancelled=False,is_completed=False,parent__is_public=False).order_by("-id")
    paginator4=Paginator(data4,30)
    page_num4=request.GET.get('page')
    private_orders=paginator4.get_page(page_num4)

    data5=BidModel.objects.select_related('parent').filter(user_id=request.user.pk,parent__is_accepted=False,is_accepted=False,parent__is_completed=False,is_completed=False,parent__is_cancelled=False,has_bid=True).order_by("-id")
    paginator5=Paginator(data5,30)
    page_num5=request.GET.get('page')
    all_writers_orders=paginator5.get_page(page_num5)

    data6=BidModel.objects.select_related('parent').filter(user_id=request.user.pk,parent__is_completed=True,parent__is_cancelled=False).order_by("-id")
    paginator6=Paginator(data6,30)
    page_num6=request.GET.get('page')
    completed_orders=paginator6.get_page(page_num6)

    data7=BidModel.objects.select_related('parent').filter(user_id=request.user.pk,parent__is_completed=False,parent__is_cancelled=False,is_accepted=True,parent__verdict__icontains='dispute').order_by("-id")
    paginator7=Paginator(data7,30)
    page_num7=request.GET.get('page')
    disputes=paginator7.get_page(page_num7) 

    data8=BidModel.objects.select_related('parent').filter(user_id=request.user.pk,parent__is_completed=False,parent__is_cancelled=False,is_accepted=True,parent__verdict__icontains='revision').order_by("-id")
    paginator8=Paginator(data8,30)
    page_num8=request.GET.get('page')
    revisions=paginator8.get_page(page_num8)

    data9=BidModel.objects.select_related('parent').filter(parent__verdict__isnull=True,parent__is_answered=True,parent__is_completed=False,user_id=request.user.pk,parent__is_cancelled=False).order_by("-id")
    paginator9=Paginator(data9,30)
    page_num9=request.GET.get('page')
    submitted_orders=paginator9.get_page(page_num9)

    messages=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).order_by("-id")
    messages_count=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).count()
    inner_qs=IndividualModel.objects.values_list('parent_id',flat=True).distinct()
    qs2=BidModel.objects.filter(user_id=request.user.pk).values_list('user_id',flat=True)
    if inner_qs:
        count1=BroadcastModel.objects.filter(group=True).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
        count2=BroadcastModel.objects.filter(to__icontains=request.user.username).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
    else:
        count1=BroadcastModel.objects.filter(group=True).count()
        count2=BroadcastModel.objects.filter(to__icontains=request.user.username).count()
    broadcasts_count=int(count1+count2)
    data={
        'title':'My Orders',
        'obj':obj,
        'data':request.user,
        'orders':orders,
        'all_writers_orders':all_writers_orders,
        'all_writers_orders_count':paginator5.count,
        'invites':invites,
        'private_orders':private_orders,
        'private_orders_count':paginator4.count,
        'cancelled_orders':cancelled_orders,
        'inprogress_orders':inprogress_orders,
        'disputes':disputes,
        'submitted_orders':submitted_orders,
        'answered_orders_count':paginator9.count,
        'revisions':revisions,
        'ocount':paginator.count,
        'ccount':paginator1.count,
        'icount':paginator2.count,
        'incount':paginator3.count,
        'compcount':paginator6.count,
        'dcount':paginator7.count,
        'rcount':paginator8.count,
        'completed_orders':completed_orders,
        'room_name':request.user.username,
        'activities':activities,
        'messages':messages,
        'messages_count':messages_count,
        'broadcasts_count':broadcasts_count,
        'contact_messages':contact_messages,
        'contact_count':contact_count,

    }
    return render(request,'panel/writer_orders.html',context=data)


#editExamOrder
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['admins','customer',]),name="dispatch")
class editExamOrder(View):
    def get(self ,request,id):
        obj=check_data()
        order = get_object_or_404(ExamModel,id=id)
        form1=ExamForm(instance=order)
        styles=CitationModel.objects.all().order_by('-id')
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        messages=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).order_by("-id")
        messages_count=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).count()
        inner_qs=IndividualModel.objects.values_list('parent_id',flat=True).distinct()
        qs2=BidModel.objects.filter(user_id=request.user.pk).values_list('user_id',flat=True)
        if inner_qs:
            count1=BroadcastModel.objects.filter(group=True).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
            count2=BroadcastModel.objects.filter(to__icontains=request.user.username).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
        else:
            count1=BroadcastModel.objects.filter(group=True).count()
            count2=BroadcastModel.objects.filter(to__icontains=request.user.username).count()
        broadcasts_count=int(count1+count2)
        data={
            'title':f'Edit order:#{order.order_id}',
            'obj':obj,
            'data':request.user,
            'form1':form1,
            'id':id,
            'order':order,
            'activities':activities,
            'styles':styles,
            'edit':True,
            'exam':True,
            'room_name':request.user.username,
            'messages':messages,
            'messages_count':messages_count,
            'broadcasts_count':broadcasts_count,
            'contact_messages':contact_messages,
            'contact_count':contact_count,
    
        }
        return render(request,'panel/new_order.html',context=data)

    def post(self,request,id,*args ,**kwargs):
        order=get_object_or_404(OrderModel,id=id)
        uform=OrderForm(request.POST,request.FILES or None,instance=order)
        if uform.is_valid():
            inviter=form.cleaned_data.get('inviter',None)
            files=request.FILES.getlist('additional_materials')
            d=uform.save(commit=False)
            if inviter:
                d.is_public=False
            else:
                d.is_public=True
            d.save()
            for file in files:
               ExamMediaModel.objects.create(user_id=request.user.pk,parent_id=id,additional_materials=file)
            activity=ActivityModel.objects.create(icon='<i class="fa fa-shopping-cart"></i>',title='Order update',user_id=request.user.pk,name=f'Order #{order.order_id} updated successfully') 
            activity.save()
            updates=UpdatesModel.objects.create(user_id=request.user.pk,name=f'Order #{order.order_id} updated successfully')
            updates.save()
            return JsonResponse({'valid':True,'message':f'Order #{order.order_id} updated successfully'},content_type="application/json")
        else:
            return JsonResponse({'valid':False,'uform_errors':uform.errors},content_type="application/json")

#deleteExam
@login_required(login_url='/accounts/login')
@api_view(['GET',])
@allowed_users(allowed_roles=['customer',])
def deleteExam(request,id):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        try:
            obj=ExamModel.objects.get(id__exact=id)
            activity=ActivityModel.objects.create(icon='<i class="fa fa-money"></i>',title='Exam/Quiz update',user_id=request.user.pk,name='Deleted exam/quiz item') 
            activity.save()
            obj.delete() 
            return JsonResponse({'deleted':False,'message':'Quiz deleted successfully.','id':id},content_type='application/json')       
        except PaymentModel.DoesNotExist:
            return JsonResponse({'deleted':True,'message':'Item does not exist'},content_type='application/json')


#chat
@login_required(login_url='/accounts/login')
@api_view(['POST',])
@allowed_users(allowed_roles=['customer','writer'])
def chat(request,order_id,user_id):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest' and request.method == 'POST':
        order=BidModel.objects.filter(order_id__exact=order_id,user_id=user_id).last()
        site=check_data()
        form=ChatForm(request.POST or None)
        if form.is_valid():
            obj=form.save(commit=False)
            obj.sender=request.user
            if request.user.extendedauthuser.category == 'writer':
                obj.receiver=order.parent.user_id
                obj.bid_id=order.id
            else:
                obj.receiver=order.user_id
                obj.bid_id=order.id
            if ChatModel.objects.filter(id__isnull=False):
                chat=ChatModel.objects.latest('id')
                if chat.receiver ==request.user.pk:
                    updates=ChatModel.objects.filter(parent_id=order.parent.id,is_read=False)
                    for item in updates:
                        item.is_read=True  
                        item.save()
            obj.parent_id=order.parent.id
            obj.save()
            messages_count=ChatModel.objects.filter(parent_id=order.parent.id,is_read=False).count()
            osave=OrderModel.objects.get(id__exact=order.parent.id)
            osave.messages=messages_count
            osave.save()
            insert_time=ChatModel.objects.latest('created_on').created_on
            subject=f'You have {messages_count} new unread messages on order #{order.parent.order_id}'
            email=order.user.email
            message={
                        'user':order.user,
                        'site_name':site.site_name,
                        'site_url':site.site_url,
                        'address':site.address,
                        'location':site.location,
                        'description':site.description,
                        'phone':site.phone,
                        'order_id':order.parent.order_id,
                        'message_count':messages_count,
                     } 
            template='emails/messages.html'
            send_email(subject,email,message,template)
            response={
                        'chat':True,
                        'valid':True,
                        'new_message':'Message sent!',
                        'submitted_message':form.cleaned_data.get('message',None),
                        'time': timeago.format(insert_time, datetime.datetime.now()),
                        'profiler':request.user.extendedauthuser.profile_pic.url,
                        'name':request.user.get_full_name(),
                    }
            return JsonResponse(response,content_type='application/json')
        return JsonResponse({'valid':False,'uform_errors':form.errors},content_type='application/json')




#writerReviews
@api_view(['GET',])
def writerReviews(request,id):
    obj=check_data()
    user=get_object_or_404(User,id__exact=id)
    data=BidModel.objects.filter(user_id=id).order_by("-id")
    paginator=Paginator(data,30)
    page_num=request.GET.get('page')
    comments=paginator.get_page(page_num)
    activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
    contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
    contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
    messages=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).order_by("-id")
    messages_count=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).count()
    inner_qs=IndividualModel.objects.values_list('parent_id',flat=True).distinct()
    qs2=BidModel.objects.filter(user_id=request.user.pk).values_list('user_id',flat=True)
    if inner_qs:
        count1=BroadcastModel.objects.filter(group=True).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
        count2=BroadcastModel.objects.filter(to__icontains=request.user.username).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
    else:
        count1=BroadcastModel.objects.filter(group=True).count()
        count2=BroadcastModel.objects.filter(to__icontains=request.user.username).count()
    broadcasts_count=int(count1+count2)
    data={
        'title':'Writer reviews',
        'obj':obj,
        'data':request.user,
        'user':user,
        'comments':comments,
        'comments_count':paginator.count,
        'room_name':request.user.username,
        'activities':activities,
        'messages':messages,
        'messages_count':messages_count,
        'broadcasts_count':broadcasts_count,
        'contact_messages':contact_messages,
        'contact_count':contact_count,

    }
    return render(request,'panel/submitted_reviews.html',context=data)


#clientReviews
@api_view(['GET',])
def clientReviews(request,id):
    obj=check_data()
    user=get_object_or_404(User,id__exact=id)
    data=OrderModel.objects.filter(user_id=id).order_by("-id")
    paginator=Paginator(data,30)
    page_num=request.GET.get('page')
    comments=paginator.get_page(page_num)
    activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
    contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
    contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
    messages=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).order_by("-id")
    messages_count=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).count()
    inner_qs=IndividualModel.objects.values_list('parent_id',flat=True).distinct()
    qs2=BidModel.objects.filter(user_id=request.user.pk).values_list('user_id',flat=True)
    if inner_qs:
        count1=BroadcastModel.objects.filter(group=True).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
        count2=BroadcastModel.objects.filter(to__icontains=request.user.username).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
    else:
        count1=BroadcastModel.objects.filter(group=True).count()
        count2=BroadcastModel.objects.filter(to__icontains=request.user.username).count()
    broadcasts_count=int(count1+count2)
    data={
        'title':'Client reviews',
        'obj':obj,
        'data':request.user,
        'user':user,
        'comments':comments,
        'comments_count':paginator.count,
        'room_name':request.user.username,
        'activities':activities,
        'messages':messages,
        'messages_count':messages_count,
        'broadcasts_count':broadcasts_count,
        'contact_messages':contact_messages,
        'contact_count':contact_count,

    }
    return render(request,'panel/submitted_reviews.html',context=data)


#allWriterReviews
@api_view(['GET',])
def allWriterReviews(request):
    obj=check_data()
    data=BidModel.objects.all().values('comment','created_on').order_by("-id")
    paginator=Paginator(data,30)
    page_num=request.GET.get('page')
    comments=paginator.get_page(page_num)
    activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
    contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
    contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
    messages=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).order_by("-id")
    messages_count=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).count()
    inner_qs=IndividualModel.objects.values_list('parent_id',flat=True).distinct()
    qs2=BidModel.objects.filter(user_id=request.user.pk).values_list('user_id',flat=True)
    if inner_qs:
        count1=BroadcastModel.objects.filter(group=True).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
        count2=BroadcastModel.objects.filter(to__icontains=request.user.username).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
    else:
        count1=BroadcastModel.objects.filter(group=True).count()
        count2=BroadcastModel.objects.filter(to__icontains=request.user.username).count()
    broadcasts_count=int(count1+count2)
    data={
        'title':'Writer reviews',
        'obj':obj,
        'data':request.user,
        'comments':comments,
        'comments_count':paginator.count,
        'room_name':request.user.username,
        'activities':activities,
        'messages':messages,
        'messages_count':messages_count,
        'broadcasts_count':broadcasts_count,
        'contact_messages':contact_messages,
        'contact_count':contact_count,

    }
    return render(request,'panel/all_writer_reviews.html',context=data)


#orderConversation
@method_decorator(login_required(login_url='/accounts/login'),name='dispatch')
@method_decorator(allowed_users(allowed_roles=['writer','admins']),name='dispatch')
class orderConversation(View):
    def get(self,request,id):
        obj=check_data()
        bid=BidModel.objects.filter(parent_id__exact=id).last()
        chats=ChatModel.objects.select_related('sender').filter(parent_id=id).order_by("id")
        messages=ChatModel.objects.filter(receiver=request.user.pk,is_read=False).order_by("id")
        messages_count=ChatModel.objects.select_related('sender').filter(parent_id=id,is_read=False).count()
        activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
        contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
        contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
        form1=ChatForm()
        inner_qs=IndividualModel.objects.values_list('parent_id',flat=True).distinct()
        qs2=BidModel.objects.filter(user_id=request.user.pk).values_list('user_id',flat=True)
        if inner_qs:
            count1=BroadcastModel.objects.filter(group=True).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
            count2=BroadcastModel.objects.filter(to__icontains=request.user.username).exclude(individualmodel__id__in=inner_qs).exclude(individualmodel__user_id__in=qs2).count()
        else:
            count1=BroadcastModel.objects.filter(group=True).count()
            count2=BroadcastModel.objects.filter(to__icontains=request.user.username).count()
        broadcasts_count=int(count1+count2)
        data={
            'title':f'Order Conversation on order #{bid.parent.order_id}',
            'obj':obj,
            'data':request.user,
            'room_name':request.user.username,
            'activities':activities,
            'messages':messages,
            'bid':bid,
            'form1':form1,
            'messages':messages,
            'chats':chats,
            'messages_count':messages_count,
            'broadcasts_count':broadcasts_count,
            'contact_messages':contact_messages,
            'contact_count':contact_count,

        }
        return render(request,'panel/conversation.html',context=data)
    def post(self,request,id,*args ,**kwargs):
        bid=get_object_or_404(BidModel,parent_id__exact=id)
        uform=BidForm(request.POST or None,instance=bid)
        if uform.is_valid():
            if not bid.parent.is_accepted:
                uform.save()
                activity=ActivityModel.objects.create(icon='<i class="fa fa-hand-pointer-o"></i>',title='Edit Bid ',user_id=request.user.pk,name=f'Edited bid on order #{bid.parent.order_id}') 
                activity.save()
                updates=UpdatesModel.objects.create(user_id=request.user.pk,name=f'Updated bid on order #{bid.parent.order_id} ')
                updates.save()
                return JsonResponse({'valid':True,'message':f'Edited bid order #{bid.parent.order_id} made successfully'},content_type="application/json")
            else:
                return JsonResponse({'deny':True,'valid':False,'message':'You cannot edit  bid on order accepted'},content_type="application/json")   
        else:
            return JsonResponse({'valid':False,'uform_errors':uform.errors},content_type="application/json")


#disableChat
@login_required(login_url='/accounts/login')
@api_view(['GET',])
@allowed_users(allowed_roles=['admins',])
def disableChat(request,id):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        order=get_object_or_404(OrderModel,id=id)
        order.is_disabled=True
        order.save()
        activity=ActivityModel.objects.create(icon='<i class="ti-comments"></i>',title='Chat disable',user_id=request.user.pk,name=f'Disabled chat of order #{order.order_id} successfuly') 
        activity.save()
        return JsonResponse({'deleted':True,'message':'Chat disabled successfuly.'},content_type='application/json')


#broadcasts
@api_view(['GET',])
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
def broadcasts(request):
    obj=check_data()
    data=BroadcastModel.objects.all().order_by("-id")
    paginator=Paginator(data,30)
    page_num=request.GET.get('page')
    broadcasts=paginator.get_page(page_num)

    data1=BroadcastModel.objects.filter(group=False,to__isnull=False).order_by("-id")
    paginator1=Paginator(data1,30)
    page_num1=request.GET.get('page')
    individuals=paginator1.get_page(page_num1)
    activities=ActivityModel.objects.filter(user_id=request.user.pk).order_by("-id")[:5]
    contact_messages=ContactModel.objects.filter(is_read=False).order_by("-id")[:10]
    contact_count=ContactModel.objects.filter(is_read=False).order_by("-id").count()
    data={
        'title':'Broadcasted messages',
        'obj':obj,
        'data':request.user,
        'room_name':request.user.username,
        'activities':activities,
        'broadcasts':broadcasts,
        'individuals':individuals,
        'individuals_count':paginator1.count,
        'broadcasts_count':paginator.count,
        'contact_messages':contact_messages,
        'contact_count':contact_count,
        'contact_messages':contact_messages,
        'contact_count':contact_count,

    }
    return render(request,'panel/broadcasts.html',context=data)

#individialMessage
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['POST',])
def individialMessage(request):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest' and request.method == 'POST':
        form=IndividualForm(request.POST or None)
        if form.is_valid():
            usr=form.save(commit=False)
            usr.user=request.user
            usr.save()
            to=form.cleaned_data.get('to',None)
            # channel_layer=get_channel_layer()
            # if to:
            #     async_to_sync(channel_layer.group_send)(
            #         "notification_%s" %to,
            #         {
            #             'type': 'send_notification',
            #             'activity':json.dumps({'category':'admin','name':request.user.get_full_name(),'profile':request.user.extendedauthuser.profile_pic.url,'message':True,'title':'New message','icon':'<i class="ti-comments"></i>','activity':form.cleaned_data.get('message',None),'time':'just now'})
            #         }
            #     )
            activity=ActivityModel.objects.create(icon='<i class="fa fa-cog"></i>',title='Broadcast Message',user_id=request.user.pk,name=f'Added new broadcast message to {to}') 
            activity.save()
            return JsonResponse({'valid':True,'message':'Message sent successfully'},content_type='application/json')
        return JsonResponse({'valid':False,'uform_errors':form.errors},content_type='application/json')



#markChatIsRead
@login_required(login_url='/accounts/login')
@api_view(['GET',])
def markChatIsRead(request):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        chats=ChatModel.objects.filter(receiver=request.user.pk,is_read=False)
        for item in chats:
            item.is_read=True
            item.save()
        return JsonResponse({'deleted':True,'message':'Chats marked read'},content_type='application/json')

#markInboxMessageIsRead
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['admins'])
@api_view(['GET',])
def markInboxMessageIsRead(request,id):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        message=get_object_or_404(ContactModel,id=id)
        message.is_read=True
        message.save()
        return JsonResponse({'deleted':True,'message':'Message marked read'},content_type='application/json')

#markMessageIsRead
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['writer','customer'])
@api_view(['GET',])
def markMessageIsRead(request,id):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        message=get_object_or_404(BroadcastModel,id=id)
        if IndividualModel.objects.filter(parent_id=id,user_id=request.user.pk).exists():
            obj=IndividualModel.objects.get(parent_id__exact=id,user_id=request.user.pk)
            obj.user=request.user
            obj.message=message.message
            obj.is_read=True
            obj.parent_id=id
            obj.save()
        else:
            IndividualModel.objects.create(user=request.user,parent_id=id,is_read=True,message=message.message)
        return JsonResponse({'deleted':True,'message':'Message marked read'},content_type='application/json')

#deleteReadMessage
@login_required(login_url='/accounts/login')
@allowed_users(allowed_roles=['writer','customer'])
@api_view(['GET',])
def deleteReadMessage(request,id):
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        message=get_object_or_404(IndividualModel,id=id)
        message.delete()
        return JsonResponse({'deleted':True,'message':'Message  deleted'},content_type='application/json')