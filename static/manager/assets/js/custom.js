const cookieContainer=document.querySelector('.cookie-container');
const cookieButton=document.querySelector('.cookieBtn');
cookieButton.addEventListener('click',()=>{
    cookieContainer.classList.remove('active');
    localStorage.setItem('cookieSet',true);
});
setTimeout(()=>{
    if(!localStorage.getItem('cookieSet'))
    {
        cookieContainer.classList.add('active');
    }
},5000);
/*proloader*/
function load()
{
  document.querySelector('.placeholder').style.display="none";
  document.querySelector('.main-display').style.display="block";
}

/*insection observer API */
function observerImages()
{
    var images=document.querySelectorAll('[data-src]'),
    imgOpts={},
    observer=new IntersectionObserver((entries,observer)=>
    {
        entries.forEach((entry)=>
        {
            if(!entry.isIntersecting) return;
            const img=entry.target;
            const newUrl=img.getAttribute('data-src');
            img.src=newUrl;
            observer.unobserve(img);
        });
    },imgOpts);
  
    images.forEach((image)=>
    {
      observer.observe(image)
    });
}

$(function () 
{
    $('.navbar-toggler').click(function () 
    {
      $('body').toggleClass('noscroll');
    });
});

$(document).ready(function () 
{	 
	observerImages();
    $(".filter-button").click(function () 
    {
      var value = $(this).attr('data-filter');
      if (value == "all") 
      {
        $('.filter').show('1000');
      }
       else 
       {
        $(".filter").not('.' + value).hide('3000');
        $('.filter').filter('.' + value).show('3000');
      }
    });

    $('.popup-with-zoom-anim').magnificPopup(
    {
      type: 'inline',

      fixedContentPos: false,
      fixedBgPos: true,

      overflowY: 'auto',

      closeBtnInside: true,
      preloader: false,

      midClick: true,
      removalDelay: 300,
      mainClass: 'my-mfp-zoom-in'
    });

    $('.popup-with-move-anim').magnificPopup(
    {
      type: 'inline',

      fixedContentPos: false,
      fixedBgPos: true,

      overflowY: 'auto',

      closeBtnInside: true,
      preloader: false,

      midClick: true,
      removalDelay: 300,
      mainClass: 'my-mfp-slide-bottom'
    });
});

window.onscroll = function () 
{
    scrollFunction()
};

function scrollFunction() 
{
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) 
    {
        document.getElementById("movetop").style.display = "block";
    } 
    else 
    {
        document.getElementById("movetop").style.display = "none";
    }
}

function topFunction() 
{
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}

/*ActiveForm*/
$(document).on('submit','.ActiveForm',function()
{
    var el=$(this),
    urlparams=new URLSearchParams(window.location.search),
    next=urlparams.get('next'),
    btn_txt=el.find('button:last').html(),
    form_data=new FormData(this);
    $('.feedback').html('');
    el.children().find('.is-invalid').removeClass('is-invalid');
    $.ajax(
    {
      url:el.attr('action'),
      method:el.attr('method'),
      dataType:'json',
      data:form_data,
      contentType:false,
      cache:false,
      processData:false,
      beforeSend:function()
      {
        el.find('button:last').html('<i class="spinner-border spinner-border-sm" role="status"></i> Please wait...').attr('disabled',true);
      },
      xhr:function()
      {
        const xhr=new window.XMLHttpRequest();
        xhr.upload.addEventListener('progress',function(e)
        {
          if(e.lengthComputable)
          {
            const percent=Math.round((e.loaded/e.total)*100);
            el.find('button:last').html('<i class="spinner-border spinner-border-sm" role="status"></i> Please wait '+percent+'% ...').attr('disabled',true);
          }
        });
        return xhr
      },
      success:function(callback)
      {
        el.find('button:last').html(btn_txt).attr('disabled',false);
        if(callback.valid)
        {
            $('.small-model').modal({show:true});
            $('.small-model').find('.modal-title').text('Success');
            $('.small-model').find('.modal-body').html('<div class="text-success text-center"><i class="fa fa-check-circle"></i> '+callback.message+'.</div>');
            el[0].reset();
            if(callback.bid)
            {
              el.find('button:last').attr('disabled',true);
            } 
            if(callback.whatsapp)
            {
              var phone=document.getElementById('phone').value,
              text=encodeURI("Hello Peter, my name is Kevin I have submitted exam/quiz order with the following details:\nOrder id:#"+callback.id+"\nTopic:"+callback.topic+"\nNumber of MCQ Questions:"+callback.mcq+"\nNumber of Essay questions:"+callback.no_essay+"\nCountry:"+callback.country);
              el.find('.whatsapp').show().append(`<a href="https://api.whatsapp.com/send?phone=${phone}&text=${text}" class="btn btn-round btn-primary btn-block" target="_blank"><i class="fa fa-whatsapp"></i> Whatsapp exam dept</a>`);
            } 
            if(callback.essay)
            {
              window.location='/go/to/dashboard/'+callback.id;
            }
            if(callback.answered)
            {
              if(!el.next().html())
              {
                clearTimeout(myTimeout)
                localStorage.removeItem('lastHValue');
                localStorage.removeItem('lastMValue');
                localStorage.removeItem('lastSValue');
                sessionStorage.removeItem('intro');
                el.find('button:last').hide();
                el.find('.float-right').append('<a class="btn btn-primary btn-round" href="/write/essay/'+callback.id+'">Finish <i class="ti-arrow-right"></i></a>')
              }
              else
              {
                el.find('button:last').removeClass('submitBtn').addClass('nextBtn').attr('type','button').html('next <i class="ti-arrow-right"></i>')
              }
            }
            if(callback.profile)
            {
              if(callback.category =='customer')
              {
                window.location='/profile/setup/complete';
              }
              else
              {
                window.location='/grammer/test/'+callback.id;
              }
            }
            if(callback.register)
            {
              if(callback.category)
              {
                window.location='/accounts/activate/'+callback.category;
              }
            } 
            if(callback.login)
            {
                if(next)
                {
                    window.location=next;
                }
                else
                {
                    window.location='/dashboard';
                }
            }
          }
        else
        {
            $.each(callback.uform_errors,function(key,value)
            {
              el.find("input[aria-label='"+key+"'],select[aria-label='"+key+"'],textarea[aria-label='"+key+"']").addClass('is-invalid').parents('.form-group,.wrapper').find('.feedback').addClass('text-danger').html('<i class="fa fa-exclamation-circle"></i> '+value);
            });
            $.each(callback.eform_errors,function(key,value)
            {
              el.find("input[aria-label='"+key+"'],textarea[aria-label='"+key+"'],select[aria-label='"+key+"']").addClass('is-invalid').parents('.form-group,.wrapper').find('.feedback').addClass('text-danger').html('<i class="fa fa-exclamation-circle"></i> '+value);
            });
        }
        if(callback.error)
        {
            $('.small-model').modal({show:true});
            $('.small-model').find('.modal-title').text('Info');
            $('.small-model').find('.modal-body').html('<div class="text-info text-center"><i class="fas fa-exclamation-triangle"></i> No changes made.</div>');
        }
        if(callback.status)
        {
            window.location='/suspended/account';
        }
      },
      error:function(err)
      {
        el.find('button:last').html(btn_txt).attr('disabled',false);
      }
    });
  return false;
});


/*websocket*/
const roomName = JSON.parse(document.getElementById('room-name').textContent);
const notificationSocket = new WebSocket('ws://'+ window.location.host + '/updates/'+ roomName);
notificationSocket.onmessage = function(e)
{
  const data = JSON.parse(e.data);
  if($('#whats-up-container').children('li').length > 0)
  {
      $('#whats-up-container').prepend(`<li class="second-row__update">
                                          <div>
                                              <span class="first-row__benefit-icon">
                                                  <span>
                                                      <i class="fa whats-up-header fa-reply-all"></i>
                                                  </span>
                                              </span>
                                              <p class="second-row__update-text whats-up-body"><strong><b>${data.title}</b></strong>:${data.activity}</p>
                                          </div><time class="second-row__update-time">
                                              <nobr class="whats-up-left">${data.time}</nobr>
                                          </time>
                                        </li>`);
  }
  else
  {
      $('#whats-up-container').html(` <li class="second-row__update">
                                          <div>
                                              <span class="first-row__benefit-icon">
                                                  <span>
                                                      <i class="fa whats-up-header fa-reply-all"></i>
                                                  </span>
                                              </span>
                                              <p class="second-row__update-text whats-up-body"><strong><b>${data.title}</b></strong>:${data.activity}</p>
                                          </div><time class="second-row__update-time">
                                              <nobr class="whats-up-left">${data.time}</nobr>
                                          </time>
                                      </li>`);
  }
};

        notificationSocket.onclose = function(e)
        {
            console.error('Notification socket closed unexpectedly');
        };
