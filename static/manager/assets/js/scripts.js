$('#myCollapsible').collapse({toggle: true})
$('.accodion-item').on('click',function()
{
    $(this).find('i').toggle()
});

/*ContactForm*/
$(document).on('submit','.ContactForm',function()
{
    var el=$(this),
    btn_txt=el.find('button:last').html(),
    form_data=new FormData(this);
    $('.feedback').html('');
    el.children().find('.is-invalid').removeClass('is-invalid');
    $.ajax(
    {
      url:el.attr('action'),
      method:el.attr('method'),
      dataType:'json',
      data:form_data,
      contentType:false,
      cache:false,
      processData:false,
      beforeSend:function()
      {
        el.find('button:last').html('<i class="spinner-border spinner-border-sm" role="status"></i> Please wait...').attr('disabled',true);
      },
      success:function(callback)
      {
        el.find('button:last').html(btn_txt).attr('disabled',false);
        if(callback.valid)
        {
            $('.small-model').modal({show:true});
            $('.small-model').find('.modal-title').text('Success');
            $('.small-model').find('.modal-body').html('<div class="text-success text-center"><i class="fa fa-check-circle"></i> '+callback.message+'.</div>');
            el[0].reset();
        }
        else
        {
            $.each(callback.uform_errors,function(key,value)
            {
              el.find("input[aria-label='"+key+"'],select[aria-label='"+key+"'],textarea[aria-label='"+key+"']").addClass('is-invalid').parents('.form-group,.wrapper').find('.feedback').addClass('text-danger').html('<i class="fa fa-exclamation-circle"></i> '+value);
            });
            $.each(callback.eform_errors,function(key,value)
            {
              el.find("input[aria-label='"+key+"'],textarea[aria-label='"+key+"'],select[aria-label='"+key+"']").addClass('is-invalid').parents('.form-group,.wrapper').find('.feedback').addClass('text-danger').html('<i class="fa fa-exclamation-circle"></i> '+value);
            });
        }
      },
      error:function(err)
      {
        el.find('button:last').html(btn_txt).attr('disabled',false);
      }
    });
  return false;
});

$(document).on('submit','.SubscribeForm',function()
{
	var el=$(this),
    btn_txt=el.find('button:last').html(),
    form_data=new FormData(this);
    $('.feedback').html('');
    el.children().find('.is-invalid').removeClass('is-invalid');
    $.ajax(
    {
      url:el.attr('action'),
      method:el.attr('method'),
      dataType:'json',
      data:form_data,
      contentType:false,
      cache:false,
      processData:false,
      beforeSend:function()
      {
        el.find('button:last').html('<i class="spinner-border spinner-border-sm" role="status"></i>').attr('disabled',true);
      },
      success:function(callback)
      {
        el.find('button:last').html(btn_txt).attr('disabled',false);
        if(callback.valid)
        {
            el.find("input[aria-label='email']").removeClass('is-invalid').parents('.end-column').find('.feedback').removeClass('text-danger').addClass('text-success').html('<i class="fa fa-check-circle"></i> '+callback.message);
            el[0].reset();
        }
        else
        {
            $.each(callback.uform_errors,function(key,value)
            {
              el.find("input[aria-label='"+key+"']").addClass('is-invalid').parents('.end-column').find('.feedback').addClass('text-danger').html('<i class="fa fa-exclamation-circle"></i> '+value);
            });
        }
      },
      error:function(err)
      {
        el.find('button:last').html(btn_txt).attr('disabled',false);
      }
    });
	return false;
});

/*ActiveForm*/
$(document).on('submit','.RegisterForm',function()
{
    var el=$(this),
    urlparams=new URLSearchParams(window.location.search),
    next=urlparams.get('next'),
    btn_txt=el.find('button:last').html(),
    form_data=new FormData(this);
    $('.feedback').html('');
    el.children().find('.is-invalid').removeClass('is-invalid');
    $.ajax(
    {
      url:el.attr('action'),
      method:el.attr('method'),
      dataType:'json',
      data:form_data,
      contentType:false,
      cache:false,
      processData:false,
      beforeSend:function()
      {
        el.find('button:last').html('<i class="spinner-border spinner-border-sm" role="status"></i> Please wait...').attr('disabled',true);
      },
      xhr:function()
      {
        const xhr=new window.XMLHttpRequest();
        xhr.upload.addEventListener('progress',function(e)
        {
          if(e.lengthComputable)
          {
            const percent=Math.round((e.loaded/e.total)*100);
            el.find('button:last').html('<i class="spinner-border spinner-border-sm" role="status"></i> Please wait '+percent+'% ...').attr('disabled',true);
          }
        });
        return xhr
      },
      success:function(callback)
      {
        el.find('button:last').html(btn_txt).attr('disabled',false);
        if(callback.valid)
        {
            $('.small-model').modal({show:true});
            $('.small-model').find('.modal-title').text('Success');
            $('.small-model').find('.modal-body').html('<div class="text-success text-center"><i class="fa fa-check-circle"></i> '+callback.message+'.</div>');
            el[0].reset();
            if(callback.register)
            {
              if(callback.customer)
              {
                window.location='/customer/activate';
              }
              else
              {
                window.location='/accounts/activate';
              }
            } 
          }
        else
        {
            $.each(callback.uform_errors,function(key,value)
            {
              el.find("input[aria-label='"+key+"'],select[aria-label='"+key+"'],textarea[aria-label='"+key+"']").addClass('is-invalid').parents('.form-group,.wrapper').find('.feedback').addClass('text-danger').html('<i class="fa fa-exclamation-circle"></i> '+value);
            });
            $.each(callback.eform_errors,function(key,value)
            {
              el.find("input[aria-label='"+key+"'],textarea[aria-label='"+key+"'],select[aria-label='"+key+"']").addClass('is-invalid').parents('.form-group,.wrapper').find('.feedback').addClass('text-danger').html('<i class="fa fa-exclamation-circle"></i> '+value);
            });
        }
        if(callback.error)
        {
            $('.small-model').modal({show:true});
            $('.small-model').find('.modal-title').text('Info');
            $('.small-model').find('.modal-body').html('<div class="text-info text-center"><i class="fas fa-exclamation-triangle"></i> No changes made.</div>');
        }
      },
      error:function(err)
      {
        el.find('button:last').html(btn_txt).attr('disabled',false);
      }
    });
  return false;
});