# notification/routing.py
from django.urls import re_path
from . import consumers

websocket_urlpatterns = [
    re_path(r'notification/(?P<room_name>\w+)$', consumers.NotificationConsumer.as_asgi()),
    re_path(r'updates/(?P<room_name>\w+)$', consumers.UpdatesConsumer.as_asgi()),
    re_path(r'writers/(?P<room_name>\w+)$', consumers.WritersConsumer.as_asgi()),
]