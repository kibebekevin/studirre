from django.http import HttpResponse
from django.shortcuts import render
from django.http import HttpResponse
from channels.layers import get_channel_layer
import json
from django.template import RequestContext
from asgiref.sync import async_to_sync


def notify(request):
    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.group_send)(
        #roon name
        "notification_broadcast",
        {
            'type': 'send_notification',
            'message':'notification received'
        }
    )
    return HttpResponse("Done")