# notifications/consumers.py
import json
from channels.generic.websocket import AsyncWebsocketConsumer
from asgiref.sync import sync_to_async
from django.contrib.auth.models import User
import timeago,datetime


@sync_to_async
def checkStatus(status,username):
    user=User.objects.get(username__exact=username)
    if status:
        user.extendedauthuser.is_online=True
    else:
        user.extendedauthuser.is_online=False
    user.save()

    
class NotificationConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = 'notification_%s' % self.room_name
        # Join room group
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )
        await checkStatus(status=True,username=self.room_name)
        await self.accept()

    async def disconnect(self, close_code):
        # Leave room group
        await checkStatus(status=False,username=self.room_name)
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    # async def receive(self, text_data):
    #     text_data_json = json.loads(text_data)
    #     message = text_data_json['message']

    #     # Send message to room group
    #     await self.channel_layer.group_send(
    #         self.room_group_name,
    #         {
    #             'type': 'chat_message',
    #             'message': message
    #         }
    #     )

    # Receive message from room group
    async def send_notification(self, event):
        activity =event.get('activity')
        await self.send(text_data=activity)

    async def send_contact(self,event):
        messages_count =event.get('messages_count')
        await self.send(text_data=messages_count)

    async def notify_client(self,event):
        update =event.get('update')
        await self.send(text_data=update)

    async def notify_users(self,event):
        broadcast_count =event.get('broadcast_count')
        await self.send(text_data=broadcast_count)


class UpdatesConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = 'updates_%s' % self.room_name
        # Join room group
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )
        await self.accept()

    async def disconnect(self, close_code):
        # Leave room group
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    async def send_updates(self,event):
        activity =event.get('activity')
        await self.send(text_data=activity)


class WritersConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = 'notify_%s' % self.room_name
        # Join room group
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )
        await self.accept()

    async def disconnect(self, close_code):
        # Leave room group
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    async def send_updates(self,event):
        activity =event.get('activity')
        print('activity:',activity)
        await self.send(text_data=activity)