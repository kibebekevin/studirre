from pathlib import Path
import environ
import os
import dj_database_url
env=environ.Env()
environ.Env.read_env()

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.2/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY ="&$_2iqt@u_8#nz2esyxe*b=%ee0l*zks$4mwpcn7-urifemqp("

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG =False

if DEBUG:
    ALLOWED_HOSTS = ['localhost','127.0.0.1','192.168.43.106','studirre-36417597e0ce.herokuapp.com','www.studirre-36417597e0ce.herokuapp.com',]
else:
    ALLOWED_HOSTS = ['localhost','127.0.0.1','192.168.43.106','studirre-36417597e0ce.herokuapp.com','www.studirre-36417597e0ce.herokuapp.com',]

# Application definition
SESSION_EXPIRE_AT_BROWSER_CLOSE=True
ACCOUNT_EMAIL_CONFIRMATION_EXPIRE_DAYS=7
ACCOUNT_EMAIL_REQUIRED=True
ACCOUNT_LOGIN_ATTEMPT_LIMIT=4
SITE_ID=8
AUTHENTICATION_BACKENDS = [
    # Needed to login by username in Django admin, regardless of `allauth`
    'django.contrib.auth.backends.ModelBackend',

    # `allauth` specific authentication methods, such as login by e-mail
    'allauth.account.auth_backends.AuthenticationBackend',
]
INSTALLED_APPS = [
    'whitenoise.runserver_nostatic',
    'clearcache',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'installation.apps.InstallationConfig',
    'django.contrib.humanize',
    #'channels',
    #'notifications.apps.NotificationsConfig',
    'phonenumber_field',
    'mathfilters',
    'ckeditor',
    'online_users',
    'ckeditor_uploader',
    'rest_framework',
    'errors.apps.ErrorsConfig',
    'panel.apps.PanelConfig',
    'django.contrib.sites', #social app 
    #'allauth', #social app
    #'allauth.account', #social app
    #'allauth.socialaccount', #social app
    #'allauth.socialaccount.providers.google', #social app
    #'allauth.socialaccount.providers.twitter', #social app
    #'allauth.socialaccount.providers.instagram', #social app
    'django_cleanup.apps.CleanupConfig',

]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'studirre.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR,'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'studirre.site_constants.export_vars',
            ],
        },
    },
]

WSGI_APPLICATION = 'studirre.wsgi.application'
#ASGI_APPLICATION = 'studirre.asgi.application'



# Database
# https://docs.djangoproject.com/en/3.2/ref/settings/#databases


# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.sqlite3',
#         'NAME': BASE_DIR / 'db.sqlite3',
#     }
# }


# DATABASES = {
#     'default': 
#             {

#                 'ENGINE': 'django.db.backends.postgresql',
#                 'NAME':env('DATABASE_NAME'),
#                 'USER':env('DATABASE_USER'),
#                 'PASSWORD':env('DATABASE_PASSWORD'),
#                 'HOST':env('DATABASE_HOST'),
#                 'PORT':env('DATABASE_PORT'),
#             }
# }

# DATABASES = {
#    'default': 
#             {

#                 'ENGINE': 'mysql.connector.django',
#                 'NAME':'studirre',
#                 'USER':'root',
#                 'PASSWORD':'',
#                 'HOST':'localhost',
#                 'PORT':3306,
#                 'OPTIONS':
#                 {
#                     'autocommit':True,
#                 },
#             }
# }

# DATABASES = {
#     'default': 
#             {

#                 'ENGINE': 'mysql.connector.django',
#                 'NAME':env('DATABASE_NAME'),
#                 'USER':env('DATABASE_USER'),
#                 'PASSWORD':env('DATABASE_PASSWORD'),
#                 'HOST':env('DATABASE_HOST'),
#                 'PORT':env('DATABASE_PORT'),
#                 'OPTIONS':
#                 {
#                     'autocommit':True,
#                 },
#             }
# }
DATABASES = {
    'default': dj_database_url.config(default="postgres://qvqwtyoq:eCbQmS7AT7Y1fRvOzTyQyP7nsxOR3IgD@suleiman.db.elephantsql.com/qvqwtyoq")
}
# Password validation
# https://docs.djangoproject.com/en/3.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
        'OPTIONS':
        {
            'min_length':6,
        },
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
    {
        'NAME':'panel.validators.NumberValidator',
        'OPTIONS':
        {
            'min_length':2,
        },
    },
    {
        'NAME':'panel.validators.UpperCaseValidator',
    },
    {
        'NAME':'panel.validators.LowerCaseValidator',
    },
]



#login
LOGIN_URL='/accounts/login'
LOGIN_REDIRECT_URL='/dashboard'
LOGOUT_REDIRECT_URL='/accounts/login'
# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.2/howto/static-files/

STATIC_URL = '/static/'
MEDIA_URL='/media/'

CKEDITOR_UPLOAD_PATH='uploads/'

CKEDITOR_IMAGE_BACKEND='pillow'
CKEDITOR_JQUERY_URL='https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js'
#STATICFILES_STORAGE='whitenoise.storage.CompressedManifestStaticFilesStorage'
# Default primary key field type
# https://docs.djangoproject.com/en/3.2/ref/settings/#default-auto-field

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

if DEBUG:
    STATICFILES_DIRS=[os.path.join(BASE_DIR,'static')]
else:
    STATIC_ROOT=os.path.join(BASE_DIR,'static')


MEDIA_ROOT=os.path.join(BASE_DIR,'media')


BASE_URL="https://studirre-36417597e0ce.herokuapp.com/"

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

EMAIL_HOST='mail.anchortrends.com'
EMAIL_HOST_USER='support@anchortrends.com'
EMAIL_HOST_PASSWORD='@KevyKibbz'
EMAIL_USE_TLS=True
EMAIL_PORT=587 
DEFAULT_FROM_EMAIL=EMAIL_HOST_USER

SESSION_ENGINE='django.contrib.sessions.backends.cache'
SESSION_CACHE_ALIAS='default'
CACHE_TTL = 60 * 45

#cache
CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        #"LOCATION": os.environ.get('REDIS_URL','redis-12648.c263.us-east-1-2.ec2.cloud.redislabs.com:12648'),
        "LOCATION":"redis://default:KevyKibbz@redis-12648.c263.us-east-1-2.ec2.cloud.redislabs.com:12648/studirre",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    }
}




CKEDITOR_CONFIGS = {
    'default': {
        #'skin': 'moono',
        # 'skin': 'office2013',
        'toolbar_Basic': [
            ['Source', '-', 'Bold', 'Italic']
        ],
        'toolbar_YourCustomToolbarConfig': [
            {'name': 'document', 'items': ['Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates']},
            {'name': 'clipboard', 'items': ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
            {'name': 'editing', 'items': ['Find', 'Replace', '-', 'SelectAll']},
            {'name': 'forms',
             'items': ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton',
                       'HiddenField']},
            '/',
            {'name': 'basicstyles',
             'items': ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},
            {'name': 'paragraph',
             'items': ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-',
                       'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl',
                       'Language']},
            {'name': 'links', 'items': ['Link', 'Unlink', 'Anchor']},
            {'name': 'insert',
             'items': ['Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe']},
            '/',
            {'name': 'styles', 'items': ['Styles', 'Format', 'Font', 'FontSize']},
            {'name': 'colors', 'items': ['TextColor', 'BGColor']},
            {'name': 'tools', 'items': ['Maximize', 'ShowBlocks']},
            {'name': 'about', 'items': ['About']},
            '/',  # put this to force next toolbar on new line
            {'name': 'yourcustomtools', 'items': [
                # put the name of your editor.ui.addButton here
                'Preview',
                'Maximize',

            ]},
        ],
        'toolbar': 'YourCustomToolbarConfig',  # put selected toolbar config here
        'toolbarGroups': [{ 'name': 'document', 'groups': [ 'mode', 'document', 'doctools' ] }],
        'height':'full',
        'width': 'full',
        'filebrowserWindowHeight': 725,
        'filebrowserWindowWidth': 940,
        'toolbarCanCollapse': True,
        'mathJaxLib': '//cdn.mathjax.org/mathjax/2.2-latest/MathJax.js?config=TeX-AMS_HTML',
        'tabSpaces': 4,
        'extraPlugins': ','.join([
            'uploadimage', # the upload image feature
            # your extra plugins here
            'div',
            'autolink',
            'autoembed',
            'embedsemantic',
            'autogrow',
            # 'devtools',
            'widget',
            'lineutils',
            'clipboard',
            'dialog',
            'dialogui',
            'elementspath'
        ]),
    }
}


#channels
"""CHANNEL_LAYERS={
    'default':{
        'BACKEND':'channels_redis.core.RedisChannelLayer',
        #'BACKEND':'channels.layers.InMemoryChannelLayer',
        'CONFIG':{
            #'hosts':[os.environ.get('REDIS_URL','redis://localhost:6379')],
            'hosts':['redis://localhost:6379',],
        },
    },
}"""
